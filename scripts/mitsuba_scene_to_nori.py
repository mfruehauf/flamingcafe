import argparse
import mitsuba as mi

mi.set_variant("scalar_rgb")
from pathlib import Path
from plyfile import PlyData
import xml.etree.ElementTree as ET
from xml.dom import minidom
import numpy as np
import os


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--scene",
                        type=str,
                        required=True,
                        help="Path to the mitsuba scene")
    parser.add_argument("--out",
                        type=str,
                        required=True,
                        help="Path to the output nori scene")
    return parser.parse_args()


def ply_to_obj(ply: str, obj_path: str = None):
    # taken from https://gist.github.com/assafrabin/ca110dba829dc960c48ad9ab90b8081e
    """
    Converts the given .ply file to an .obj file
    """
    ply = PlyData.read(ply)
    with open(obj_path, 'w') as f:
        f.write("# OBJ file\n")

        verteces = ply['vertex']

        for v in verteces:  # convert vertices
            p = [v['x'], v['y'], v['z']]
            if 'red' in v and 'green' in v and 'blue' in v:
                c = [v['red'] / 256, v['green'] / 256, v['blue'] / 256]
            else:
                c = [0, 0, 0]
            a = p + c
            f.write("v %.6f %.6f %.6f %.6f %.6f %.6f \n" % tuple(a))

        for v in verteces:  # vertex normals
            if 'nx' in v and 'ny' in v and 'nz' in v:
                n = [v['nx'], v['ny'], v['nz']]
                f.write("vn %.6f %.6f %.6f\n" % n)

        for v in verteces:  # texture coordinates
            if 's' in v and 't' in v:
                t = (v['s'], v['t'])
                f.write("vt %.6f %.6f\n" % t)

        if 'face' in ply:
            for i in ply['face']['vertex_indices']:
                f.write("f")
                for j in range(i.size):
                    # ii = [ i[j]+1 ]
                    ii = [i[j] + 1, i[j] + 1, i[j] + 1]
                    # f.write(" %d" % tuple(ii) )
                    f.write(" %d/%d/%d" % tuple(ii))
                f.write("\n")


def get_basic_type(xml, type_name, name, default=None):
    try:
        return next((d.get("value") for d in xml.findall(type_name)
                     if d.get("name") == name))
    except StopIteration:
        return default


def shapes_to_nori(scene_path: str, out: str, mitsuba_root: ET.ElementTree,
                   nori_root: ET.ElementTree):
    for shape in mitsuba_root.findall("shape"):
        # get the mesh data
        shape_index = int(
            get_basic_type(shape, "integer", "shape_index", default=0))
        file_name = get_basic_type(shape, "string", "filename")
        mesh_path = os.path.join(os.path.dirname(scene_path), file_name)

        # get the transform
        transform = shape.find("transform")
        parsed_transform = mi.Transform4f()
        for t in transform:
            if t.tag == "rotate":
                axis = np.array([0, 0, 0])
                if t.get("x") == "1":
                    axis = np.array([1, 0, 0])
                elif t.get("y") == "1":
                    axis = np.array([0, 1, 0])
                elif t.get("z") == "1":
                    axis = np.array([0, 0, 1])
                parsed_transform = mi.ScalarTransform4f.rotate(
                    axis, float(t.get("angle"))) @ parsed_transform
            elif t.tag == "matrix":
                matrix = np.asarray(
                    list(
                        map(float,
                            transform.find("matrix").get("value").split(
                                " ")))).reshape(4, 4)
                parsed_transform = mi.Transform4f(matrix) @ parsed_transform
            elif t.tag == "translate":
                offset = np.array([0, 0, 0])
                if t.get("value"):
                    offset = np.array(
                        list(map(float,
                                 t.get("value").split(" "))))
                if t.get("x"):
                    offset[0] = float(t.get("x"))
                if t.get("y"):
                    offset[1] = float(t.get("y"))
                if t.get("z"):
                    offset[2] = float(t.get("z"))
                parsed_transform = mi.ScalarTransform4f.translate(
                    offset) @ parsed_transform

        transform_type = transform.get("name")

        # load the mesh
        mesh = mi.load_dict({
            "type": shape.get("type"),
            "filename": mesh_path,
            "shape_index": shape_index,
            "face_normals": False,
            transform_type: parsed_transform
        })

        # save the mesh as ply and convert it to obj
        out_mesh_dir = Path(out) / "meshes"
        out_mesh_dir.mkdir(exist_ok=True)
        ply_path = out_mesh_dir / f"{shape.get('id')}.ply"
        obj_path = out_mesh_dir / f"{shape.get('id')}.obj"
        mesh.write_ply(str(ply_path))

        # nori only supports obj files. Therefore, we convert the ply file to obj here
        # ply_to_obj(ply_path, obj_path)
        # ply_path.unlink()

        # create the mesh node in nori
        nori_mesh = ET.SubElement(nori_root, "mesh")
        nori_mesh.set("type", "obj")
        nori_mesh.append(
            ET.Element("string", name="filename", value=os.path.join("meshes", obj_path.name)))


def camera_to_nori(mitsuba_root: ET.ElementTree, nori_root: ET.ElementTree):
    for sensor in mitsuba_root.findall("sensor"):
        nori_camera = ET.SubElement(nori_root, "camera")
        nori_camera.set("type", sensor.get("type"))

        fov = get_basic_type(sensor, "float", "fov")
        if fov:
            nori_camera.append(ET.Element("float", name="fov", value=fov))

        transform = sensor.find("transform")

        nori_transform = ET.Element(
            "transform",
            name="toWorld" if transform.get("name") == "to_world" else "")

        lookat = transform.find("lookat")
        if lookat is not None:
            nori_transform.append(
                ET.Element("lookat",
                           target=lookat.get("target"),
                           origin=lookat.get('origin'),
                           up=lookat.get("up")))

        nori_camera.append(nori_transform)

        width = get_basic_type(mitsuba_root, "default", "width")
        height = get_basic_type(mitsuba_root, "default", "height")
        nori_camera.append(ET.Element("integer", name="height", value=height))
        nori_camera.append(ET.Element("integer", name="width", value=width))


def add_integrator(nori_root: ET.Element):
    integrator = ET.Element("integrator", type="av")
    integrator.append(ET.Element("float", name="length", value="10"))
    nori_root.append(integrator)


def main(scene_path: str, out: str):
    # load all shapes from the serialized file
    mitsuba_root = ET.parse(scene_path).getroot()
    nori_root = ET.Element("scene")

    add_integrator(nori_root)
    camera_to_nori(mitsuba_root, nori_root)
    shapes_to_nori(scene_path,out, mitsuba_root, nori_root)

    # write pretty xml to file
    nori_xml = ET.tostring(nori_root, "utf-8")
    parsed = minidom.parseString(nori_xml)
    with open(Path(out) / "scene.xml", "w") as f:
        f.write(parsed.toprettyxml())


if __name__ == "__main__":
    args = parse_args()
    main(args.scene, args.out)