#if !defined(__NORI_INTERPOLATE_H)
#define __NORI_INTERPOLATE_H

#include <Eigen/Core>
#include <nori/common.h>

NORI_NAMESPACE_BEGIN

/**
 * @brief Interpolates a Dynamic Eigen array af any shape
 *
 * @tparam T
 * @param uv
 * @param map
 * @return T
 */
template <typename T>
T bilinearInterpolation(
    const Point2f &uv,
    // TODO(rasaford): This is not yet as generic as it could be
    const Eigen::Array<T, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>
        &map) {
  // taken from https://en.wikipedia.org/wiki/Bilinear_interpolation
  float x = uv.x(), y = uv.y();
  int x1 = (int)floor(x), x2 = (int)ceil(x);
  int y1 = (int)floor(y), y2 = (int)ceil(y);

  // make sure all points are in bounds
  int rows = map.rows() - 1, cols = map.cols() - 1;

  x1 = clamp(x1, 0, cols), x2 = clamp(x2, 0, cols);
  y1 = clamp(y1, 0, rows), y2 = clamp(y2, 0, rows);

  float x1_f = (float)x1, x2_f = (float)x2;
  float y1_f = (float)y1, y2_f = (float)y2;

  // avoid division by 0
  float wx, wy;
  wx = (x1==x2)? 0 : (x2_f - x) / (x2_f - x1_f);
  wy = (y1==y2)? 0 : (y2_f - y) / (y2_f - y1_f);

  T q_11 = map(y1, x1);
  T q_12 = map(y2, x1);
  T q_21 = map(y1, x2);
  T q_22 = map(y2, x2);


  // first interpolate horizontally
  T r1 = wx * q_11 + (1-wx) * q_21;
  T r2 = wx * q_12 + (1-wx) * q_22;
  // then vertically
  return (wy) * r1 + (1-wy) * r2;
}

template <typename T>
T bilinearInterpolation(
    const Point2f &uv,
    // TODO(rasaford): This is not yet as generic as it could be
    const std::vector<T> &vec, int const& rows, int const& cols) {
  // taken from https://en.wikipedia.org/wiki/Bilinear_interpolation
  float x = uv.x(), y = uv.y();
  int x1 = (int)floor(x), x2 = (int)ceil(x);
  int y1 = (int)floor(y), y2 = (int)ceil(y);

  // make sure all points are in bounds

  x1 = clamp(x1, 0, cols), x2 = clamp(x2, 0, cols);
  y1 = clamp(y1, 0, rows), y2 = clamp(y2, 0, rows);

  float x1_f = (float)x1, x2_f = (float)x2;
  float y1_f = (float)y1, y2_f = (float)y2;

  // avoid division by 0
  float wx, wy;
  wx = (x1==x2)? 0 : (x2_f - x) / (x2_f - x1_f);
  wy = (y1==y2)? 0 : (y2_f - y) / (y2_f - y1_f);


  T q_11 = vec[x1*cols + y1];
  T q_12 = vec[x1*cols + y2];
  T q_21 = vec[x2*cols + y1];
  T q_22 = vec[x2*cols + y2];

  // first interpolate horizontally
  T r1 = wx * q_11 + (1-wx) * q_21;
  T r2 = wx * q_12 + (1-wx) * q_22;
  // then vertically
  return (wy) * r1 + (1-wy) * r2;
}

NORI_NAMESPACE_END

#endif /* __NORI_INTERPOLATE_H */