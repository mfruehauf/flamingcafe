#pragma once

#include "nori/common.h"
#include "nori/object.h"

NORI_NAMESPACE_BEGIN

struct PhaseFunctionQueryRecord {
	/// Incident direction (in the local frame) input
	Vector3f wi;

	/// Outgoing direction (in the local frame) sample this
	Vector3f wo;

	/// Point associated with the point
	Point3f p;

	//Create an uninitialized PhaseFunctionQueryRecord
	PhaseFunctionQueryRecord() {}

	//Constructor used to call sample
	PhaseFunctionQueryRecord(Vector3f const& wi) : wi(wi)
	{}

	//Constructor used to call eval
	PhaseFunctionQueryRecord(Vector3f const& wi, Vector3f const& wo, Point3f const& p) : wi(wi), wo(wo), p(p)
	{}
};




class PhaseFunction : public NoriObject
{
public:

	virtual float eval(PhaseFunctionQueryRecord const& pRec)  const = 0;
	virtual float sample(PhaseFunctionQueryRecord& pRec, Point2f const& sample) const = 0;


	virtual EClassType getClassType() const { return EPhaseFunction; }

};



NORI_NAMESPACE_END