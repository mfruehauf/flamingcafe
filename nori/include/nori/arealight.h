#include <nori/emitter.h>
#include <nori/mesh.h>
#include <nori/shape.h>
#include <nori/warp.h>

NORI_NAMESPACE_BEGIN

class AreaEmitter : public Emitter {
public:
  AreaEmitter(const PropertyList &props) {
    m_radiance = props.getColor("radiance");
  }

  /**
   * @brief  This constructor is only used for creating new Triangle based
   * lights.
   *
   * @param radiance radiance of the area emitter
   */
  AreaEmitter(const Color3f &radiance) : m_radiance(radiance){};

  virtual Color3f eval(const EmitterQueryRecord &lRec) const override;

  virtual Color3f sample(EmitterQueryRecord &lRec,
                         const Point2f &sample) const override;

  virtual float pdf(const EmitterQueryRecord &lRec) const override;

  virtual Color3f samplePhoton(Ray3f &ray, const Point2f &sample1,
                               const Point2f &sample2) const override;

  virtual EmitterBounds bounds() const override;

  virtual bool isAreaLight() const override { return true; }

  Color3f getRadiance() const { return m_radiance; }

  virtual std::string toString() const override;

protected:
  Color3f m_radiance;
};

NORI_NAMESPACE_END