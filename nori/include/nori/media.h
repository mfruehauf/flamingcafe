#if !defined(__NORI_MEDIA_H)
#define __NORI_MEDIA_H

#include <nori/object.h>
#include <nori/sampler.h>
#include <nori/volume.h>
#include <nori/phasefunctions.h>

NORI_NAMESPACE_BEGIN




struct MediumQueryRecord {
	//The ray that is being followed inside the volume
	Ray3f passingRay;

	//The t for which an interaction in the medium is found
	float t = 0.f;

	//Indicate if the sampled point is inside the medium (true) or attained the maximum length allowed for the ray(false)
	bool sampledMedium = false;

	//Estimated light emitted by the medium if it is emissive
	Color3f Le = Color3f::Zero();

	//Create an uninitialized MediumQueryRecord
	MediumQueryRecord() {}

	//To sample a medium interaction
	MediumQueryRecord(Ray3f const& passingRay):passingRay(passingRay){}

	//To evaluate the transmittance along a ray in a medium
	MediumQueryRecord(Ray3f const& passingRay, float t) : passingRay(passingRay), t(t) {}
};



class Medium : public NoriObject
{
public:

	//Evaluate the transmittance along the passing ray (we suppose that the ray is fully contained inside the medium)
	virtual Color3f eval(MediumQueryRecord const& mRec, Sampler* sampler) const = 0;

	virtual Color3f evalEmission(MediumQueryRecord const& mRec, Sampler* sampler) const {return 0.0f;}//Not emissive by default;


	//Sample a ray inside the volume and return the transmittance of light along this ray
	virtual Color3f sample(MediumQueryRecord& mRec, Sampler* sampler) const = 0;

	virtual const PhaseFunction* getPhaseFunction() const { return m_PhaseFunction; }

	virtual void activate() override;

	virtual void addChild(NoriObject* child) override;

	virtual BoundingBox3f getBoundingBox() const {return m_bbox;}

	virtual EClassType getClassType() const override { return EMedium; }


	virtual bool isEmissive() const { return false; }

protected:
	BoundingBox3f m_bbox;
	PhaseFunction* m_PhaseFunction = nullptr;
	
};



struct MediaInterface {
	const Medium* innerMedium = nullptr;
	const Medium* outerMedium = nullptr;

	MediaInterface() = default;
	MediaInterface(Medium* innerMedium, Medium* outerMedium = nullptr) : innerMedium(innerMedium), outerMedium(outerMedium) {}

	bool isMediumTransition() const { return (innerMedium != outerMedium); }
};

NORI_NAMESPACE_END

#endif /* __NORI_MEDIA_H */