#pragma once

#include <nori/object.h>
#include <nori/bbox.h>

NORI_NAMESPACE_BEGIN

template<typename T>
class Volume : public NoriObject
{
public:
	Volume() {}
	virtual ~Volume(){}


	virtual EClassType getClassType() const override{ return EVolume; }

	virtual T eval(Point3f const& eval) const = 0;

	virtual bool isOutOfBounds(Point3f const& p) const {return false;}

	virtual BoundingBox3f getBoundingBox() const {return m_bbox;}
	virtual Transform getTransform() const {return m_transform;}

	virtual bool rayIntersect(Ray3f const& ray, float& maxt) const {  maxt = ray.maxt; return true;}



	virtual T maxValue()  const = 0;

protected:
	BoundingBox3f m_bbox;
	Transform m_transform;
};


NORI_NAMESPACE_END