#if !defined(__NORI__DISTRIBUTION_H)
#define __NORI_DISTRIBUTION_H

#include <memory>
#include <nori/common.h>
#include <nori/vector.h>

NORI_NAMESPACE_BEGIN

// these implementations are taken from pbrt (Book 3rd ed.)
class Distribution1D {
public:
  Distribution1D(const float *f, int n);

  int count() const { return func.size(); }

  float sampleContinuous(const float u, float *pdf, int *pos = nullptr) const;

  int sampleDiscrete(const float u, float *pdf,
                       float *u_remapped = nullptr) const;

  std::vector<float> func, cdf;
  float func_int;

private:
  int getOffset(const float u) const;
};

class Distribution2D {
public:
  Distribution2D(const float *data, const int rows, const int cols);

  Point2f sampleContinuous(const Point2f &u, float *pdf) const;

  float pdf(const Point2f &u) const;

protected:
  std::vector<std::unique_ptr<Distribution1D>> m_conditionals;
  std::unique_ptr<Distribution1D> m_marginal;
};

NORI_NAMESPACE_END

#endif