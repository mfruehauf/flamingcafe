#!/bin/bash
mkdir -p build &&
cd build &&
cmake .. -DCMAKE_CXX_STANDARD="14" &&
CXXFLAGS="-std=c++14" make -j16
