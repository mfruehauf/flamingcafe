#include <nori/bsdf.h>
#include <nori/integrator.h>
#include <nori/sampler.h>
#include <nori/scene.h>
#include <stdio.h>

NORI_NAMESPACE_BEGIN

class PathBRDFIntegrator : public Integrator {
public:
  PathBRDFIntegrator(const PropertyList &props) {}

  bool inline russianRoulette(const float sample,
                              const float success_prob) const {
    return sample > success_prob;
  }

  virtual Color3f Li(const Scene *scene, Sampler *sampler,
                     const Ray3f &ray_) const override {
    //
    // Estimate the local illumination integral
    //
    Color3f Li{Color3f::Zero()};
    Ray3f ray = ray_; // copy the initial ray
    Color3f t{Color3f::Ones()};
    BSDFQueryRecord bRec{Point3f::Zero()};
    EmitterQueryRecord lRec;
    Color3f f;
    float success_prob = 1.f;
    // EmitterQueryRecord lRec{its.p};

    Intersection its;
    while (true) {
      if (!scene->rayIntersect(ray, its)) {
        break;
      }
      if (its.mesh != nullptr && its.mesh->isEmitter()) {
        lRec = EmitterQueryRecord(ray.o, its.p, its.shFrame.n);
        Li += t * its.mesh->getEmitter()->eval(lRec);
      }

      success_prob = std::min(t.x(), 1.f - Epsilon);
      // std::cout << success_prob << std::endl;
      if (russianRoulette(sampler->next1D(), success_prob)) {
        break;
      } else {
        t /= success_prob;
      }

      bRec.wi = its.toLocal(-ray.d);
      bRec.uv = its.uv;
      bRec.p = its.p;

      // the surface BSDF is already importance sampled
      // f = BSDF * cos(theta) / pdf
      f = its.mesh->getBSDF()->sample(bRec, sampler->next2D());
      t *= f;

      // update the ray for next termination
      ray = Ray3f(its.p, its.toWorld(bRec.wo));
    }
    return Li;
  }

  std::string toString() const {
    return tfm::format("PathMatsIntegrator[\n"
                       "lightsampler = %s\n"
                       "]",
                       m_light_sampler->toString());
  }

protected:
};

NORI_REGISTER_CLASS(PathBRDFIntegrator, "path_mats");

NORI_NAMESPACE_END