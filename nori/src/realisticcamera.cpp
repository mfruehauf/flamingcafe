
#include <nori/bbox.h>
#include <nori/camera.h>
#include <nori/common.h>
#include <nori/ray.h>
#include <nori/rfilter.h>
#include <nori/sampler.h>
#include <filesystem/resolver.h>
#include <tbb/tbb.h>
#include <array>
#include <fstream>
#include <iostream>
#include <memory>
#include <sstream>
#include <string>
#include <vector>

NORI_NAMESPACE_BEGIN

struct LensElement {
  LensElement(const float curvatureRadius, const float thickness,
              const float eta, const float apertureRadius)
      : curvatureRadius(curvatureRadius),
        thickness(thickness),
        eta(eta),
        apertureRadius(apertureRadius){};
  float curvatureRadius;
  float thickness;
  float eta;
  float apertureRadius;
};

struct Film {
 public:
  Film(const int width, const int height, const float size) {
    fullResolution =
        Vector2f(static_cast<float>(width), static_cast<float>(height));
    float aspect = fullResolution.x() / fullResolution.y();
    diagonal = size * std::sqrt(1 + aspect * aspect);

    // precompute the 2D bounding box of the film
    aspect = fullResolution.y() / fullResolution.x();
    float x = std::sqrt(diagonal * diagonal / (1 + aspect * aspect));
    float y = aspect * x;
    extent = BoundingBox2f(Point2f(-x / 2, -y / 2), Point2f(x / 2, y / 2));
  }

  BoundingBox2f getPhysicalExtent() const { return extent; }

  Vector2f fullResolution;
  // size of the film diagonal
  float diagonal;  // (m)
  BoundingBox2f extent;
};

// This implementation is based on the one described in PBRT
class RealisticCamera : public Camera {
 public:
  RealisticCamera(const PropertyList &props) {
    /* Width and height in pixels. Default: 720p */
    m_outputSize.x() = props.getInteger("width", 1280);
    m_outputSize.y() = props.getInteger("height", 720);
    m_invOutputSize = m_outputSize.cast<float>().cwiseInverse();

    // hard coded film size for now
    m_film = std::unique_ptr<Film>(
        new Film(m_outputSize.x(), m_outputSize.y(),
                 props.getFloat("filmSize", 0.001f * 36)));

    /* Specifies an optional camera-to-world transformation. Default: none */
    m_cameraToWorld = props.getTransform("toWorld", Transform());

    /* Horizontal field of view in degrees */
    m_fov = props.getFloat("fov", 30.0f);

    /* Near and far clipping planes in world-space units */
    m_nearClip = props.getFloat("nearClip", 1e-4f);
    m_farClip = props.getFloat("farClip", 1e4f);

    m_focus_distance = props.getFloat("focusDistance", 1.f);  // default 1m
    m_apertureDiameter =
        props.getFloat("apertureDiameter", 0.f);  // default 0mm i.e. no DoF

    m_apertureOffset = props.getFloat(
        "apertureOffset", 0.f);  // default 0 i.e. no chromatic abberation

    std::string lensPath =
        getFileResolver()->resolve(props.getString("lensfile")).str();
    m_lensElements = parseLensFile(lensPath);

    // focus the lens system
    m_lensElements.back().thickness = focusThickLens(m_focus_distance);

    std::cout << tfm::format("Realistic Camera focus: %f -> %f",
                             m_lensElements.back().thickness,
                             focusDistance(m_lensElements.back().thickness))
              << std::endl;

    // compute exit pupil bounds
    const int numSamples = 64;
    m_exitPupilBounds.resize(numSamples);
    tbb::parallel_for(tbb::blocked_range<int>(0, numSamples),
                      [&](const tbb::blocked_range<int> r) {
                        for (int i = r.begin(); i < r.end(); i++) {
                          float r0 = static_cast<float>(i) / numSamples *
                                     m_film->diagonal / 2;
                          float r1 = static_cast<float>(i + 1) / numSamples *
                                     m_film->diagonal / 2;
                          m_exitPupilBounds[i] = boundExitPupil(r0, r1);
                        }
                      });

    m_rfilter = NULL;
  }

  float focusDistance(float filmDistance) const {
    // Find offset ray from film center through lens
    BoundingBox2f bounds = boundExitPupil(0, .001 * m_film->diagonal);

    const std::array<float, 3> scaleFactors = {0.1f, 0.01f, 0.001f};
    float lu = 0.0f;

    Ray3f ray;

    // Try some different and decreasing scaling factor to find focus ray
    // more quickly when `aperturediameter` is too small.
    // (e.g. 2 [mm] for `aperturediameter` with wide.22mm.dat),
    bool foundFocusRay = false;
    for (float scale : scaleFactors) {
      lu = scale * bounds.max[0];
      if (traceLensFromFilm(Ray3f(Point3f(0, 0, lensRearZ() - filmDistance),
                                  Vector3f(lu, 0, filmDistance)),
                            ray)) {
        foundFocusRay = true;
        break;
      }
    }

    if (!foundFocusRay) {
      std::cerr << tfm::format(
          "Focus ray at lens pos(%f,0) didn't make it through the lenses "
          "with film distance %f?!??\n",
          lu, filmDistance);
      return -1;
    }

    // Compute distance _zFocus_ where ray intersects the principal axis
    float tFocus = -ray.o.x() / ray.d.x();
    float zFocus = ray(tFocus).z();
    if (zFocus < 0) zFocus = -1;
    return zFocus;
  }
  virtual void activate() override {
    /* If no reconstruction filter was assigned, instantiate a Gaussian filter
     */
    if (!m_rfilter) {
      m_rfilter = static_cast<ReconstructionFilter *>(
          NoriObjectFactory::createInstance("gaussian", PropertyList()));
      m_rfilter->activate();
    }
  }

 protected:
  std::vector<float> readFloatFile(const std::string &path) const {
    // code for reading a float file adapted from
    // https://stackoverflow.com/questions/8421170/read-floats-from-a-txt-file
    std::ifstream source;
    source.open(path, std::ios_base::in);
    if (!source) {
      std::cerr << "Cannot open file " << path << std::endl;
      return {};
    }
    std::vector<float> out;
    for (std::string line; std::getline(source, line);) {
      std::istringstream in(line);
      // does the string start with "#"
      if (in.peek() == '#') {
        continue;
      }
      // otherwise read the 4 floats in this line
      std::array<float, 4> data;
      in >> data[0] >> data[1] >> data[2] >> data[3];
      out.insert(out.end(), data.begin(), data.end());
    }
    return out;
  }

  std::vector<LensElement> parseLensFile(const std::string &lensPath) const {
    std::vector<float> lensData = readFloatFile(lensPath);
    if (lensData.size() % 4 != 0) {
      throw NoriException(
          "Invalid lens file format, number of elements was "
          "not a multiple of 4");
    }
    std::vector<LensElement> lensElements;
    for (size_t i = 0; i < lensData.size(); i += 4) {
      if (lensData[i] == 0) {
        if (m_apertureDiameter > 0.001f * lensData[i + 3]) {
          std::cerr << tfm::format(
                           "Specified aperture diameter is %f is "
                           "greater than maximum %f. Clipping it ",
                           m_apertureDiameter, 0.001f * lensData[i + 3])
                    << std::endl;
          lensData[i + 3] = 1000.f * m_apertureDiameter;
        }
      }
      // each fine is of the format (radius, axpos, eta, aperture)
      // units are converted from millimeters to meters and the aperture
      // diameter is converted to a radius
      lensElements.emplace_back(0.001f * lensData[i], 0.001f * lensData[i + 1],
                                lensData[i + 2], 0.001f * lensData[i + 3] / 2);
    }
    return lensElements;
  }

  float lensRearZ() const { return m_lensElements.back().thickness; }

  float lensFrontZ() const {
    float sum = 0.f;
    for (const auto &element : m_lensElements) {
      sum += element.thickness;
    }
    return sum;
  }

  float rearElementRadius() const {
    return m_lensElements.back().apertureRadius;
  }

  bool intersectSphericalElement(const float radius, const float zCenter,
                                 const Ray3f &ray, float *t,
                                 Vector3f *n) const {
    // intersect the ray with a sphere
    Point3f o = ray.o - Vector3f(0, 0, zCenter);
    // quadratic intersection test
    // NOTE(rasaford): Need to use doubles here for sufficient numerical
    // precision
    double a = ray.d.squaredNorm();
    double b = 2 * ray.d.dot(o);
    double c = o.squaredNorm() - radius * radius;
    double discriminant = b * b - 4.0 * a * c;
    if (discriminant < 0.f) {
      return false;
    }
    discriminant = sqrt(discriminant);
    double q = (b < 0) ? -0.5 * (b - discriminant) : -0.5 * (b + discriminant);
    float t0 = static_cast<float>(q / a);
    float t1 = static_cast<float>(c / q);

    if (t0 > t1) {
      std::swap(t0, t1);
    }

    // select the correct intersection
    bool useCloser = (ray.d.z() > 0) ^ (radius < 0);
    *t = useCloser ? std::min(t0, t1) : std::max(t0, t1);
    if (*t < 0.f) {
      return false;
    }
    *n = (o + *t * ray.d).normalized();
    // face forward (ray.d and n have to point in opposite directions)
    *n = (-ray.d).dot(*n) < 0.f ? Vector3f(-(*n)) : Vector3f(*n);
    return true;
  }
  bool refractRay(const Vector3f &wi, const Vector3f &n, const float eta,
                  Vector3f *wt) const {
    float cosThetaI = n.dot(wi);
    float sin2ThetaI = std::max(0.f, 1.f - cosThetaI * cosThetaI);
    float sin2ThetaT = eta * eta * sin2ThetaI;
    // total internal reflection
    if (sin2ThetaT >= 1) {
      return false;
    }
    float cosThetaT = std::sqrt(1 - sin2ThetaT);
    // refract the ray
    *wt = (eta * -wi + (eta * cosThetaI - cosThetaT) * n).normalized();
    return true;
  }

  bool traceLensFromFilm(const Ray3f &rayCamera, Ray3f &rayOut) const {
    float elementZ = 0;
    // rayCamera to lens space
    static const Transform cameraToLens = Transform::scale(Vector3f(1, 1, -1));
    Ray3f rayLens = cameraToLens * rayCamera;

    for (int i = m_lensElements.size() - 1; i >= 0; i--) {
      const LensElement &element = m_lensElements[i];
      elementZ -= element.thickness;
      // compute ray intersection with the lens element
      float t;
      Vector3f n;
      bool isStop = (element.curvatureRadius == 0);
      if (isStop) {  // the lens radius is infinite so we just compute a ray /
                     // plane intersection
        if (rayLens.d.z() >= 0.f) return false;
        t = (elementZ - rayLens.o.z()) / rayLens.d.z();
      } else {
        float radius = element.curvatureRadius;
        float zCenter = elementZ + element.curvatureRadius;
        if (!intersectSphericalElement(radius, zCenter, rayLens, &t, &n)) {
          return false;
        }
      }
      if (t < 0.f) {
        throw NoriException("RealisticCamera: Ray intersection is negative");
      }

      // test the intersection point against the spherical aperture
      Point3f pHit = rayLens(t - Epsilon);
      float r2 = pHit.x() * pHit.x() + pHit.y() * pHit.y();
      if (r2 > element.apertureRadius * element.apertureRadius) {
        return false;
      }
      rayLens.o = pHit;
      // ray refraction

      if (!isStop) {
        Vector3f w;
        float etaI = element.eta;  // entering IoR
        float etaT = (i > 0 && m_lensElements[i - 1].eta != 0)
                         ? m_lensElements[i - 1].eta
                         : 1;  // exiting IoR
        if (!refractRay((-rayLens.d).normalized(), n, etaI / etaT, &w)) {
          return false;
        }
        rayLens.d = w;
      }
    }
    // transform the ray to camera space
    static const Transform lensToCamera(Transform::scale(Vector3f(1, 1, -1)));
    Ray3f r = lensToCamera * rayLens;
    rayOut = r;
    return true;
  }

  bool traceLensFromScene(const Ray3f &rayCamera, Ray3f &rayOut) const {
    float elementZ = -lensFrontZ();
    // rayCamera to lens space
    static const Transform cameraToLens = Transform::scale(Vector3f(1, 1, -1));
    Ray3f rayLens = cameraToLens * rayCamera;

    for (size_t i = 0; i < m_lensElements.size(); i++) {
      const LensElement &element = m_lensElements[i];
      // compute ray intersection with the lens element
      float t;
      Vector3f n;
      bool isStop = element.curvatureRadius == 0;
      if (isStop) {  // the lens radius is infinite so we just compute a ray /
                     // plane intersection
        t = (elementZ - rayLens.o.z()) / rayLens.d.z();
      } else {
        float radius = element.curvatureRadius;
        float zCenter = elementZ + element.curvatureRadius;
        if (!intersectSphericalElement(radius, zCenter, rayLens, &t, &n)) {
          return false;
        }
      }
      if (t < 0.f) {
        throw NoriException("RealisticCamera: Ray intersection is negative");
      }

      // test the intersection point against the spherical aperture
      Point3f pHit = rayLens(t - Epsilon);
      float r2 = pHit.x() * pHit.x() + pHit.y() * pHit.y();
      if (r2 > element.apertureRadius * element.apertureRadius) {
        return false;
      }
      rayLens.o = pHit;
      // ray refraction

      if (!isStop) {
        Vector3f w;
        float etaI = (i == 0 || m_lensElements[i - 1].eta == 0)
                         ? 1
                         : m_lensElements[i - 1].eta;       // entering IoR
        float etaT = (element.eta != 0) ? element.eta : 1;  // exiting IoR
        if (!refractRay((-rayLens.d).normalized(), n, etaI / etaT, &w)) {
          return false;
        }
        rayLens.d = w;
      }

      elementZ += element.thickness;
    }
    // transform the ray to camera space
    static const Transform lensToCamera(Transform::scale(Vector3f(1, 1, -1)));
    Ray3f r = lensToCamera * rayLens;
    rayOut = r;
    return true;
  }

  /**
   * @brief Computes the lens system's cardinal points. Rays are in camera space
   * but returned optical points are in lens space
   *
   * @param rayIn
   * @param rayOut
   * @param pz
   * @param fz
   */
  void computeCardinalPoints(const Ray3f &rayIn, const Ray3f &rayOut, float *pz,
                             float *fz) const {
    float tf = -rayOut.o.x() / rayOut.d.x();
    float tp = (rayIn.o.x() - rayOut.o.x()) / rayOut.d.x();
    *fz = -rayOut(tf).z();
    *pz = -rayOut(tp).z();
  }

  void computeThickLensApproximation(float pz[2], float fz[2]) const {
    float x = 0.001 * m_film->diagonal;

    // these rays are in camera space
    Ray3f rayScene(Point3f(x, 0, lensFrontZ() + 1), Vector3f(0, 0, -1));
    Ray3f rayFilm;
    if (!traceLensFromScene(rayScene, rayFilm)) {
      throw NoriException(
          "RealisticCamera: Could not trace ray from scene to film");
    }
    computeCardinalPoints(rayScene, rayFilm, &pz[0], &fz[0]);

    rayFilm = Ray3f(Point3f(x, 0, lensRearZ() - 1), Vector3f(0, 0, 1));
    if (!traceLensFromFilm(rayFilm, rayScene)) {
      throw NoriException(
          "RealisticCamera: Could not trace ray from film to scene");
    }
    computeCardinalPoints(rayFilm, rayScene, &pz[1], &fz[1]);
  }

  float focusThickLens(const float focusDistance) const {
    float pz[2], fz[2];
    computeThickLensApproximation(pz, fz);
    std::cout << tfm::format("Cardinal points p'=%f f'=%f, p=%f, f=%f", pz[0],
                             fz[0], pz[1], fz[1])
              << std::endl;

    float f = fz[0] - pz[0];
    float z = -focusDistance;
    float c = (pz[1] - z - pz[0]) * (pz[1] - z - 4 * f - pz[0]);
    if (c < 0) {
      throw NoriException(
          "Coefficient must be positive. Looks like m_focusDistance %f is too "
          "short for the given lens configuration",
          m_focus_distance);
    }
    float delta = 0.5f * (pz[1] - z + pz[0] - std::sqrt(c));
    return m_lensElements.back().thickness + delta;
  }

  BoundingBox2f boundExitPupil(const float posFilmX0,
                               const float posFilmX1) const {
    BoundingBox2f pupilBounds;
    const int numSamples = 1024 * 1024;
    int numExitingRays = 0;
    float rearRadius = rearElementRadius();
    constexpr float boundScale = 1.5f;
    BoundingBox2f projRearBounds(
        Point2f(-boundScale * rearRadius, -boundScale * rearRadius),
        Point2f(boundScale * rearRadius, boundScale * rearRadius));
    // find the x location of the sample plane
    for (int i = 0; i < numSamples; i++) {
      Point3f posFilm(lerp((i + 0.5f) / numSamples, posFilmX0, posFilmX1), 0,
                      0);
      float u[2] = {RadicalInverse::get(0, i), RadicalInverse::get(1, i)};
      Point3f posRear(
          lerp(u[0], projRearBounds.min.x(), projRearBounds.max.x()),
          lerp(u[1], projRearBounds.min.y(), projRearBounds.max.y()),
          lensRearZ());
      Ray3f dummy;
      if (pupilBounds.contains(Point2f(posRear.x(), posRear.y())) ||
          traceLensFromFilm(Ray3f(posFilm, posRear - posFilm), dummy)) {
        pupilBounds.expandBy(Point2f(posRear.x(), posRear.y()));
        numExitingRays++;
      }
    }
    // if no rays make it through the lens
    if (numExitingRays == 0) {
      std::cout << "Unable to find exit pupil" << std::endl;
      return projRearBounds;
    }

    // expand the final bonds by the spacing between samples
    pupilBounds.expandBy(2 * projRearBounds.getExtents().norm() /
                         std::sqrt(numSamples));
    return pupilBounds;
  }

  Point3f sampleExitPupil(const Point2f &posFilm, const Point2f &lensSample,
                          float *sampleBoundsArea) const {
    // find the exit pupil bound for a sample distance from the center
    float radiusFilm =
        std::sqrt(posFilm.x() * posFilm.x() + posFilm.y() * posFilm.y());
    int rIndex = radiusFilm / (m_film->diagonal / 2) * m_exitPupilBounds.size();
    rIndex = std::min(static_cast<int>(m_exitPupilBounds.size()) - 1, rIndex);
    BoundingBox2f bounds = m_exitPupilBounds[rIndex];
    if (sampleBoundsArea) {
      *sampleBoundsArea = bounds.getVolume();
    }
    // sample the exit bounds
    Point2f posLens = bounds.lerp(lensSample);

    // rotate the sample back
    float sinTheta = (radiusFilm != 0) ? posFilm.y() / radiusFilm : 0;
    float cosTheta = (radiusFilm != 0) ? posFilm.x() / radiusFilm : 1;
    return Point3f(cosTheta * posLens.x() - sinTheta * posLens.y(),
                   sinTheta * posLens.x() + cosTheta * posLens.y(),
                   lensRearZ());
  }

  int sampleReuse(const float &u, const int bins, float &u_rescaled) const {
    int bin_idx = std::min(std::max(0, static_cast<int>(bins * u)), bins - 1);
    u_rescaled = bins * (u - static_cast<float>(bin_idx) / bins);
    return bin_idx;
  }

  virtual Color3f sampleRay(Ray3f &ray, const Point2f &samplePosition,
                            const Point2f &apertureSample) const override {
    Point2f pFilm = samplePosition.cwiseProduct(m_invOutputSize);
    Point2f posFilm2 = m_film->getPhysicalExtent().lerp(pFilm);

    Point2f sample = apertureSample;
    int offset = sampleReuse(sample.x(), 3, sample.x());
    Point2f halfOutputSize = m_outputSize.cast<float>() / 2;
    Point2f offsetDir =
        m_apertureOffset * offset *
        (samplePosition - halfOutputSize).cwiseProduct(0.5f * m_invOutputSize);
    float aspect = (float)m_outputSize.y() / m_outputSize.x();
    offsetDir.y() *= -aspect;

    // trace a ray from the film through the lens system
    Point3f posFilm = Point3f(-posFilm2.x(), posFilm2.y(), 0);
    if (m_apertureOffset > 0.f) {
      posFilm += Point3f(offsetDir.x(), offsetDir.y(), 0);
    }
    float exitPupilBoundsArea;
    Point3f posRear = sampleExitPupil(Point2f(posFilm.x(), posFilm.y()), sample,
                                      &exitPupilBoundsArea);
    Ray3f rayFilm(posFilm, (posRear - posFilm).normalized());
    if (!traceLensFromFilm(rayFilm, ray)) {
      return Color3f::Zero();
    }

    // transform the camera ray to world space
    float invZ = 1.f / ray.d.z();
    ray = m_cameraToWorld * ray;
    ray.mint = m_nearClip * invZ;
    ray.maxt = m_farClip * invZ;
    ray.update();

    Color3f weight(Color3f::Ones());
    if (m_apertureOffset > 0.f) {
      switch (offset) {
        case 0:
          weight = weight.cwiseProduct(Color3f(3.0f, 0.f, 0.f));
          break;
        case 1:
          weight = weight.cwiseProduct(Color3f(0.f, 3.f, 0.f));
          break;
        case 2:
          weight = weight.cwiseProduct(Color3f(0.f, 0.f, 3.f));
          break;
        default:
          weight = Color3f::Zero();
          break;
      }
    }
    return weight;
  }

  virtual std::string toString() const override {
    return tfm::format(
        "RealisticCamera{\n"
        "  fov=%d",
        m_fov);
  }

 protected:
  Vector2f m_invOutputSize;
  Transform m_cameraToWorld;

  float m_fov;
  float m_nearClip, m_farClip;
  float m_focus_distance, m_apertureDiameter;

  float m_apertureOffset;

  // parsed lens description file
  std::vector<LensElement> m_lensElements;
  // cached exit pupil bounds
  std::vector<BoundingBox2f> m_exitPupilBounds;

  // film
  std::unique_ptr<Film> m_film;
};

NORI_REGISTER_CLASS(RealisticCamera, "realistic")

NORI_NAMESPACE_END