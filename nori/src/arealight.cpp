/*
    This file is part of Nori, a simple educational ray tracer

    Copyright (c) 2015 by Romain Prévost

    Nori is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    Nori is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <nori/arealight.h>

NORI_NAMESPACE_BEGIN

Color3f AreaEmitter::eval(const EmitterQueryRecord &lRec) const {
  if (!m_shape)
    throw NoriException("There is no shape attached to this Area light!");

  // since wi is defined as pointing towards the emitter, the surface
  // is visible if n and wi point in **opposite** directions
  return lRec.n.dot(lRec.wi) <= 0 ? m_radiance : Color3f::Zero();
}

Color3f AreaEmitter::sample(EmitterQueryRecord &lRec,
                            const Point2f &sample) const {
  if (!m_shape)
    throw NoriException("There is no shape attached to this Area light!");
  // Only lRec.ref is set

  // check for back-facing surfaces

  ShapeQueryRecord sRec{lRec.ref};
  // randomly sample a point on the surface
  m_shape->sampleSurface(sRec, sample);

  // update the EmitterQueryRecord
  lRec.n = sRec.n;
  lRec.p = sRec.p;

  // wi points form the hit point to the emitter point
  Vector3f wi = lRec.p - lRec.ref;
  const float d = wi.norm();
  wi /= d;
  lRec.wi = wi;
  lRec.shadowRay = Ray3f(lRec.ref, lRec.wi, Epsilon, d - Epsilon);

  lRec.pdf = pdf(lRec);
  if (lRec.pdf == 0) {
    return Color3f::Zero();
  }
  return eval(lRec) / lRec.pdf;
}

float AreaEmitter::pdf(const EmitterQueryRecord &lRec) const {
  if (!m_shape)
    throw NoriException("There is no shape attached to this Area light!");

  if (eval(lRec).isZero()) {
    return 0.f;
  }
  ShapeQueryRecord sRec{lRec.ref, lRec.p};
  const float cos_theta = (-lRec.wi).dot(lRec.n);
  const float d_sq = (lRec.p - lRec.ref).squaredNorm();
  // convert area measure to solid angles
  return m_shape->pdfSurface(sRec) * d_sq / cos_theta;
}

Color3f AreaEmitter::samplePhoton(Ray3f &ray, const Point2f &sample1,
                                  const Point2f &sample2) const {

  ShapeQueryRecord sRec{ray.o}; // TODO; what is ref here?
  m_shape->sampleSurface(sRec, sample1);

  // update the ray
  // choose cos weighted direction
  const Point3f dir = Warp::squareToCosineHemisphere(sample2);
  ray = Ray3f(sRec.p, Frame(sRec.n).toWorld(dir));
  if (sRec.pdf <= 0.f) {
    return Color3f::Zero();
  }

  // Assuming uniform sampling of the surface here
  const float area = 1.f / sRec.pdf; // TODO: is this correct?
  // EmitterQueryRecord lRec{sRec.p + dir, sRec.p, sRec.n};
  return M_PI * area * m_radiance;
}

EmitterBounds AreaEmitter::bounds() const {
  if (!m_shape)
    throw NoriException("There is no shape attached to this Area light!");

  // compute normal bounds
  // we know that the attached shape of this emitter is a mesh

  // TODO: this is a very dirty hack
  Triangle *triangle = static_cast<Triangle *>(m_shape);

  ShapeQueryRecord sRec;
  float area = 1.f / m_shape->pdfSurface(sRec);
  float phi = M_PI * area * m_radiance.maxCoeff();
  BoundingBox3f bbox = triangle->getBoundingBox(0);
  Vector3f n = triangle->getNormal();
  return EmitterBounds(bbox, n, phi, 1, 0, true);
}

std::string AreaEmitter::toString() const {
  return tfm::format("AreaLight[\n"
                     "  radiance = %s,\n"
                     "]",
                     m_radiance.toString());
}

NORI_REGISTER_CLASS(AreaEmitter, "area")
NORI_NAMESPACE_END