#include "nori/media.h"

NORI_NAMESPACE_BEGIN

void Medium::activate()
{
	if (!m_PhaseFunction)
		m_PhaseFunction = static_cast<PhaseFunction*>(NoriObjectFactory::createInstance("isotropic", PropertyList()));
}


void Medium::addChild(NoriObject* child) 
{
	switch (child->getClassType()) {
		case EPhaseFunction:
			if (m_PhaseFunction != nullptr)
				throw NoriException("Medium: Trying to attach multiple phase functions");
			m_PhaseFunction = static_cast<PhaseFunction*>(child);
			break;
		default: throw NoriException("Shape::addChild(<%s>) is not supported!",
			classTypeName(child->getClassType()));
	}
}




class HomogeneousMedium: public Medium
{
public:
	HomogeneousMedium(PropertyList const& propList)
	{
		m_sigma_a = propList.getColor("sigma_a", Color3f(0.1f));
		m_sigma_s = propList.getColor("sigma_s", Color3f(0.1f));
		m_sigma_t = m_sigma_a + m_sigma_s;

		m_Emission = propList.getColor("emission", Color3f::Zero());
		m_isEmissive = !m_Emission.isZero();

		Point3f minBbox = propList.getPoint3("box_min", Point3f::Zero());
		Point3f maxBbox = propList.getPoint3("box_max", Point3f::Zero());

		m_bbox = BoundingBox3f(minBbox, maxBbox);
	}

	Color3f eval(MediumQueryRecord const& mRec, Sampler* sampler) const override
	{
		const float l = mRec.passingRay.maxt;
		return exp(-l*m_sigma_t);
	}

	Color3f evalEmission(MediumQueryRecord const& mRec, Sampler* sampler) const override
	{	
		if(!isEmissive())
			return 0.0f;

		return (m_Emission/m_sigma_t)*(1-eval(mRec, sampler)); 
	}

	Color3f sample(MediumQueryRecord& mRec, Sampler* sampler) const override
	{
		int channel = std::min(int(sampler->next1D() * 3), 2);

		float l = -log(1 - sampler->next1D()) / m_sigma_t[channel];
		mRec.t = std::max(0.f,std::min(l, mRec.passingRay.maxt));

		mRec.sampledMedium = l < mRec.passingRay.maxt;
		mRec.passingRay.maxt = mRec.t;

		Color3f tr = exp(-m_sigma_t*mRec.t);
		Color3f val;
		Color3f density;
		if(mRec.sampledMedium)
		{
			density = m_sigma_t * tr;
			val = tr * m_sigma_s;
		}
		else
		{
			density = tr;
			val = tr;
		}

		float pdf = density.mean();
		return val/pdf;
	}

	bool isEmissive() const override
	{
		return m_isEmissive;
	}

	std::string toString() const override
	{
		return tfm::format("Homogeneous medium: [\n"
			"sigma_a = %s"
			"sigma_s = %s"
			"sigma_t = %s",
			m_sigma_a.toString(),
			m_sigma_s.toString(),
			m_sigma_t.toString());
	}

private:
	Color3f m_sigma_a;
	Color3f m_sigma_s;
	Color3f m_sigma_t;

	bool m_isEmissive;
	Color3f m_Emission;

};


class HeterogeneousMedium: public Medium
{
public:
	HeterogeneousMedium(PropertyList const& props)
	{
		m_sigma_a = props.getColor("sigma_a", 0.5f);
		m_sigma_s = props.getColor("sigma_s", 0.5f);

		if(props.has("emissive_color") || props.has("emissive_intensity"))
		{
			
			m_isEmissive == true;
		}
		m_emissiveColor = props.getColor("emissive_color", Color3f::Ones());
		
		
		
		Color3f sigma_t = m_sigma_a + m_sigma_s;
		if (!(sigma_t - sigma_t[0]).isZero())
		{
			throw NoriException("Medium Error: sigma_t = sigma_a + sigma_s is not monochromatic!");
		}
		m_sigma_t = sigma_t[0];
	}

	virtual void addChild(NoriObject* child)
	{
		if (child->getClassType() == EVolume)
		{
			if (child->getIdName() == "density") {
				density = dynamic_cast<Volume<float>*>(child);
				if (density == nullptr)
					throw NoriException("Invalid type for density");
			}
			else if (child->getIdName() == "emissive_intensity") {
				m_EmissiveIntensity = dynamic_cast<Volume<float>*>(child);
				if (m_EmissiveIntensity == nullptr)
					throw NoriException("Invalid type for emissive_color");
				m_isEmissive = true;
			}
			else{
				throw NoriException("unknown Property");
			}
		}
		else
			Medium::addChild(child);
	}

	bool isEmissive() const override{
		return m_isEmissive;
	}


	Color3f eval(MediumQueryRecord const& mRec, Sampler* sampler) const
	{
		float transmittance(1.0f);

		//Ratio tracking to estimate the transmittance in the interval [tmin,tmax]
		float t = mRec.passingRay.mint;
		Point3f p = mRec.passingRay(t);
		int channel;
		float invMaxDensity = 1.f/density->maxValue();

		while (true)
		{
			t -= std::log(1 - sampler->next1D()) * invMaxDensity/m_sigma_t;
			p = mRec.passingRay(t);
			if (t >= mRec.passingRay.maxt || transmittance<Epsilon || density->isOutOfBounds(p))
				break;
			
			transmittance *= 1 - std::max(0.0f, density->eval(p) * invMaxDensity);
		}


		return Color3f(transmittance);
	}

	Color3f evalEmission(MediumQueryRecord const& mRec, Sampler* sampler) const{
		if(!m_isEmissive)
			return Color3f(0.0f);

		float transmittance(1.0f);

		//Ratio tracking to estimate the transmittance in the interval [tmin,tmax]
		float t = mRec.passingRay.mint;
		Point3f p = mRec.passingRay(t);
		float invMaxDensity = 1.f/density->maxValue();
		Color3f Le = Color3f::Zero();

		while (true)
		{
			t -= std::log(1 - sampler->next1D()) * invMaxDensity/m_sigma_t;
			p = mRec.passingRay(t);
			if (t >= mRec.passingRay.maxt || transmittance<Epsilon || density->isOutOfBounds(p) || m_EmissiveIntensity->isOutOfBounds(p))
				break;
			transmittance *= 1 - std::max(0.0f, density->eval(p) * invMaxDensity);
			Le+=transmittance*m_emissiveColor*m_EmissiveIntensity->eval(p);
		}


		return Le;
	}

	Color3f sample(MediumQueryRecord& mRec, Sampler* sampler) const
	{
		float t = mRec.passingRay.mint;
		float invMaxDensity = 1.f/density->maxValue();
		Point3f p;
		
		float maxt;
		bool intersect = density->rayIntersect(mRec.passingRay,maxt);
		maxt = std::max(maxt, mRec.passingRay.maxt);
		while (intersect)
		{
			t -= std::log(1 - sampler->next1D()) * invMaxDensity / m_sigma_t;
			p = mRec.passingRay(t);
			if (t >= maxt )
				break;
			if (density->eval(p) * invMaxDensity > sampler->next1D())
			{
				mRec.t = t;
				mRec.sampledMedium = true;
				mRec.passingRay.maxt = t;
				return m_sigma_s / m_sigma_t;
			}


		}
		mRec.t = mRec.passingRay.maxt;
		mRec.sampledMedium = false;
		return Color3f(1.0f);
	}

	virtual std::string toString() const override
	{
		std::string emissive = "";
		if(m_isEmissive)
			emissive = tfm::format("emissive_color = %s ,\n"
				"emissive_intensity = \n %s \n",
				indent(m_emissiveColor.toString()),
				indent(m_EmissiveIntensity->toString()));

		return tfm::format("Heterogeneous medium: [\n,"
		" sigma_s  =  %s ,\n"
		" sigma_a =  %s ,\n"
		" density : \n %s\n"
		" is emissive : %b \n %s"
		"]" ,
		indent(m_sigma_s.toString()),
		indent(m_sigma_a.toString()),
		indent(density->toString()),
		m_isEmissive,
		emissive
		);
	}

protected:

	Volume<float>* density;

	float m_sigma_t;	
	Color3f m_sigma_s;
	Color3f m_sigma_a;

	bool m_isEmissive = false;
	Color3f m_emissiveColor;
	Volume<float>* m_EmissiveIntensity;
};




NORI_REGISTER_CLASS(HomogeneousMedium, "homogeneous");
NORI_REGISTER_CLASS(HeterogeneousMedium, "heterogeneous");
NORI_NAMESPACE_END