/*
 This file immplements the Disney BRDF (see <https://media.disneyanimation.com/uploads/production/publication_asset/48/asset/s2012_pbs_disney_brdf_notes_v3.pdf>)
 It is mainly based on the microfacet model thus uses the same mechanisms
 The implemented parameters are:
  -Albedo
  -metallic
  -specular
  -roughness
  -anisotropic
  -clearcoat
  -clearcoat gloss
 */

#include <nori/bsdf.h>
#include <nori/texture.h>
#include <nori/frame.h>
#include <nori/warp.h>
#include <lodepng/lodepng.h>

NORI_NAMESPACE_BEGIN

class DisneyBSDF : public BSDF {
public:
    DisneyBSDF(const PropertyList& propList) {
        /* Disney parameters */
        m_alpha = propList.getFloat("alpha", 0.1f);

        /* Interior IOR (default: BK7 borosilicate optical glass) */
        m_intIOR = propList.getFloat("intIOR", 1.5046f);

        /* Exterior IOR (default: air) */
        m_extIOR = propList.getFloat("extIOR", 1.000277f);

        /* Albedo of the diffuse base material (a.k.a "kd") */
        m_kd = propList.getColor("kd", Color3f(0.5f));

        /* To ensure energy conservation, we must scale the
           specular component by 1-kd.

           While that is not a particularly realistic model of what
           happens in reality, this will greatly simplify the
           implementation. Please see the course staff if you're
           interested in implementing a more realistic version
           of this BRDF. */
        m_ks = 1 - m_kd.maxCoeff();
    }

    void addChild(NoriObject* child) override
    {
	    switch(child->getClassType())
	    {
			case ETexture:
                if(child->getIdName() == "albedo"){
                    if (m_albedo)
                        throw NoriException("There is already an albedo defined!");
                    m_albedo = static_cast<Texture<Color3f> *>(child);
                }
                else if (child->getIdName() == "metallic")
                {
                    if (m_metallic)
                        throw NoriException("There is already a metallic texture defined!");
                    m_metallic = static_cast<Texture<float> *>(child);

                }
                else if (child->getIdName() == "specular")
                {
                    if (m_specular)
                        throw NoriException("There is already a specular texture defined!");
                    m_specular = static_cast<Texture<float> *>(child);

                }
                else if (child->getIdName() == "roughness")
                {
                    if (m_roughness)
                        throw NoriException("There is already a roughness texture defined!");
                    m_roughness = static_cast<Texture<float> *>(child);

                }
                else if (child->getIdName() == "anisotropic")
                {
                    if (m_anisotropic)
                        throw NoriException("There is already an anisotropic texture defined!");
                    m_anisotropic = static_cast<Texture<float> *>(child);

                }
                else if(child->getIdName() == "clearcoat")
                {
                    if (m_clearcoat)
                        throw NoriException("There is already a clearcoat texture defined!");
                    m_clearcoat = static_cast<Texture<float> *>(child);
	                
                }
                else if (child->getIdName() == "clearcoatgloss")
                {
                    if (m_clearcoatGloss)
                        throw NoriException("There is already clearcoatGloss texture defined!");
                    m_clearcoatGloss = static_cast<Texture<float> *>(child);

                }
                break;
            default:
                throw NoriException("DisneyBSDF::addChild(<%s>) is not supported!",
                    classTypeName(child->getClassType()));
	    }
    }

    virtual void activate() override
    {
	    if(!m_albedo)
            m_albedo = LoadDefaultColorTexture();
        if (!m_metallic)
            m_metallic = LoadDefaultScalarTexture();
        if (!m_specular)
            m_specular = LoadDefaultScalarTexture();
        if (!m_roughness)
            m_roughness = LoadDefaultScalarTexture();
        if (!m_anisotropic)
            m_anisotropic = LoadDefaultScalarTexture();
        if (!m_clearcoat)
            m_clearcoat = LoadDefaultScalarTexture();
        if (!m_clearcoatGloss)
            m_clearcoatGloss = LoadDefaultScalarTexture();
    }

    //Evaluate the GGX distribution with coefficients ax and ay
    float evalGGX(const Vector3f wh,  const float& ax, const float& ay) const
    {
        float cosTheta = Frame::cosTheta(wh);
        if (cosTheta < 0.f)
            return 0.f;

        float termincos = wh.x()*wh.x() / (ax * ax); //wh.x = sinTheta*cosPhi
    	float terminsin = wh.y()*wh.y()/ (ay * ay); //wh.y = sinTheta*sinPhi
        return INV_PI / ((ax * ay) * pow((termincos+terminsin) + cosTheta*cosTheta, 2));
    }

    //Evaluate the isotropic Generalized Trowbridge-Reitz distribution with gamma 1 and coefficient alpha
    float evalGTR1(const Vector3f wh, const float& alpha) const
    {
        float cosTheta2 = Frame::cosTheta(wh) * Frame::cosTheta(wh);
        float a2 = alpha * alpha;
        float alphaterm = (a2 - 1) / log(a2);
        return INV_PI * alphaterm / (1 + (a2 - 1) * cosTheta2);
    }

    /// Evaluate Smith's shadowing-masking function G1
    float smithBeckmannG1(const Vector3f& v, const Normal3f& m, float alpha) const {
        float tanTheta = Frame::tanTheta(v);

        /* Perpendicular incidence -- no shadowing/masking */
        if (tanTheta == 0.0f)
            return 1.0f;

        /* Can't see the back side from the front and vice versa */
        if (m.dot(v) * Frame::cosTheta(v) <= 0)
            return 0.0f;

        float a = 1.0f / (alpha * tanTheta);
        if (a >= 1.6f)
            return 1.0f;
        float a2 = a * a;

        /* Use a fast and accurate (<0.35% rel. error) rational
           approximation to the shadowing-masking function */
        return (3.535f * a + 2.181f * a2) / (1.0f + 2.276f * a + 2.577f * a2);
    }

    //Schlick's approximation of the Fresnel equations (faster while still low error)
    float SchlickApprox(float CosThetaI, float R0) const
    {
        if(CosThetaI < 0.f)
        {
        	CosThetaI = -CosThetaI;
        }
        return R0 + (1.f - R0) * pow(1.f - CosThetaI, 5.f);
    }

    Color3f SchlickApprox(float CosThetaI, Color3f R0) const
    {
        if (CosThetaI < 0.f)
        {
            CosThetaI = -CosThetaI;
        }
        return R0 + (1.f - R0) * pow(1.f - CosThetaI, 5.f);
    }

    //The advanced shadowing function for the GGX microfacet (we use G = 1/(1+Lambda(wo) + Lambda(wi))
    // see <https://www.pbr-book.org/3ed-2018/Reflection_Models/Microfacet_Models>
    float TrowbridgeReitzVisibilityG(Vector3f wi, Vector3f wo, Normal3f wh, const float& ax, const float& ay) const
    {
        if (wh.dot(wi) < 0.f || wh.dot(wo) < 0.f)
            return 0.f;
        const float alphaI = getIsotropicAlpha2(wi, ax, ay);
        const float alphaO = getIsotropicAlpha2(wo, ax, ay);
        return 2.f / (sqrt(1 + alphaI * Frame::tanTheta(wi) * Frame::tanTheta(wi)) + sqrt(1 + alphaI * Frame::tanTheta(wo) * Frame::tanTheta(wo)));
    }

    //The shadowing function for one direction of the GGX microfacet distribution (G = 1 / ( 1+Lambda(w) ) )
    float SingleGGX_G(Vector3f w, const float& alpha) const 
    {
        return 2 / (1 + sqrt(1 + alpha * alpha * Frame::tanTheta(w) * Frame::tanTheta(w)));
    }

    float modulo(float a, float b) const//modulo of 2 floats, needs b to be > 0
    {
        int nSubstractions = std::floor(a / b);
        return a - b * nSubstractions;
    }

    //Return the isotropic version of alphax and alphay for the ggx distribution
    float getIsotropicAlpha2(Vector3f w, const float& ax, const float& ay) const
    {
        return Frame::cosPhi2(w) *(ax * ax) + Frame::sinPhi2(w) *(ay * ay);
    }

    //Computes together the dielectric and the metallic response of the BRDF
    Color3f dielectricMetallicEval(const BSDFQueryRecord& bRec, const Vector3f& w_h, const Color3f& albedo, const float& roughness, const float& specular, const float& metallic, const float& alphax, const float& alphay) const
    {
        //Diffuse//


        float F90 = 0.5f + 2.f * roughness * pow(w_h.dot(bRec.wi), 2.f);
        Color3f diffuse = albedo * INV_PI * (1 + (F90 - 1) * pow(1 - Frame::cosTheta(bRec.wo), 5.f))
            * (1 + (F90 - 1) * pow(1 - Frame::cosTheta(bRec.wi), 5.f));


        //Specular//
        Color3f specularintensity;
        /*if (abs(Frame::cosTheta(bRec.wi))< Epsilon)
        {
            //90� angle, the ray is absorbed by the microfacets;
            return (1-metallic)*diffuse;
        }*/

        const float G = TrowbridgeReitzVisibilityG(bRec.wi, bRec.wo, w_h, alphax, alphay);
        const float D = evalGGX(w_h, alphax, alphay);
        Color3f FColor = metallic * albedo + (1 - metallic)*specular;
        const Color3f F = SchlickApprox(w_h.dot(bRec.wi), FColor);
        
        const float norm =
            4.f * Frame::cosTheta(bRec.wo) * Frame::cosTheta(bRec.wi);
        specularintensity = D * F * G / norm;

        //Total
        return (1-metallic)*diffuse + specularintensity;
    }

    //The evaluation of the clearcoat layer of our material. A very ad-hoc layer, mainly for artistic purpose
    //Uses the long tailed GTR1 distribution and an ad-hoc shadowing factor
    Color3f clearcoatEval(const BSDFQueryRecord& bRec, const Vector3f& w_h,  const float& ClearcoatGloss) const
    {
        // IOR = 1.5 -> R0 = 0.04 (R0 = (IOR - 1)^2/(IOR + 1)^2 )
        const float F = SchlickApprox(w_h.dot(bRec.wi), 0.04); 

        //Ad-Hoc choice from the author of the paper, we may try to use the same as for the specular case instead
        //see <https://github.com/wdas/brdf/blob/main/src/brdfs/disney.brdf> for the implementation from the authors
        const float G = SingleGGX_G(bRec.wi, .25) * SingleGGX_G(bRec.wo, .25);

        float alphaClearcoat = .001 + ClearcoatGloss * (.1 - 0.001); //Okay...  to keep computations safe
        const float D = evalGTR1(w_h, alphaClearcoat);
        const float pdf = G * F * D;
        const float norm = 4.f* Frame::cosTheta(bRec.wi) * Frame::cosTheta(bRec.wo);
        return Color3f(pdf/norm);
    }

    /// Evaluate the BRDF for the given pair of directions
    virtual Color3f eval(const BSDFQueryRecord& bRec) const override {
        
        if (Frame::cosTheta(bRec.wo) <= 0.f || Frame::cosTheta(bRec.wi) <= 0.f)
            return 0.f;
        const auto uv = Point2f(modulo(bRec.uv.x(), 1), modulo(bRec.uv.y(), 1));
        const Color3f albedo = m_albedo->eval(uv);
        const float metallic = std::max(0.f, std::min(1.f, m_metallic->eval(uv)));
        const float specular = m_specular->eval(uv)*0.08f;
        const float roughness = .01f+ (1.f - .01f)*m_roughness->eval(uv);//To make sure the distributions evaluation doesn't explode (roughness > 0 but not neccessarily < 1.)
        const float anisotropic = m_anisotropic->eval(uv);
        const float clearcoat = m_clearcoat->eval(uv);
        const float clearcoatGloss = m_clearcoatGloss->eval(uv);

        float alpha = roughness * roughness;
        float aspect = sqrt(1.f - .9f * anisotropic);//See the paper for this choice
        float alphax = alpha / aspect;
        float alphay = alpha * aspect;
    	
        const Vector3f w_h = (bRec.wi + bRec.wo).normalized();

        //Computation of the default layers
        Color3f result = dielectricMetallicEval(bRec, w_h, albedo, roughness, specular, metallic, alphax, alphay);

        //Computation of the clearcoat layer
        if (clearcoat == 0.f)
            return result;
        return result +.25f*clearcoat*clearcoatEval(bRec, w_h, clearcoatGloss);  // The clearcoat is a supplementary layer with not much energy
        //so we do not balance the energy it may add
    }

    float pdfDiffuse(const BSDFQueryRecord& bRec) const
    {
        return INV_PI*Frame::cosTheta(bRec.wo);
    }



    float pdfSpecular(const BSDFQueryRecord& bRec, const Normal3f& wh, const float& ax, const float& ay, const float& specular) const
    {
        const float Jh = 4.f * abs(wh.dot(bRec.wo));
        return Warp::squareToGGXPdf(wh, ax,ay)/Jh;
    }

    float pdfClearcoat(const BSDFQueryRecord& bRec, const Normal3f& wh, const float& clearcoatGloss) const
    {
        float alpha = .001f + clearcoatGloss * (.1f - 0.001f);
        const float Jh = 4.f * abs(wh.dot(bRec.wo));
        return Warp::SquareToGTR1Pdf(wh, alpha)/Jh;
    }

    float totalPDF(const BSDFQueryRecord& bRec, const Normal3f& wh,const Color3f& albedo, const float& ax, const float& ay,
        const float& specular, const float& roughness, const float& metallic, float clearcoat, const float& clearcoatGloss) const
    {
        if (Frame::cosTheta(bRec.wo) <= 0 || Frame::cosTheta(bRec.wi) <= 0)
            return 0.f;

        const float iDiff = albedo.maxCoeff();
        const float pDiff = iDiff / (1 + iDiff)*(1-metallic);

        clearcoat *= .25;
        const float pClearcoat = clearcoat / (1 + clearcoat);
        float PdfClearcoat(0.f);
        if(pClearcoat > 0.f)
            PdfClearcoat = pdfClearcoat(bRec, wh, clearcoatGloss);

        return (1-pClearcoat)*((1-pDiff) * pdfSpecular(bRec, wh, ax, ay, specular)
            + pDiff* pdfDiffuse(bRec)) + pClearcoat*PdfClearcoat;
    }

    /// Evaluate the sampling density of \ref sample() wrt. solid angles
    virtual float pdf(const BSDFQueryRecord& bRec) const override {
        // assign zero mass to values outside the hemisphere
        const float cos_theta = Frame::cosTheta(bRec.wo);
        if (cos_theta <= 0.f) {
            return 0.f;
        }
        const auto uv = Point2f(modulo(bRec.uv.x(), 1), modulo(bRec.uv.y(), 1));//confine the uv in the [0,1] domain
        const Color3f albedo = m_albedo->eval(uv);
        const float metallic = std::max(0.f,std::min(1.f,m_metallic->eval(uv)));
        const float specular = m_specular->eval(uv) * 0.08f;
        const float roughness = .01f + (1.f - .01f) * m_roughness->eval(uv);
        const float anisotropic = m_anisotropic->eval(uv);
        const float clearcoat = m_clearcoat->eval(uv);
        const float clearcoatGloss = m_clearcoatGloss->eval(uv);

        float alpha = roughness * roughness;
        float aspect = sqrt(1.f - .9f * anisotropic);
        float alphax = alpha / aspect;
        float alphay = alpha * aspect;

        const Vector3f wh = (bRec.wo + bRec.wi).normalized();

        /*const float beckmann = evalBeckmann(wh);
        // conversion to solid angles
        const float Jh = 4.f * wh.dot(bRec.wo);
        return m_ks * beckmann * Frame::cosTheta(wh) / Jh +
            (1.f - m_ks) * cos_theta * INV_PI;*/

        return totalPDF(bRec, wh, albedo, alphax, alphay, specular, roughness, metallic, clearcoat, clearcoatGloss);
    }

	void sampleSpecular(BSDFQueryRecord& bRec, const Point2f& _sample, const float alphax, const float alphay, const float& specular) const
    {
        const Vector3f wh = Warp::squareToGGX(_sample,alphax,alphay);
        bRec.wo = (2.f * wh.dot(bRec.wi) * wh - bRec.wi);
        bRec.eta = (1 - sqrt(specular)/(1 + sqrt(specular)));
    }

    /// Sample the BRDF
    virtual Color3f sample(BSDFQueryRecord& bRec, const Point2f& _sample) const override
    {
        Point2f sample = _sample;
        bRec.measure = ESolidAngle;

        const auto uv = Point2f(modulo(bRec.uv.x(), 1), modulo(bRec.uv.y(), 1));//confine the uv in the [0,1] domain
        const Color3f albedo = m_albedo->eval(uv);
        const float metallic =  m_metallic->eval(uv);
        const float specular = m_specular->eval(uv) * 0.08f;
        const float roughness = .01f + (1.f - .01f) * m_roughness->eval(uv);
        const float anisotropic = m_anisotropic->eval(uv);
        const float clearcoat = m_clearcoat->eval(uv);
        const float clearcoatGloss = m_clearcoatGloss->eval(uv);

        float alpha = roughness * roughness;
        float aspect = sqrt(1.f - .9f * anisotropic);
        float alphax = alpha / aspect;
        float alphay = alpha * aspect;

        float pDiff = albedo.maxCoeff() / (1 + albedo.maxCoeff()) * (1 - metallic);
        float pClearcoat = clearcoat * .25f / (1 + clearcoat * .25f);


        //sample diffuse
        if ( sample.y() < pClearcoat)
        {
            sample.y() /= pClearcoat;

            float alphaClearcoat = .001f + (.1f - .001f) * clearcoatGloss;
            bRec.wo = Warp::squareToGTR1(sample, alphaClearcoat);
            bRec.eta = 1.5f;
        }
        else
        {
            if (clearcoat > 0.f)
            {
                sample.y() = (sample.y() - pClearcoat) / (1 - pClearcoat);
            }
            if (sample.x() < pDiff)
            {
                sample.x() /= pDiff;

                bRec.wo = Warp::squareToCosineHemisphere(sample);
                bRec.eta = 1.f;
            }

            //sample specular (if metallic or (dielectric+specular)
            else
            {
                sample.x() /= (1 - pDiff);



                sampleSpecular(bRec, sample, alphax, alphay, specular);
            }
        }
        float cosTheta = Frame::cosTheta(bRec.wo);
        Vector3f wh = (bRec.wo + bRec.wi).normalized();
        if (cosTheta <= 0.f)
            return Color3f::Zero();
        float Pdf = totalPDF(bRec, wh, albedo, alphax, alphay, specular, roughness, metallic, clearcoat, clearcoatGloss);
        if (Pdf == 0.f)
            return 0.f;

        return eval(bRec) * cosTheta /Pdf ;
    }


    virtual std::string toString() const override {
        return tfm::format("Disney[\n"
            "  albedo = %s ,\n"
            "]",
            m_albedo->toString());
    }

private:

    //Load a default texture 
    Texture<float>* LoadDefaultScalarTexture()
    {
        PropertyList l;
        l.setFloat("value", 0.f);
        Texture<float>* texture = static_cast<Texture<float> *>(NoriObjectFactory::createInstance("constant_float", l));
        texture->activate();
        return texture;
    }
    Texture<Color3f>* LoadDefaultColorTexture()
    {
        PropertyList l;
        l.setColor("value", Color3f(.5f));
        Texture<Color3f>* texture = static_cast<Texture<Color3f> *>(NoriObjectFactory::createInstance("constant_color", l));
        texture->activate();
        return texture;
    }

private:
    Texture<Color3f>* m_albedo = nullptr;

    Texture<float>* m_metallic = nullptr;
    Texture<float>* m_specular = nullptr;
    Texture<float>* m_roughness = nullptr;
	Texture<float>* m_anisotropic = nullptr;

    Texture<float>* m_clearcoat = nullptr;
    Texture<float>* m_clearcoatGloss = nullptr;

    float m_alpha;
    float m_intIOR, m_extIOR;
    float m_ks;
    Color3f m_kd;
};

NORI_REGISTER_CLASS(DisneyBSDF, "disneybsdf");
NORI_NAMESPACE_END
