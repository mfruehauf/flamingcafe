#include <nori/distribution.h>

NORI_NAMESPACE_BEGIN

Distribution1D::Distribution1D(const float *f, int n)
    : func(f, f + n), cdf(n + 1) {
  // integrate the cdf from the function values
  cdf[0] = 0.f;
  for (unsigned int i = 1; i < cdf.size(); i++) {
    cdf[i] = cdf[i - 1] + func[i - 1] / static_cast<float>(n);
  }

  // normalize the distribution
  func_int = cdf[n];
  if (func_int == 0.f) {
    for (int i = 1; i < n + 1; i++) {
      cdf[i] = static_cast<float>(i) / static_cast<float>(n);
    }
  } else {
    for (int i = 1; i < n + 1; i++) {
      cdf[i] /= func_int;
    }
  }
}

template <typename Predicate>
int findInterval(int size, const Predicate &pred) {
  int first = 0, len = size;
  while (len > 0) {
    int half = len >> 1, middle = first + half;
    if (pred(middle)) {
      first = middle + 1;
      len -= half + 1;
    } else {
      len = half;
    }
  }
  return clamp(first - 1, 0, size - 2);
}

int Distribution1D::getOffset(const float u) const {
  return findInterval(cdf.size(), [&](int idx) { return cdf[idx] <= u; });
}

float Distribution1D::sampleContinuous(const float u, float *pdf,
                                       int *pos) const {
  int offset = getOffset(u);
  if (pos != nullptr) {
    *pos = offset;
  }
  float du = u - cdf[offset];
  if ((cdf[offset + 1] - cdf[offset]) > 0.f) {
    du /= cdf[offset + 1] - cdf[offset];
  }
  if (pdf)
    *pdf = func[offset] / func_int;
  return (offset + du) / static_cast<float>(count());
}

int Distribution1D::sampleDiscrete(const float u, float *pdf,
                                     float *u_remapped) const {
  int offset = getOffset(u);
  if (pdf) {
    *pdf = func[offset] / (func_int * func.size());
  }
  if (u_remapped) {
    *u_remapped = (u - cdf[offset]) / (cdf[offset + 1] - cdf[offset]);
  }
  return offset;
}

Distribution2D::Distribution2D(const float *data, const int rows,
                               const int cols) {
  for (int i = 0; i < rows; i++) {
    m_conditionals.emplace_back(new Distribution1D(&data[i * cols], cols));
  }
  std::vector<float> marginals;
  for (int i = 0; i < rows; i++) {
    marginals.push_back(m_conditionals[i]->func_int);
  }
  m_marginal = std::unique_ptr<Distribution1D>(
      new Distribution1D(marginals.data(), rows));
}

Point2f Distribution2D::sampleContinuous(const Point2f &u, float *pdf) const {
  float pdf0, pdf1;
  int v;
  float d1 = m_marginal->sampleContinuous(u.y(), &pdf1, &v);
  float d0 = m_conditionals[v]->sampleContinuous(u.x(), &pdf0);
  *pdf = pdf0 * pdf1;
  return Point2f{d0, d1};
}

float Distribution2D::pdf(const Point2f &u) const {
  int iu = clamp(static_cast<int>(u.x() * m_conditionals[0]->count()), 0,
                 m_conditionals[0]->count() - 1);
  int iv = clamp(static_cast<int>(u.y() * m_marginal->count()), 0,
                 m_marginal->count() - 1);
  return m_conditionals[iv]->func[iu] / m_marginal->func_int;
}

NORI_NAMESPACE_END