#include <nori/bsdf.h>

NORI_NAMESPACE_BEGIN

class blankBSDF:public BSDF
{
public:
	blankBSDF(const PropertyList &propList){}
	Color3f eval(const BSDFQueryRecord& bRec) const override { return Color3f::Ones() * (bRec.wi + bRec.wo).isZero(); }
	Color3f sample(BSDFQueryRecord& bRec, const Point2f& sample) const override
	{
		bRec.wo = -bRec.wi;
		bRec.measure = EDiscrete;
		return Color3f::Ones();
	}
	float pdf(const BSDFQueryRecord& bRec) const override { return 0.0f; }

	bool isDiffuse() const override { return false; }
	bool isTranslucent() const override { return true; }

	std::string toString() const override
	{
		return tfm::format("blank bsdf[]");
	}
};

NORI_REGISTER_CLASS(blankBSDF,"blank");
NORI_NAMESPACE_END