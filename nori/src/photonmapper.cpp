/*
    This file is part of Nori, a simple educational ray tracer

    Copyright (c) 2015 by Wenzel Jakob

    Nori is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    Nori is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <nori/bsdf.h>
#include <nori/emitter.h>
#include <nori/integrator.h>
#include <nori/photon.h>
#include <nori/sampler.h>
#include <nori/scene.h>

NORI_NAMESPACE_BEGIN

class PhotonMapper : public Integrator {
public:
  /// Photon map data structure
  typedef PointKDTree<Photon> PhotonMap;

  PhotonMapper(const PropertyList &props) {
    /* Lookup parameters */
    m_photonCount = props.getInteger("photonCount", 1000000);
    m_photonRadius =
        props.getFloat("photonRadius", 0.0f /* Default: automatic */);
  }

  bool inline russianRoulette(const float sample,
                              const float success_prob) const {
    return sample > success_prob;
  }

  const Color3f sampleRandomPhoton(const Scene *scene, Ray3f &ray,
                                   Sampler *sampler) const {
    const Emitter *emitter = scene->getRandomEmitter(sampler->next1D());
    return emitter->samplePhoton(ray, sampler->next2D(), sampler->next2D()) *
           scene->getLights().size();
  }

  virtual void preprocess(const Scene *scene) override {
    cout << "Gathering " << m_photonCount << " photons .. ";
    cout.flush();

    /* Create a sample generator for the preprocess step */
    Sampler *sampler = static_cast<Sampler *>(
        NoriObjectFactory::createInstance("independent", PropertyList()));

    /* Allocate memory for the photon map */
    m_photonMap = std::unique_ptr<PhotonMap>(new PhotonMap());
    m_photonMap->reserve(m_photonCount);

    /* Estimate a default photon radius */
    if (m_photonRadius == 0)
      m_photonRadius = scene->getBoundingBox().getExtents().norm() / 500.0f;

    /* How to add a photon?
     * m_photonMap->push_back(Photon(
     *	Point3f(0, 0, 0),  // Position
     *	Vector3f(0, 0, 1), // Direction
     *	Color3f(1, 2, 3)   // Power
     * ));
     */

    // put your code to trace photons here
    for (int photons = 0; photons < m_photonCount;) {/*photons is updated only when we register a new photon in the photon map*/
      Ray3f ray{};
      Color3f W = sampleRandomPhoton(scene, ray, sampler);
      m_emittedPhotonCount++;

      Intersection its;
      BSDFQueryRecord bRec{Vector3f::Zero()};
      float success_prob;

      while (true) {
        if (!scene->rayIntersect(ray, its)) {
          break;
        }

        if (its.mesh->getBSDF()->isDiffuse()) {
           m_photonMap->push_back(Photon(its.p, -ray.d, W));
			//Check the number of sampled photons
        	photons++;
        	if(photons >= m_photonCount)
        	{
                break;
        	}
        }

        success_prob = std::min(W.x(), 1.f - Epsilon);
        if (russianRoulette(sampler->next1D(), success_prob)) {
          break;
        } else {
          W /= success_prob;
        }

        bRec = BSDFQueryRecord{its.toLocal(-ray.d)};
        bRec.uv = its.uv;
        bRec.p = its.p;

        // the surface BSDF is already importance sampled
        // f = BSDF * cos(theta) / pdf
        W *= its.mesh->getBSDF()->sample(bRec, sampler->next2D());

        // update the ray for next termination
        ray = Ray3f(its.p, its.toWorld(bRec.wo));
      }
    }

    /* Build the photon map */
    m_photonMap->build();
  }

  virtual Color3f Li(const Scene *scene, Sampler *sampler,
                     const Ray3f &_ray) const override {

    /* How to find photons?
     * std::vector<uint32_t> results;
     * m_photonMap->search(Point3f(0, 0, 0), // lookup position
     *                     m_photonRadius,   // search radius
     *                     results);
     *
     * for (uint32_t i : results) {
     *    const Photon &photon = (*m_photonMap)[i];
     *    cout << "Found photon!" << endl;
     *    cout << " Position  : " << photon.getPosition().toString() << endl;
     *    cout << " Power     : " << photon.getPower().toString() << endl;
     *    cout << " Direction : " << photon.getDirection().toString() << endl;
     * }
     */

    // put your code for path tracing with photon gathering here

    Color3f Li{Color3f::Zero()}, t{Color3f::Ones()};
    Intersection its;
    BSDFQueryRecord bRec{Vector3f::Zero()};
    EmitterQueryRecord lRec;
    float success_prob;
    Ray3f ray = _ray;
    Color3f power{Color3f::Zero()};
    std::vector<uint32_t> results;
    int bounces = 0;
    const float r2 = m_photonRadius * m_photonRadius;
    while (true) {
      if (!scene->rayIntersect(ray, its)) {
        break;
      }
      if (its.mesh != nullptr && its.mesh->isEmitter()) {
        lRec = EmitterQueryRecord(ray.o, its.p, its.shFrame.n);
        Li += t * its.mesh->getEmitter()->eval(lRec);
      }

      if (its.mesh->getBSDF()->isDiffuse()) {
        // sample from the photon distribution
        power = Color3f::Zero();
        results.clear();
        m_photonMap->search(its.p, m_photonRadius, results);
        for (uint32_t i : results) {
          const Photon &photon = (*m_photonMap)[i];
          // both directions have to be in the local frame of the **sampling**
          // intersection
          bRec = BSDFQueryRecord(its.toLocal(-ray.d),
                                 its.toLocal(photon.getDirection()),
                                 EMeasure::ESolidAngle);
          power += its.mesh->getBSDF()->eval(bRec) * photon.getPower();
        }
        Li += t * power * INV_PI / (r2 * m_emittedPhotonCount);
        break;
      }

      success_prob = std::min(t.x(), 1.f - Epsilon);
      // std::cout << success_prob << std::endl;
      if (bounces >= 3) {
          if (bounces >= 3 && russianRoulette(sampler->next1D(), success_prob)) {
              break;
          }
          else {
              t /= success_prob;
          }
      }

      bRec = BSDFQueryRecord{its.toLocal(-ray.d)};
      bRec.uv = its.uv;
      bRec.p = its.p;

      // the surface BSDF is already importance sampled
      // f = BSDF * cos(theta) / pdf
      t *= its.mesh->getBSDF()->sample(bRec, sampler->next2D());

      // update the ray for next termination
      ray = Ray3f(its.p, its.toWorld(bRec.wo));
      bounces++;
    }

    return Li;
  }

  virtual std::string toString() const override {
    return tfm::format("PhotonMapper[\n"
                       "  photonCount = %i,\n"
                       "  photonRadius = %f\n"
                       "]",
                       m_photonCount, m_photonRadius);
  }

private:
  /*
   * Important: m_photonCount is the total number of photons deposited in the
   * photon map, NOT the number of emitted photons. You will need to keep
   * track of those yourself.
   */
  int m_photonCount;
  int m_emittedPhotonCount;
  float m_photonRadius;
  std::unique_ptr<PhotonMap> m_photonMap;
};

NORI_REGISTER_CLASS(PhotonMapper, "photonmapper");
NORI_NAMESPACE_END
