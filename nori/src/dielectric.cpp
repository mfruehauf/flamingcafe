/*
    This file is part of Nori, a simple educational ray tracer

    Copyright (c) 2015 by Wenzel Jakob

    Nori is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    Nori is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <nori/bsdf.h>
#include <nori/common.h>
#include <nori/frame.h>

NORI_NAMESPACE_BEGIN

/// Ideal dielectric BSDF
class Dielectric : public BSDF {
public:
  Dielectric(const PropertyList &propList) {
    /* Interior IOR (default: BK7 borosilicate optical glass) */
    m_intIOR = propList.getFloat("intIOR", 1.5046f);

    /* Exterior IOR (default: air) */
    m_extIOR = propList.getFloat("extIOR", 1.000277f);
  }

  virtual Color3f eval(const BSDFQueryRecord &) const override {
    /* Discrete BRDFs always evaluate to zero in Nori */
    return Color3f(0.0f);
  }

  virtual float pdf(const BSDFQueryRecord &) const override {
    /* Discrete BRDFs always evaluate to zero in Nori */
    return 0.0f;
  }

  virtual Color3f sample(BSDFQueryRecord &bRec,
                         const Point2f &sample) const override {
    const float cos_theta = Frame::cosTheta(bRec.wi);
    const float reflectance = fresnel(cos_theta, m_extIOR, m_intIOR); // [0, 1]

    // Dielectric has a discrete (dirac delta) distributions
    bRec.measure = EDiscrete;

    if (sample.x() < reflectance) { // light is reflected
      // Taken from mirror.cpp
      bRec.wo = Vector3f(-bRec.wi.x(), -bRec.wi.y(), bRec.wi.z());
      // reflecting off the object does not change the ratio of IoR
      bRec.eta = 1.f;

      return Color3f::Ones();
    } else { // light is transmitted
      // TODO: change bi-directional scaling for path tracing
      const bool entering = cos_theta > 0.f;
      float up = entering ? 1.f : -1.f;
      const float etaI = entering ? m_extIOR : m_intIOR;
      const float etaT = entering ? m_intIOR : m_extIOR;
      bRec.eta = etaI / etaT;

      // snells law
      const float cos_t = abs(cos_theta);
      const float a = bRec.eta * cos_t;
      const float b = sqrt(1.f - bRec.eta * bRec.eta * (1.f - cos_t * cos_t));
      bRec.wo = -bRec.eta * bRec.wi + Vector3f(0.f, 0.f, up * (a - b));

      return Color3f::Ones();
    }
  }

  virtual std::string toString() const override {
    return tfm::format("Dielectric[\n"
                       "  intIOR = %f,\n"
                       "  extIOR = %f\n"
                       "]",
                       m_intIOR, m_extIOR);
  }

private:
  float m_intIOR, m_extIOR;
};

NORI_REGISTER_CLASS(Dielectric, "dielectric");
NORI_NAMESPACE_END
