/*
    This file is part of Nori, a simple educational ray tracer

    Copyright (c) 2015 by Romain Prévost

    Nori is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    Nori is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <nori/bsdf.h>
#include <nori/common.h>
#include <nori/emitter.h>
#include <nori/shape.h>
#include <nori/warp.h>

NORI_NAMESPACE_BEGIN

class Sphere : public Shape {
public:
  Sphere(const PropertyList &propList) {
    m_position = propList.getPoint3("center", Point3f());
    m_radius = propList.getFloat("radius", 1.f);

    m_bbox.expandBy(m_position - Vector3f(m_radius));
    m_bbox.expandBy(m_position + Vector3f(m_radius));

    m_InnerMediumName = propList.getString("inside", "");
    m_outerMediumName = propList.getString("outside", "");
  }

  virtual BoundingBox3f getBoundingBox(uint32_t index) const override {
    return m_bbox;
  }

  virtual Point3f getCentroid(uint32_t index) const override {
    return m_position;
  }

  virtual bool rayIntersect(uint32_t index, const Ray3f &ray, float &u,
                            float &v, float &t) const override {
    float r_sq = m_radius * m_radius;
    // cubic polynomial coefficients for the intersection
    float a = ray.d.squaredNorm();
    float b = -2.f * ray.d.dot((m_position - ray.o));
    float c = (m_position - ray.o).squaredNorm() - r_sq;
    // Compute the cubic polynomial discriminant
    // https://en.wikipedia.org/wiki/Discriminant
    float discriminant = b * b - 4.f * a * c;

    // if the roots are non-real there's no intersection
    if (discriminant < 0.f) {
      return false;
    }

    // compute cubic roots
    float root_min = (-b + sqrt(discriminant)) / (2 * a);
    float root_max = (-b - sqrt(discriminant)) / (2 * a);

    // swap roots if they are not correctly ordered
    if (root_max < root_min) {
      float tmp = root_min;
      root_min = root_max;
      root_max = tmp;
    }

    // select the smallest positive & real root that is in the bounds on t
    if (root_min > 0.f && ray.mint <= root_min && root_min <= ray.maxt) {
      t = root_min;
    } else if (root_max > 0.f && ray.mint <= root_max && root_max <= ray.maxt) {
      t = root_max;
    } else {
      return false;
    }
    return true;
  }

  virtual void setHitInformation(uint32_t index, const Ray3f &ray,
                                 Intersection &its) const override {
    /* to be implemented */
    its.p = ray(its.t);
    // The shading frame is defined by the sphere normal at p
    const Vector3f n = (its.p - m_position).normalized();
    its.geoFrame = Frame(n);
    its.shFrame = Frame(n);
    // compute polar coordinates of the sphere.
    /*// https://en.wikipedia.org/wiki/Spherical_coordinate_system#Cartesian_coordinates
    const float u = atan2(n.y(), n.x()) * INV_TWOPI;
    const float v = atan2(n.segment(0, 2).norm(), n.z()) * INV_PI;
    its.uv = Point2f(u, v);*/
    its.uv = sphericalCoordinates(n).cwiseProduct(Vector2f(INV_PI, INV_TWOPI));
    updateNormals(its);

  }

  virtual void sampleSurface(ShapeQueryRecord &sRec,
                             const Point2f &sample) const override {
    Vector3f q = Warp::squareToUniformSphere(sample);
    sRec.p = m_position + m_radius * q;
    sRec.n = q;
    sRec.pdf = std::pow(1.f / m_radius, 2) *
               Warp::squareToUniformSpherePdf(Vector3f(0.0f, 0.0f, 1.0f));
  }
  virtual float pdfSurface(const ShapeQueryRecord &sRec) const override {
    return std::pow(1.f / m_radius, 2.f) *
           Warp::squareToUniformSpherePdf(Vector3f(0.0f, 0.0f, 1.0f));
  }

  virtual std::string toString() const override {
    return tfm::format(
        "Sphere[\n"
        "  center = %s,\n"
        "  radius = %f,\n"
        "  bsdf = %s,\n"
        "  emitter = %s\n"
        "]",
        m_position.toString(), m_radius,
        m_bsdf ? indent(m_bsdf->toString()) : std::string("null"),
        m_emitter ? indent(m_emitter->toString()) : std::string("null"));
  }

protected:
  Point3f m_position;
  float m_radius;
};

NORI_REGISTER_CLASS(Sphere, "sphere");
NORI_NAMESPACE_END
