#include <nori/emitter.h>
#include <sstream>

NORI_NAMESPACE_BEGIN

class PointLight : public Emitter {
public:
  PointLight(const PropertyList &props)
      : m_position(props.getPoint3("position", Point3f(0.f, 0.f, 0.f))),
        m_power(props.getColor("power", Color3f(100.f, 100.f, 100.f))){};

  virtual Color3f sample(EmitterQueryRecord &lRec,
                         const Point2f &sample) const override {

    // update the EmitterQueryRecord
    lRec.p = m_position;
    lRec.n = Vector3f(0.f, 0.f, 1.f);
    lRec.wi = (m_position - lRec.ref).normalized();
    lRec.pdf = pdf(lRec);
    lRec.shadowRay =
        Ray3f(lRec.ref, lRec.wi, Epsilon, (lRec.p - lRec.ref).norm() - Epsilon);
    return eval(lRec) / lRec.pdf;
  }

  virtual Color3f eval(const EmitterQueryRecord &lRec) const override {
    // Emitter value is 1 / || p - p' ||^2
    return m_power / ((m_position - lRec.ref).squaredNorm() * power());
  }

  float power() const { return 4 * M_PI; }

  virtual float pdf(const EmitterQueryRecord &lRec) const override {
    return 1.f;
  }

  virtual EmitterBounds bounds() const override {
    BoundingBox3f bbox{};
    return EmitterBounds(bbox, Vector3f::UnitZ(), m_power.maxCoeff(), -1, 0, false);
  }

  std::string toString() const {
    std::stringstream ss;
    ss << "PointLight[position=" << m_position.toString()
       << ", power=" << m_power.toString() << "]";
    return ss.str();
  }

protected:
  Point3f m_position; // world space
  Color3f m_power;    // watts
};

NORI_REGISTER_CLASS(PointLight, "point");
NORI_NAMESPACE_END
