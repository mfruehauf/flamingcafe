#include <nori/sampler.h>
NORI_NAMESPACE_BEGIN

inline uint32_t reverse32(uint32_t n) {
  n = (n << 16) | (n >> 16);
  n = ((n & 0x00ff00ff) << 8) | ((n & 0xff00ff00) >> 8);
  n = ((n & 0x0f0f0f0f) << 4) | ((n & 0xf0f0f0f0) >> 4);
  n = ((n & 0x33333333) << 2) | ((n & 0xcccccccc) >> 2);
  n = ((n & 0x55555555) << 1) | ((n & 0xaaaaaaaa) >> 1);
  return n;
}

inline uint64_t reverse64(uint64_t n) {
  uint64_t n0 = reverse32((uint32_t)n);
  uint64_t n1 = reverse32((uint32_t)(n >> 32));
  return (n0 << 32) | n1;
}

template <int base> inline float radicalInverseSpecialized(uint64_t a) {
  const float invBase = 1.f / static_cast<float>(base);
  uint64_t reversedDigits = 0;
  float invBaseN = 1;
  while (a) {
    uint64_t next = a / base;
    uint64_t digit = a - next * base;
    reversedDigits = reversedDigits * base + digit;
    invBaseN *= invBase;
    a = next;
  }
  return std::min(reversedDigits * invBaseN, 1.f - Epsilon);
}

float RadicalInverse::get(int base, uint64_t a) {
  switch (base) {
  case 0:
    return reverse64(a) * 5.4210108624275222e-20; // division by 2^64
  case 1:
    return radicalInverseSpecialized<3>(a);
  case 2:
    return radicalInverseSpecialized<5>(a);
  case 3:
    return radicalInverseSpecialized<7>(a);
  case 4:
    return radicalInverseSpecialized<11>(a);
  default:
    throw NoriException("Radical inverse is not implemented for base %d", base);
  }
}

NORI_NAMESPACE_END