/*
    This file loads a texturemap (an image loaded as a texture)
*/


#include <nori/texture.h>
#include <nori/interpolation.h>
#include <lodepng/lodepng.h>
#include <filesystem/resolver.h>

NORI_NAMESPACE_BEGIN

template <typename T>
class TextureMap : public Texture<T> {
 public:
  typedef std::vector<T> Base;

  TextureMap(const PropertyList &props, std::map<std::string, void *> *cache);

  virtual std::string toString() const override;

  virtual T eval(const Point2f &uv) const override {
    if (!m_valuesArray || m_valuesArray->empty()) {
      return m_baseValue;
    }
    Point2f st = uv.unaryExpr([](const float f) { return f - std::floor(f); });
    // go from image space (0,0) top left to uv space (0,0) bottom right
    st.y() = 1.f - st.y(); 

    return bilinearInterpolation(Point2f(st.x()*(m_height), st.y()*(m_width )), (*m_valuesArray), m_height-1 , m_width-1 );
  }

  void changeBaseValue(const T &newBaseValue) {
    T valueChange = newBaseValue / m_baseValue;
    for (unsigned i = 0; i < m_height; i++)
      for (unsigned j = 0; j < m_width; j++)
        (*m_valuesArray)(m_width * i + j) *= valueChange;
    m_baseValue = newBaseValue;
  }

 private:
  void *LoadTexture();

 protected:
  Base *m_valuesArray = nullptr;
  T m_baseValue;
  filesystem::path m_filename;
  unsigned int m_width, m_height = 0;

 private:
  Base _m_values;  // holds memory of m_valuesArray. Do not use directly
};

template <>
void *TextureMap<Color3f>::LoadTexture() {
  if (m_filename.empty()) {
    return nullptr;
  }
  std::vector<unsigned char> png;  // The image
  std::vector<unsigned char> rawImage;

  // load and decode
  unsigned error =
      lodepng::load_file(png, m_filename.str());  // Load the png file

  lodepng::State pngstate;
  if (!error)
    error = lodepng::decode(rawImage, m_width, m_height, pngstate, png);

  // Check if the file was decoded
  if (error) {
    throw NoriException(
        "Error while loading Texture from file \"%s \"! \n    "
        "decoding error: %s\n",
        m_filename.str(), lodepng_error_text(error));
  }

  unsigned pixelLength;
  switch (pngstate.info_raw.colortype) {
    case LodePNGColorType::LCT_RGB:
      pixelLength = 3;
      break;
    case LodePNGColorType::LCT_RGBA:
      pixelLength = 4;
      break;
    default:
      pixelLength = 0;
      throw NoriException(
          "Error while loading Texture from file \"%s \"! \n    "
          "Color type is not RGB or RGBA but %s\n",
          m_filename.str(), pngstate.info_raw.colortype);
      break;
  }

  unsigned bitDepth = pngstate.info_raw.bitdepth;
  float scaleInv = 1. / (std::pow(2, bitDepth) - 1.);  // ???

  _m_values.clear();
  _m_values.reserve(m_width * m_height);

  for (unsigned i = 0; i < m_width; i++)
    for (unsigned j = 0; j < m_height; j++) {
      float r, g, b;
      r = rawImage[pixelLength * (j * m_width + i)] * scaleInv;
      g = rawImage[pixelLength * (j * m_width + i) + 1] * scaleInv;
      b = rawImage[pixelLength * (j * m_width + i) + 2] * scaleInv;
      _m_values.push_back(Color3f(r, g, b));
    }
  m_valuesArray = &_m_values;
  return this;
}

template <>
void *TextureMap<float>::LoadTexture() {
  if (m_filename.empty()) {
    return nullptr;
  }
  std::vector<unsigned char> png;  // The image
  std::vector<unsigned char> rawImage;

  // load and decode
  unsigned error =
      lodepng::load_file(png, m_filename.str());  // Load the png file

  lodepng::State pngstate;
  if (!error)
    error = lodepng::decode(rawImage, m_width, m_height, pngstate, png);

  // Check if the file was decoded
  if (error) {
    throw NoriException(
        "Error while loading Texture from file \"%s \"! \n    "
        "decoding error: %s\n",
        m_filename, lodepng_error_text(error));
  }

  size_t pixelLength;
  switch (pngstate.info_raw.colortype) {
    case LodePNGColorType::LCT_GREY:
      pixelLength = 1;
      break;
    case LodePNGColorType::LCT_GREY_ALPHA:  // Read only the grey value
      pixelLength = 2;
      break;
    case LodePNGColorType::LCT_RGB:  // Read only the first value
      pixelLength = 3;
      break;
    case LodePNGColorType::LCT_RGBA:  // Read only the first value
      pixelLength = 4;
      break;
    default:
      pixelLength = 0;
      throw NoriException(
          "Error while loading Texture from file \"%s \"! \n    Color type is "
          "not GREY, GREY_ALPHA, RGB or RGBA, but %s\n",
          m_filename, pngstate.info_raw.colortype);
      break;
  }

  unsigned bitDepth = pngstate.info_raw.bitdepth;
  float scaleInv = 1. / (std::pow(2, bitDepth) - 1.);

  _m_values.clear();
  _m_values.reserve(m_width * m_height);

  for (size_t i = 0; i < m_width; i++)
    for (size_t j = 0; j < m_height; j++) {
      float value = rawImage[pixelLength * (j * m_width + i)] * scaleInv;
      _m_values.push_back(value);
    }
  m_valuesArray = &_m_values;
  return this;
}

template <>
TextureMap<float>::TextureMap(const PropertyList &props,
                              std::map<std::string, void *> *cache) {
  m_baseValue = props.getFloat("baseValue", 1.f);
  if (props.has("filename")) {
    m_filename = getFileResolver()->resolve(props.getString("filename"));
    std::string fileStr = m_filename.str();
    if (cache->find(fileStr) == cache->end()) {
      (*cache)[fileStr] = LoadTexture();
    } else {
      auto other = static_cast<TextureMap<float> *>((*cache)[fileStr]);
      if (!other) {
        throw NoriException("valid tex cache lookup");
      }
      m_valuesArray = other->m_valuesArray;
      m_width = other->m_width;
      m_height = other->m_height;
    }
  } else if (!props.has("baseValue")) {
    throw NoriException("No filename found for the texture!");
  }
}

template <>
TextureMap<Color3f>::TextureMap(const PropertyList &props,
                                std::map<std::string, void *> *cache) {
  m_baseValue = props.getColor("baseValue", Color3f(1.f));
  if (props.has("filename")) {
    m_filename = getFileResolver()->resolve(props.getString("filename"));
    std::string fileStr = m_filename.str();
    if (cache->find(fileStr) == cache->end()) {
      (*cache)[fileStr] = LoadTexture();
    } else {
      auto other = static_cast<TextureMap<Color3f> *>((*cache)[fileStr]);
      if (!other) {
        throw NoriException("valid tex cache lookup");
      }
      m_valuesArray = other->m_valuesArray;
      m_width = other->m_width;
      m_height = other->m_height;
    }
  } else if (!props.has("baseValue")) {
    throw NoriException("No filename found for the texture!");
  }
}

template <>
std::string TextureMap<float>::toString() const {
  return tfm::format("TextureMap[ type: float,  path: %s, base value:  %f ]",
                     m_filename.str(), m_baseValue);
}

template <>
std::string TextureMap<Color3f>::toString() const {
  return tfm::format(
      "TextureMap[ type: Color,  path: %s, base Color:  %f, WxH : %i x %i ]",
      m_filename.str(), m_baseValue, m_width, m_height);
}
NORI_REGISTER_TEMPLATED_CLASS_CACHED(TextureMap, float, "texturemap_float")
NORI_REGISTER_TEMPLATED_CLASS_CACHED(TextureMap, Color3f, "texturemap_color")
NORI_NAMESPACE_END