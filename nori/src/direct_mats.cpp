#include <nori/bsdf.h>
#include <nori/integrator.h>
#include <nori/sampler.h>
#include <nori/scene.h>
#include <stdio.h>

NORI_NAMESPACE_BEGIN

class DirectBRDFIntegrator : public Integrator {
public:
  DirectBRDFIntegrator(const PropertyList &props) {}

  Color3f estimateDirect(const Scene *scene, Sampler *sampler,
                         const Vector3f &wi, const Intersection &its,
                         const Point2f &u_scattering) const {
    //
    // Estimate the local illumination integral
    //
    Color3f Le(Color3f::Zero()), Li{Color3f::Zero()};
    EmitterQueryRecord lRec{its.p};

    Color3f f(Color3f::Zero());
    if (its.mesh != nullptr) {
      BSDFQueryRecord bRec{wi};
      bRec.uv = its.uv;
      bRec.p = its.p;
      // why no cos scaling here? --> It's already a part fo the BSDF.sample()
      // function
      f = its.mesh->getBSDF()->sample(bRec, u_scattering);

      // Update the shadow ray
      lRec.shadowRay = Ray3f(its.p, its.shFrame.toWorld(bRec.wo));
    }

    if (!f.isZero()) {
      // TODO: handleMedia
      // Compute effect of visibility of the light source sample
      Intersection its_wo;
      if (scene->rayIntersect(lRec.shadowRay, its_wo) && its.mesh != nullptr &&
          its_wo.mesh->isEmitter()) {
        EmitterQueryRecord lRec_isc{lRec.ref, its_wo.p, its_wo.shFrame.n};
        Li += its_wo.mesh->getEmitter()->eval(lRec_isc);
      }

      // Add light's contribution to reflected radiance
      Le += f * Li;
    }

    return Le;
  };

  virtual Color3f Li(const Scene *scene, Sampler *sampler,
                     const Ray3f &ray) const override {
    Intersection its;
    if (!scene->rayIntersect(ray, its)) {
      return Color3f(0.f);
    }
    Color3f Le(Color3f::Zero());
    // Estimate constant Le(p, wo)
    if (its.mesh != nullptr && its.mesh->isEmitter()) {
      EmitterQueryRecord lRec_mesh{ray.o, its.p, its.shFrame.n};
      Le += its.mesh->getEmitter()->eval(lRec_mesh);
    }

    // by convention wo, wi point away from the intersection
    Vector3f wi = its.shFrame.toLocal(-ray.d);
    return Le + estimateDirect(scene, sampler, wi, its, sampler->next2D());
  }

  std::string toString() const {
    return tfm::format("DirectMatsIntegrator[\n"
                       "lightsampler = %s\n"
                       "]",
                       m_light_sampler->toString());
  }

protected:
};

NORI_REGISTER_CLASS(DirectBRDFIntegrator, "direct_mats");

NORI_NAMESPACE_END