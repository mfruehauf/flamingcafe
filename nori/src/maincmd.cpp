

#include <nori/block.h>
#include <nori/gui.h>
#include <nori/render.h>
#include <filesystem/path.h>

#ifdef _WIN32
#include <Windows.h>
#else
#include <unistd.h>
#endif


int main(int argc, char **argv) {
  using namespace nori;

  try {
    ImageBlock block(Vector2i(720, 720), nullptr);
    if (argc == 2) {
      std::string filename = argv[1];
      filesystem::path path(filename);

      if (path.extension() == "xml") {
        /* Render the XML scene file */
        RenderThread renderThread(block);
        renderThread.renderScene(filename);

        // wait for rending to start
        while (!renderThread.isBusy() || renderThread.getProgress() == 1.f)
          sleep(1);

        constexpr int barWidth = 70;
        while (renderThread.isBusy()) {
          cout << "Rendering: [";
          int pos = static_cast<int>(barWidth * renderThread.getProgress());
          for (int i = 0; i < barWidth; i++) {
            if (i < pos)
              cout << "=";
            else if (i == pos)
              cout << ">";
            else
              cout << " ";
          }
          cout << "] " << int(renderThread.getProgress() * 100.0) << " %\r";
          cout.flush();
          sleep(1);
        }
        cout << std::endl;

      } else {
        cerr << "Error: unknown file \"" << filename
             << "\", expected an extension of type .xml or .exr" << endl;
      }
    }
  } catch (const std::exception &e) {
    cerr << "Fatal error " << e.what() << endl;
    return -1;
  }
  return 0;
}
