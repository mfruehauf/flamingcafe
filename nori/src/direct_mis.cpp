#include <math.h>
#include <nori/bsdf.h>
#include <nori/integrator.h>
#include <nori/sampler.h>
#include <nori/scene.h>
#include <stdio.h>

NORI_NAMESPACE_BEGIN

class DirectMISIntegrator : public Integrator {
public:
  DirectMISIntegrator(const PropertyList &props) {}

  Color3f sampleLight(const Scene *scene, Sampler *sampler, const Vector3f &wo,
                      const Intersection &its) const {
    // Light sampling
    EmitterQueryRecord em_light_rec{its.p};
    Color3f f_light(Color3f::Zero());

    LightSamplingContext light_ctx;
    Emitter *light = m_light_sampler->sample(light_ctx, sampler->next1D());

    const Point2f u_light = sampler->next2D();
    Color3f Le_light = light->sample(em_light_rec, u_light) / light_ctx.pdf;
    float em_light_pdf = light->pdf(em_light_rec);

    const Vector3f wi = its.toLocal(em_light_rec.wi);
    BSDFQueryRecord em_scattering_rec(wo, wi, EMeasure::ESolidAngle);
    em_scattering_rec.uv = its.uv;
    em_scattering_rec.p = its.p;
    float em_scattering_pdf = 0.f;

    if (its.mesh != nullptr) {
      f_light =
          its.mesh->getBSDF()->eval(em_scattering_rec) * Frame::cosTheta(wi);
      em_scattering_pdf = its.mesh->getBSDF()->pdf(em_scattering_rec);
    }
    if (!f_light.isZero()) {
      Intersection its_wi;
      if (scene->rayIntersect(em_light_rec.shadowRay, its_wi) &&
          !its_wi.mesh->isEnvironmentLight()) {
        Le_light = Color3f::Zero();
      }
    }

    const float joint_pdf = em_light_pdf + em_scattering_pdf;
    float w_em = em_light_pdf / joint_pdf;
    w_em = isnan(w_em) ? 0.f : w_em;
    return w_em * f_light * Le_light;
  };

  Color3f sampleBSDF(const Scene *scene, Sampler *sampler, const Vector3f &wo,
                     const Intersection &its) const {
    // BSDF sampling
    const Point2f u_scattering = sampler->next2D();
    BSDFQueryRecord mat_scatter_rec{wo};
    mat_scatter_rec.uv = its.uv;
    mat_scatter_rec.p = its.p;
    float mat_scattering_pdf = 0.f;
    Color3f f_scatter(Color3f::Zero());

    if (its.mesh != nullptr) {
      f_scatter = its.mesh->getBSDF()->sample(mat_scatter_rec, u_scattering);
      mat_scattering_pdf = its.mesh->getBSDF()->pdf(mat_scatter_rec);
    }

    Color3f Le_scatter{Color3f::Zero()};
    float mat_light_pdf = 0.f;
    Ray3f shadow_ray(its.p, its.toWorld(mat_scatter_rec.wo));
    if (!f_scatter.isZero()) {
      // Compute effect of visibility of the light source sample
      Intersection its_wo;
      if (scene->rayIntersect(shadow_ray, its_wo) && its_wo.mesh != nullptr &&
          its_wo.mesh->isEmitter()) {
        EmitterQueryRecord mat_light_rec{its.p, its_wo.p, its_wo.shFrame.n};
        Le_scatter = its_wo.mesh->getEmitter()->eval(mat_light_rec);
        mat_light_pdf = its_wo.mesh->getEmitter()->pdf(mat_light_rec);
      }
    }
    const float joint_pdf = mat_scattering_pdf + mat_light_pdf;
    float w_mat = mat_scattering_pdf / joint_pdf;
    w_mat = isnan(w_mat) ? 0.f : w_mat;

    return w_mat * f_scatter * Le_scatter;
  }

  virtual Color3f Li(const Scene *scene, Sampler *sampler,
                     const Ray3f &ray) const override {
    Intersection its;
    if (!scene->rayIntersect(ray, its)) {
      return Color3f(0.f);
    }
    Color3f Le{Color3f::Zero()};

    if (its.mesh != nullptr && its.mesh->isEmitter()) {
      EmitterQueryRecord lRec_mesh{ray.o, its.p, its.shFrame.n};
      Le = its.mesh->getEmitter()->eval(lRec_mesh);
    }

    // by convention wo, wi point away from the intersection
    Vector3f wo = its.shFrame.toLocal(-ray.d);

    return Le + sampleLight(scene, sampler, wo, its) +
           sampleBSDF(scene, sampler, wo, its);
  }

  std::string toString() const {
    return tfm::format("DirectMISIntegrator[\n"
                       "lightsampler = %s\n"
                       "]",
                       m_light_sampler->toString());
  }

protected:
};

NORI_REGISTER_CLASS(DirectMISIntegrator, "direct_mis");

NORI_NAMESPACE_END