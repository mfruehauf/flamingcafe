#include <algorithm>
#include <nori/dpdf.h>
#include <nori/lightsampling.h>
#include <nori/timer.h>
#include <string>

NORI_NAMESPACE_BEGIN

/*
Uniform light sampler
*/

void UniformLightSampler::preprocess(const Scene *scene) {
  m_lights = scene->getLights(); // copy assignment
}

Emitter *UniformLightSampler::sample(LightSamplingContext &ctx,
                                     const float sample) const {
  const int num_lights = m_lights.size();
  if (num_lights == 0) {
    ctx.pdf = 0.f;
    return nullptr;
  }

  const int light_idx = std::min(
      std::max(0, static_cast<int>(sample * num_lights)), num_lights - 1);

  ctx.pdf = 1.f / num_lights;
  return m_lights[light_idx];
}

float UniformLightSampler::pmf(const LightSamplingContext &ctx,
                               const Emitter *) const {
  return 1.f / m_lights.size();
}
std::string UniformLightSampler::toString() const {
  return "UniformLightSampler";
}

NORI_REGISTER_CLASS(UniformLightSampler, "uniform_light")

/*
Multiple Light Sampler
*/
void MultipleLightSampler::preprocess(const Scene *scene) {

  std::cout << "Building SAOH Light BVH";
  Timer timer;

  const std::vector<Emitter *> bvh_lights = scene->getLights();
  std::vector<std::pair<int, EmitterBounds>> bvh_bounds;
  bvh_bounds.reserve(bvh_lights.size());
  m_lights.reserve(bvh_lights.size());

  // Initialize class level m_lights, m_infinite_lights, m_bounds
  for (size_t i = 0; i < bvh_lights.size(); i++) {
    Emitter *light = bvh_lights[i];
    EmitterBounds emitter_bounds = light->bounds();

    if (!emitter_bounds.isValid()) {
      m_infinite_lights.push_back(light);
    } else if (emitter_bounds.phi > 0) {
      m_lights.push_back(light);
      bvh_bounds.push_back(std::make_pair(i, emitter_bounds));
      m_bounds.expandBy(emitter_bounds.bounds);
    }
  }

  if (bvh_lights.size() > 0) {
    buildBVH(bvh_bounds, 0, bvh_bounds.size(), 0, 0);
  }

  std::cout << "done (took " << timer.elapsedString() << " build "
            << m_nodes.size() << " nodes"
            << " bounds " << m_bounds.toString() << std::endl;
  std::cout << "infinite lights: " << m_infinite_lights.size() << std::endl;
  std::cout << "finite lights: " << m_lights.size() << std::endl;
}

float MultipleLightSampler::evaluateCost(const EmitterBounds &emitterBounds,
                                         const BoundingBox3f &bounds,
                                         int dim) const {
  if (dim < 0 || dim >= 3) {
    throw NoriException("MultipleLightSampler dim %d needs to be in [0, 3]",
                        dim);
  }

  float theta_o = std::acos(emitterBounds.cosTheta_o),
        theta_e = std::acos(emitterBounds.cosTheta_e);

  float cos_theta_o = emitterBounds.cosTheta_o;
  float theta_w = std::min(theta_o + theta_e, M_PI);
  float sin_theta_o = sqrt(1 - cos_theta_o * cos_theta_o);
  float M_omega = 2 * M_PI * (1 - cos_theta_o) +
                  M_PI / 2 *
                      (2 * theta_w * sin_theta_o - cos(theta_o - 2 * theta_w) -
                       2 * theta_o * sin_theta_o + cos_theta_o);
  float M_a = emitterBounds.bounds.getSurfaceArea();

  Vector3f diagonal = emitterBounds.bounds.getDiagonal();
  float Kr = diagonal.maxCoeff() / diagonal[dim];
  return Kr * emitterBounds.phi * M_omega * M_a;
}

std::pair<int, EmitterBounds> MultipleLightSampler::buildBVH(
    std::vector<std::pair<int, EmitterBounds>> &bvhLights, int start, int end,
    uint64_t bitTrail, int depth) {
  if (start >= end) {
    throw NoriException("MultipleLightSampler: %d >= %d", start, end);
  }

  // add a leaf node if there's nothing else to split
  if (end - start == 1) {
    const int node_index = m_nodes.size();
    const int light_index = bvhLights[start].first;
    EmitterBounds bounds = bvhLights[start].second;
    m_nodes.push_back(LightBVHNode::makeLeaf(light_index, bounds));
    m_emitter_to_bit_trail.insert({m_lights[light_index], bitTrail});
    return {node_index, bvhLights[start].second};
  }

  // Choose a offset and dimension from modified SAOH

  // compute the union over the active lights
  BoundingBox3f bounds, centroids;
  float min_cost = INFINITY;
  int min_cost_bucket = -1, min_cost_split_dim = -1;
  for (int i = start; i < end; i++) {
    const EmitterBounds &b = bvhLights[i].second;
    bounds.expandBy(b.bounds);
    centroids.expandBy(b.centroid());
  }
  // choose the optimal split dimension
  constexpr int n_buckets = 12;
  for (int d = 0; d < 3; d++) {
    if (centroids.max[d] == centroids.min[d]) {
      continue;
    }
    EmitterBounds bucket_emitter_bounds[n_buckets];
    for (int i = start; i < end; i++) {
      Point3f p = bvhLights[i].second.centroid();
      int bucket_idx = n_buckets * centroids.getOffset(p)[d];
      bucket_idx = std::min(bucket_idx, n_buckets - 1);

      if (bucket_idx < 0) {
        throw NoriException(
            "MultipleLightSampling: bucket_idx %d not in [%d, %d]", bucket_idx,
            0, n_buckets);
      }
      bucket_emitter_bounds[bucket_idx] = EmitterBounds::merge(
          bucket_emitter_bounds[bucket_idx], bvhLights[i].second);
    }

    // evaluate the current split
    float cost[n_buckets - 1];
    for (int i = 0; i < n_buckets - 1; i++) {
      EmitterBounds b0, b1;
      // merge bounds for each split
      for (int j = 0; j <= i; j++) {
        b0 = EmitterBounds::merge(b0, bucket_emitter_bounds[j]);
      }
      for (int j = i + 1; j < n_buckets; j++) {
        b1 = EmitterBounds::merge(b1, bucket_emitter_bounds[j]);
      }

      cost[i] = evaluateCost(b0, bounds, d) + evaluateCost(b1, bounds, d);
    }

    // find the minumum cost split
    for (int i = 1; i < n_buckets - 1; i++) {
      if (cost[i] > 0 && cost[i] < min_cost) {
        min_cost = cost[i];
        min_cost_bucket = i;
        min_cost_split_dim = d;
      }
    }
  }

  // Partition lights according to the split
  int mid;
  if (min_cost_split_dim == -1) {
    // if we did not find a good split
    mid = (start + end) / 2;
  } else {
    const auto *partition_mid = std::partition(
        &bvhLights[start], &bvhLights[end - 1] + 1,
        [=](const std::pair<int, EmitterBounds> &bounds) {
          int b = n_buckets * centroids.getOffset(
                                  bounds.second.centroid())[min_cost_split_dim];
          if (b == n_buckets) {
            b = n_buckets - 1;
          }
          return b <= min_cost_bucket;
        });
    mid = partition_mid - &bvhLights[0];
    if (mid == start || mid == end) {
      mid = (start + end) / 2;
    }
    if (mid <= start || mid >= end) {
      throw NoriException("Split index is invalid");
    }
  }

  // create an interior node and recurse
  int node_index = m_nodes.size();
  // create a dummy node here to keep the nodes list in order
  m_nodes.push_back(LightBVHNode());
  if (depth >= 64) {
    throw NoriException("Depth %d too large (>= 64)", depth);
  }
  auto child_l = buildBVH(bvhLights, start, mid, bitTrail, depth + 1);
  if (child_l.first != node_index + 1) {
    throw NoriException("internal BVH index invalid");
  }
  auto child_r =
      buildBVH(bvhLights, mid, end, bitTrail | (1u << depth), depth + 1);

  EmitterBounds merged_bounds =
      EmitterBounds::merge(child_l.second, child_r.second);
  m_nodes[node_index] =
      LightBVHNode::makeInternal(child_r.first, merged_bounds);
  return {node_index, merged_bounds};
}

Emitter *MultipleLightSampler::sample(LightSamplingContext &ctx,
                                      const float sample) const {
  float prob_infinite =
      static_cast<float>(m_infinite_lights.size()) /
      static_cast<float>(m_infinite_lights.size() + (m_nodes.empty() ? 0 : 1));
  float u = sample;
  if (u < prob_infinite) { // sample infinite lights uniformly
    // rescale sample
    u /= prob_infinite;
    int index = std::min<int>(u * m_infinite_lights.size(),
                              m_infinite_lights.size() - 1);
    ctx.pdf = prob_infinite / m_infinite_lights.size();
    return m_infinite_lights[index];
  } else { // Traverse the BVH
    if (m_nodes.empty()) {
      return nullptr;
    }
    Point3f p = ctx.p;
    Vector3f n = ctx.n;
    // rescale the samle
    u = std::min((u - prob_infinite) / (1 - prob_infinite), 1.f - Epsilon);
    int index = 0;
    float pdf = 1 - prob_infinite;
    while (true) {
      LightBVHNode node = m_nodes[index];
      if (!node.isLeaf) {
        // recurse
        const LightBVHNode *child_l = &m_nodes[index + 1];
        const LightBVHNode *child_r = &m_nodes[node.index];

        const float importance_l = child_l->bounds.importance(p, n);
        const float importance_r = child_r->bounds.importance(p, n);
        if (importance_l == 0 && importance_r == 0) {
          return nullptr;
        }

        // sample according to the importance computed above
        DiscretePDF importance_pdf(2);
        importance_pdf.append(importance_l);
        importance_pdf.append(importance_r);
        importance_pdf.normalize();
        float node_pdf;
        int child_idx = importance_pdf.sampleReuse(u, node_pdf);
        pdf *= node_pdf;
        index = (child_idx == 0) ? (index + 1) : node.index;
      } else {
        // sample the leaf node
        float importance = node.bounds.importance(p, n);
        if (index > 0 && importance <= 0.f) {
          throw NoriException("sampled light has zero importance");
        }
        if (index > 0 || importance > 0) {
          ctx.pdf = pdf;
          return m_lights[node.index];
        }
        return nullptr;
      }
    }
  }
}

float MultipleLightSampler::pmf(const LightSamplingContext &ctx,
                                const Emitter *emitter) const {
  // if the emitter is unknown, it has to be an infinite light. Since infinite
  // lights are sampled at random, return a uniform probability (give non-zero
  // probability for sampling other lights, if they are nodes in the tree)

  float p_infinite =
      1.f / (m_infinite_lights.size() + (m_nodes.empty() ? 0 : 1));
  if (m_emitter_to_bit_trail.find(emitter) == m_emitter_to_bit_trail.end()) {
    return p_infinite;
  }

  Point3f p = ctx.p;
  Vector3f n = ctx.n;

  // access the value at *emitter in a const fashion, as operator[] is non-const
  auto it = m_emitter_to_bit_trail.find(emitter);
  if (it == m_emitter_to_bit_trail.end()) {
    throw NoriException(
        "MultipleLightSampler: Emitter is unknown to the sampler");
  }
  uint64_t bit_trail = it->second;

  int node_index = 0; // start search from the root
  p_infinite *= m_infinite_lights.size();
  float pdf = 1 - p_infinite;
  while (true) {
    const LightBVHNode *node = &m_nodes[node_index];
    if (node->isLeaf) {
      // if (emitter != m_lights[node->index]) {

      //   throw NoriException(
      //       "MultipleLightSampler hash map is corrupt. The given emitter "
      //       "should be in the map, but cannot be found");
      // }
      return pdf;
    }
    const LightBVHNode *child_l = &m_nodes[node_index + 1];
    const LightBVHNode *child_r = &m_nodes[node->index];
    const float importances[2] = {
        child_l->bounds.importance(p, n),
        child_r->bounds.importance(p, n),
    };

    // decision variable proceed to the left or right subtree
    uint64_t go_lr = bit_trail & 1;
    float importance = importances[go_lr];
    if (importance <= 0.f) {
      throw NoriException(
          "MultipleLightSampler: The sampled light has 0 importance");
    }
    pdf *= importance / (importances[0] + importances[1]);
    node_index = go_lr ? node->index : (node_index + 1);
    // shift the next decision bit to the 0th position
    bit_trail >>= 1;
  }
}

std::string MultipleLightSampler::toString() const {
  return "MultipleLightSampler";
}

NORI_REGISTER_CLASS(MultipleLightSampler, "mls")

NORI_NAMESPACE_END