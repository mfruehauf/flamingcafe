/*
    This file is part of Nori, a simple educational ray tracer

    Copyright (c) 2015 by Romain Prévost

    Nori is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    Nori is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <nori/shape.h>
#include <nori/bsdf.h>
#include <nori/emitter.h>

#include "nori/scene.h"
//#include <nori/warp.h>
//#include <Eigen/Geometry>

NORI_NAMESPACE_BEGIN


Shape::~Shape() {
    delete m_bsdf;
    //delete m_emitter; // scene is responsible for deleting the emitter
}

void Shape::activate() {
    if (!m_bsdf) {
        /* If no material was assigned, instantiate no BSDF : the shape serves only to close a zone (for medium ) */
        m_bsdf = nullptr;
    }
}

void Shape::addChild(NoriObject *obj) {
    switch (obj->getClassType()) {
        case EBSDF:
            if (m_bsdf)
                throw NoriException(
                    "Shape: tried to register multiple BSDF instances!");
            m_bsdf = static_cast<BSDF *>(obj);
            break;

        case EEmitter:
            if (m_emitter)
                throw NoriException(
                    "Shape: tried to register multiple Emitter instances!");
            m_emitter = static_cast<Emitter *>(obj);
            m_emitter->setShape(static_cast<Shape*>(this));
            break;
        case ETexture:
            if(obj->getIdName() == "normals")
            {
                if(m_normals)
                    throw NoriException(
                    "Shape: tried to register multiple normal maps instances!");
                m_normals = dynamic_cast<Texture<Color3f>*>(obj);
                if(!m_normals)
                    throw NoriException(
                    "Shape: tried to register invalid normal map format!");
                break;
            }
            else 
                throw NoriException(
                    "Shape: tried to register an unknown texture!");
        default:
            throw NoriException("Shape::addChild(<%s>) is not supported!",
                                classTypeName(obj->getClassType()));
    }
}

void Shape::setMedia(const Scene* scene)
{
    m_VInt.innerMedium = scene->getMedium(m_InnerMediumName);
    m_VInt.outerMedium = scene->getMedium(m_outerMediumName);
}

void Shape::updateNormals(Intersection& its) const{
    if(!m_normals)
        return;
    Color3f textureVal = m_normals->eval(its.uv);
    textureVal = textureVal*2.f - 1;//transform each component from [0, 1] to [-1, 1] as needed
    its.shFrame.n = its.shFrame.toWorld(Vector3f(textureVal.x(), textureVal.y(), textureVal.z())).normalized();   
}


std::string Intersection::toString() const {
    if (!mesh)
        return "Intersection[invalid]";

    return tfm::format(
        "Intersection[\n"
        "  p = %s,\n"
        "  t = %f,\n"
        "  uv = %s,\n"
        "  shFrame = %s,\n"
        "  geoFrame = %s,\n"
        "  mesh = %s\n"
        "]",
        p.toString(),
        t,
        uv.toString(),
        indent(shFrame.toString()),
        indent(geoFrame.toString()),
        mesh ? mesh->toString() : std::string("null")
    );
}



NORI_NAMESPACE_END
