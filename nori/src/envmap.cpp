/*
    This file is part of Nori, a simple educational ray tracer

    Copyright (c) 2015 by Romain Prévost

    Nori is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    Nori is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/


#include <nori/emitter.h>
#include <nori/scene.h>
#include <nori/sampler.h>
#include <nori/shape.h>
#include <nori/bitmap.h>
#include <filesystem/resolver.h>
#include <nori/distribution.h>
#include <nori/interpolation.h>
#include <nori/warp.h>
#include <memory>
#include <cstdio>

NORI_NAMESPACE_BEGIN

class EnvironmentMap : public Emitter {
public:
  EnvironmentMap(const PropertyList &props) {
    m_path = getFileResolver()->resolve(props.getString("filename")).str();
    m_scale = props.getFloat("scale", 1.f);
    m_map = Bitmap(m_path);

    // compute scalar img from the env map
    m_scalar = ScalarMap::Zero(m_map.rows(), m_map.cols());
    // TODO(rasaford): Add size based filtering
    // const float filter = 1.f / std::max(m_map.rows(), m_map.cols());
    for (int i = 0; i < m_map.rows(); i++) {
      float sin_theta = sin(M_PI * static_cast<float>(i + 0.5f) /
                            static_cast<float>(m_map.rows()));
      for (int j = 0; j < m_map.cols(); j++) {
        m_scalar(i, j) = m_scale * sin_theta * m_map(i, j).getLuminance();
      }
    }
    m_distribution.reset(
        new Distribution2D(m_scalar.data(), m_scalar.rows(), m_scalar.cols()));

    m_validate = props.getBoolean("validate", false);
  }

  /**
   * @brief  Draws a quick "circle" (aka aliased box) at the specified pixel
   * location
   *
   * @tparam Img
   * @param img
   * @param px
   * @param radius
   * @param color
   */
  template <typename Img>
  void drawCircle(Img &img, const Point2i &px, const int &radius,
                  const Color3f &color = Color3f(1.f, 0.f, 0.f)) {
    for (int i = px.y() - radius; i < px.y() + radius; i++) {
      for (int j = px.x() - radius; j < px.x() + radius; j++) {
        if (i < 0 || i >= img.rows() || j < 0 || j >= img.cols()) {
          continue;
        }
        Point2i current(j, i);
        if ((current - px).squaredNorm() < radius * radius) {
          img(i, j) = color;
        }
      }
    }
  }

  virtual void preprocess(Scene *scene) override {
    BoundingBox3f bbox = scene->getBoundingBox();
    m_center = bbox.getCenter();
    m_radius = (m_center - bbox.max).norm();

    // Validation
    // Show the drawn samples in an image
    if (m_validate) {
      const int num_samples = 1000;
      std::cout << "Drawing " << num_samples << " samples from env map"
                << std::endl;

      Bitmap img = m_map;
      Sampler *sampler = scene->getSampler();
      std::vector<Point2f> sample_loc;
      for (int i = 0; i < num_samples; i++) {
        Point2f u = sampler->next2D();
        EmitterQueryRecord lRec(Point3f::Zero());
        Color3f L = this->sample(lRec, u);

        float theta = Frame::sphericalTheta(lRec.wi),
              phi = Frame::sphericalPhi(lRec.wi);
        Point2f st{phi * INV_TWOPI, theta * INV_PI}; // in [0, 1]^2
        Point2i px =
            (st.cwiseProduct(Vector2f(m_map.cols() - 1, m_map.rows() - 1)))
                .cast<int>();
        px = px.cwiseMax(0).cwiseMin(
            Point2i(m_map.cols() - 1, m_map.rows() - 1));

        drawCircle(img, px, 2);
      }

      img.saveToLDR("env_validate.png");
    }
  }

  virtual std::string toString() const override {
    return tfm::format("EnvironmentMap[\n"
                       "  path = %s,\n"
                       "  scale= %f,\n"
                       "]",
                       m_path, m_scale);
  }

  Color3f colorLookup(const Point2f &uv) const {
    Point2f st = uv.cwiseMax(0.f).cwiseMin(1.f); // clamp in [0, 1]
    st = uv.cwiseProduct(Point2f(static_cast<float>(m_map.cols() - 1),
                                 static_cast<float>(m_map.rows() - 1)));
    if (isnan(uv.x()) || isnan(uv.y())) {
      return Color3f::Zero();
    }
    return m_scale * bilinearInterpolation<Color3f>(st, m_map);
  }

  virtual Color3f eval(const EmitterQueryRecord &lRec) const override {
    // convert wi to spherical coordinates and sample the texture there
    Vector3f dir = lRec.wi;
    float theta = Frame::sphericalTheta(dir), phi = Frame::sphericalPhi(dir);
    Point2f uv{phi * INV_TWOPI, theta * INV_PI}; // in [0, 1]^2
    return colorLookup(uv);
  }

  virtual Color3f sample(EmitterQueryRecord &lRec,
                         const Point2f &sample) const override {

    float map_pdf;
    const Point2f uv = m_distribution->sampleContinuous(sample, &map_pdf);

    if (map_pdf == 0.f) {
      return Color3f::Zero();
    }

    // convert uv coordinates on the sphere to a direction
    const float theta = uv.y() * M_PI, phi = uv.x() * 2.f * M_PI;
    const float cos_theta = cos(theta), sin_theta = sin(theta);
    const float sin_phi = sin(phi), cos_phi = cos(phi);
    // wi points towards the sample
    lRec.wi = Vector3f(sin_theta * cos_phi, sin_theta * sin_phi, cos_theta)
                  .normalized();

    // the shading normal is the opposite direction
    lRec.n = -lRec.wi;
    lRec.p = m_center + m_radius * lRec.wi;
    lRec.shadowRay = Ray3f(lRec.ref, lRec.wi, Epsilon, m_radius);

    // convert from map density to solid angles
    lRec.pdf = map_pdf / (2.f * M_PI * M_PI * sin_theta);
    if (sin_theta == 0.f || lRec.pdf == 0.f) {
      lRec.pdf = 0.f;
      return Color3f::Zero();
    }

    return eval(lRec) / lRec.pdf;
  }

  virtual float pdf(const EmitterQueryRecord &lRec) const override {

    const float theta = Frame::sphericalTheta(lRec.wi),
                phi = Frame::sphericalPhi(lRec.wi);
    const float sin_theta = sin(theta);
    // prevent division by 0 at the poles
    if (sin_theta == 0.f) {
      return 0.f;
    }

    return m_distribution->pdf(Point2f(phi * INV_TWOPI, theta * INV_PI)) /
           (2.f * M_PI * M_PI * sin_theta);
  }

  virtual EmitterBounds bounds() const override {
    BoundingBox3f sphere_bounds;
    // intentionally create an invalid bounding box for the EnvMap bounds, to
    // make this light infinite
    sphere_bounds.min = m_center + Vector3f(m_radius);
    sphere_bounds.max = m_center - Vector3f(m_radius);
    const float phi =
        (M_PI * m_radius * m_radius * colorLookup(Point2f(.5f, .5f)))
            .maxCoeff();
    return EmitterBounds(sphere_bounds, Vector3f::UnitZ(), phi, -1, 0, false);
  }

  virtual Color3f samplePhoton(Ray3f &ray, const Point2f &sample1,
                               const Point2f &sample2) const override {
    return Color3f::Zero();
  }

  bool isEnvironmentLight() const override { return true; }

protected:
  std::string m_path;
  float m_scale;
  Bitmap m_map;
  ScalarMap m_scalar;

  // infinite sphere params
  Point3f m_center;
  float m_radius;
  std::unique_ptr<Distribution2D> m_distribution;

  // Validation
  bool m_validate;
};

NORI_REGISTER_CLASS(EnvironmentMap, "envmap")
NORI_NAMESPACE_END