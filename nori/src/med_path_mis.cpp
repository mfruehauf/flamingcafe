#include <nori/bsdf.h>
#include <nori/media.h>
#include <nori/integrator.h>
#include <nori/sampler.h>
#include <nori/scene.h>
#include <stdio.h>

NORI_NAMESPACE_BEGIN

class MediumPathMISIntegrator : public Integrator {
public:
    MediumPathMISIntegrator(const PropertyList& props)
    {
        m_baseMediumName = props.getString("base_medium", "");
        m_maxDistance = props.getFloat("max_distance", 10000.f);
        m_maxDepth = props.getInteger("max_depth", 64);

    }

    bool inline russianRoulette(const float sample,
        const float success_prob) const {
        return sample > success_prob;
    }

    void updateMedium(const Medium*& currentMedium, Intersection const& mediumInterface, Vector3f const& wi) const
    {
        if (mediumInterface.mesh && mediumInterface.mesh->isMediumTransition())
        {
            currentMedium = (mediumInterface.geoFrame.n.dot(wi) > 0) ? mediumInterface.mesh->getVolumeInterface().outerMedium : mediumInterface.mesh->getVolumeInterface().innerMedium;
        }
    }



    Color3f sampleOneLight(Point3f p, Vector3f wo, const Medium* pMedium, bool sampledMedium, float& light_pdf, float& scatter_pdf,EmitterQueryRecord& lRec, MediumQueryRecord& mRec, BSDFQueryRecord& bRec, Intersection const& its,Intersection& its_nee, LightSamplingContext& ctx, const Scene* scene, Sampler* sampler) const
    {

        //sample the light

        ctx = LightSamplingContext(its.p, its.shFrame.n);

        Emitter* light = m_light_sampler->sample(ctx, sampler->next1D());

        if (light == nullptr)
            return Color3f::Zero();
        lRec.ref = p;
        Color3f Le = light->sample(lRec, sampler->next2D()) / ctx.pdf;
        light_pdf = lRec.pdf*ctx.pdf;
        Color3f t = Color3f::Ones();
        Color3f Lout = Color3f::Zero();
        bool foundMediumInterface = false;

        if(!sampledMedium)
            updateMedium(pMedium, its, lRec.shadowRay.d);
        
        do
        {
            foundMediumInterface = scene->rayIntersect(lRec.shadowRay, its_nee);
            if (foundMediumInterface && !its_nee.mesh->isEnvironmentLight() && its_nee.mesh->getBSDF() )
            {
                lRec.pdf = 0.0f;
                return Color3f::Zero();
            }

            if (pMedium) {
                mRec.passingRay = lRec.shadowRay;
                if(foundMediumInterface){
                    mRec.passingRay.maxt = its_nee.t;
                    mRec.passingRay.update();
                }
                if(pMedium->isEmissive())
                    Lout+=t*pMedium->evalEmission(mRec, sampler);
                t *= pMedium->eval(mRec, sampler);
                
            }
            if(t.isZero(Epsilon))
                    break;
            if (foundMediumInterface && !its_nee.mesh->isEnvironmentLight() && !its_nee.mesh->getBSDF())
            {

                lRec.shadowRay.o = lRec.shadowRay(its_nee.t);
                lRec.shadowRay.maxt = (lRec.p - lRec.shadowRay.o).norm();
                lRec.shadowRay.update();
                

                updateMedium(pMedium, its_nee, lRec.shadowRay.d);
            }
        } while (foundMediumInterface && !its_nee.mesh->isEnvironmentLight());
        scatter_pdf = t.mean();
        return t * Le;

    }

    void preprocess(const Scene* scene) override
    {
        Integrator::preprocess(scene);
        m_baseMedium = scene->getMedium(m_baseMediumName);
        for (auto shape : scene->getShapes())
            shape->setMedia(scene);
    }

    virtual Color3f Li(const Scene* scene, Sampler* sampler,
        const Ray3f& ray_) const override {
        //
        // Estimate the local illumination integral
        //
        Ray3f ray = ray_; // copy the initial ray
        ray.maxt = m_maxDistance;
        ray.update();
        const Medium* currentMedium = m_baseMedium;

        Color3f Li{ Color3f::Zero() }, f{ Color3f::Zero() }, t{ Color3f::Ones() };
        Color3f lRad;
        BSDFQueryRecord bRec{ Point3f::Zero() }, bRec_light{ Point3f::Zero() };
        EmitterQueryRecord lRec, lRec_light;
        MediumQueryRecord mRec, mRec_light;
        PhaseFunctionQueryRecord pRec;
        float success_prob = 1.f, w_mat = 1.f, w_em = 1.f, mat_scattering_pdf = 1.f,
            mat_light_pdf = 1.f, em_scattering_pdf = 1.f, em_light_pdf = 1.f;
        // ray intersection, next event estimation intersection
        Intersection its, its_nee;
        LightSamplingContext ctx;
        bool LastWasDiscrete = false;

        int bounces = 0;
        bool sceneIntersect;

        while(bounces < m_maxDepth) {
            sceneIntersect = scene->rayIntersect(ray, its);
            if (!sceneIntersect && !currentMedium) {
                break;
            }

            //Update transmittance through medium
            if (currentMedium)
            {
                mRec.passingRay = ray;
                if(sceneIntersect)
                {
                    mRec.passingRay.maxt =  its.t;
                    mRec.passingRay.update();
                }
                Color3f LocT = currentMedium->sample(mRec, sampler);
                mat_scattering_pdf*=LocT.mean();
                if(currentMedium->isEmissive())
                    Li+=t*currentMedium->evalEmission(mRec, sampler);
                t *= LocT;
                if (t.isZero(Epsilon))
                    break;
                
            }
            else
            {
                mRec.sampledMedium = false;
                mRec.t = its.t;
            }
            Point3f p = ray(mRec.t);

            if(p.norm() > m_maxDistance  || p.hasNaN() || (!mRec.sampledMedium && !its.mesh))
                break;



            if (!mRec.sampledMedium && its.mesh->isEmitter())
            {      
                lRec = EmitterQueryRecord(ray.o, its.p, its.shFrame.n);
                mat_light_pdf = its.mesh->getEmitter()->pdf(lRec)*m_light_sampler->pmf(ctx, its.mesh->getEmitter());
                w_mat = LastWasDiscrete
                    ? 1.f // EDiscrete and EUnknown
                    : mat_scattering_pdf / (mat_scattering_pdf + mat_light_pdf);
                w_mat = isnan(w_mat) ? 0.f : w_mat;
                Li += w_mat * t * its.mesh->getEmitter()->eval(lRec);
            }
            if (!mRec.sampledMedium && !its.mesh->getBSDF())// A shape with no bsdf is intersected : no interaction
            {

                ray = Ray3f(p, ray.d);
                updateMedium(currentMedium, its, ray.d);
                
            }
            else
            {
                //Sample a light
                lRec_light.ref = p;
                lRad = sampleOneLight(p, -ray.d, currentMedium,mRec.sampledMedium,em_light_pdf, em_scattering_pdf, lRec_light, mRec_light, bRec_light, its,its_nee,ctx, scene, sampler);

                if (!lRad.isZero())
                {
                    if (mRec.sampledMedium)
                    {

                        pRec = PhaseFunctionQueryRecord(-ray.d, lRec_light.wi, its.p);
                        em_scattering_pdf *= currentMedium->getPhaseFunction()->eval(pRec);
                        w_em = lRad.isZero() ? 0.f : em_light_pdf / (em_light_pdf + em_scattering_pdf);
                        lRad *= currentMedium->getPhaseFunction()->eval(pRec);

                    }
                    else
                    {
                        bRec_light = BSDFQueryRecord{ its.toLocal(-ray.d), its.toLocal(lRec_light.wi),
                                                     EMeasure::ESolidAngle };
                        bRec_light.uv = its.uv;
                        em_scattering_pdf *= its.mesh->getBSDF()->pdf(bRec_light);
                        w_em = bRec_light.measure == EMeasure::EDiscrete || lRad.isZero() 
                            ? 0.f 
                            : em_light_pdf / (em_light_pdf + em_scattering_pdf);
                        float cosTheta = std::max(0.f, (Frame::cosTheta(bRec_light.wo)));
                        lRad *= its.mesh->getBSDF()->eval(bRec_light) * cosTheta;
                    }
                    
                    w_em = isnan(w_em) ? 0.f : w_em;

                    Li +=
                        w_em * t * lRad;
                }


                //The end of the ray in the medium is a surface
                if(!mRec.sampledMedium)
                {
                    bRec.wi = its.toLocal(-ray.d);
                    bRec.uv = its.uv;
                    bRec.p = its.p;

                    // the surface BSDF is already importance sampled
                    // f = BSDF * cos(theta) / pdf
                    f = its.mesh->getBSDF()->sample(bRec, sampler->next2D());
                    mat_scattering_pdf = its.mesh->getBSDF()->pdf(bRec);
                    LastWasDiscrete = bRec.measure==EDiscrete;
                    t *= f;

                    // update the ray for next termination
                    ray = Ray3f(its.p, its.toWorld(bRec.wo));
                    

                    updateMedium(currentMedium, its, ray.d);
                }
                else
                {
                    pRec = PhaseFunctionQueryRecord(-ray.d);
                    pRec.p = p;
                    mat_scattering_pdf = currentMedium->getPhaseFunction()->sample(pRec, sampler->next2D());
                    //f = Color3f::Ones();//monochromatic scaterring only scatering, no absorbtion
                    LastWasDiscrete = false;
                    ray = Ray3f(p, pRec.wo);

                }

                if (t.isZero(Epsilon))
                    break;

                if (bounces > 2) {
                    success_prob = std::min(t.maxCoeff(), 1.f - Epsilon);
                    // std::cout << success_prob << std::endl;
                    if (russianRoulette(sampler->next1D(), success_prob)) {
                        break;
                    }
                    else {
                        t /= success_prob;
                    }
                }
                bounces++;
            }

            //Makes sure the ray cannot travel up to infinity (needed to avoid the heterogeneous media to be stucked in a loop)
            ray.maxt = m_maxDistance;
            ray.update();


        }
        return Li;
    }

    std::string toString() const override { return "PathMISIntegrator for Media[]"; }

protected:
    const Medium* m_baseMedium = nullptr;
    std::string m_baseMediumName;
    float m_maxDistance;
    int m_maxDepth;
};

NORI_REGISTER_CLASS(MediumPathMISIntegrator, "med_path_mis");

NORI_NAMESPACE_END