#include <nori/integrator.h>
#include <nori/scene.h>
#include <nori/warp.h>

NORI_NAMESPACE_BEGIN

class AverageVisibilityIntegrator : public Integrator {
public:
  AverageVisibilityIntegrator(const PropertyList &props)
      : m_length(props.getFloat("length", 1.f)){};

  /// Compute the radiance value for a given ray. Just return green here
  Color3f Li(const Scene *scene, Sampler *sampler, const Ray3f &ray) const {
    /* Find the surface that is visible in the requested direction */
    Intersection its;
    if (!scene->rayIntersect(ray, its)) {
      return Color3f(1.0f);
    }

    Vector3f ray_dir = Warp::sampleUniformHemisphere(sampler, its.shFrame.n);
    Ray3f ray_out{its.p, ray_dir};
    // since ray_dir is normalized, we can directly use m_length as the maximal
    // ray length
    ray_out.maxt = m_length;
    return scene->rayIntersect(ray_out, its) ? Color3f(0.f) : Color3f(1.f);
  }

  /// Return a human-readable description for debugging purposes
  std::string toString() const { return "NormalIntegrator[]"; }

protected:
  float m_length;
};

NORI_REGISTER_CLASS(AverageVisibilityIntegrator, "av");
NORI_NAMESPACE_END