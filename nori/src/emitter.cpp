#include <nori/emitter.h>

NORI_NAMESPACE_BEGIN


EmitterBounds::EmitterBounds()
    : bounds(BoundingBox3f(Point3f::Zero())), w(Vector3f::Zero()), phi(0),
      cosTheta_o(1), cosTheta_e(0), twoSided(false){};
EmitterBounds::EmitterBounds(const BoundingBox3f &bounds, const Vector3f &w,
                             float phi, float cosTheta_o, float cosTheta_e,
                             bool twoSided)
    : bounds(bounds), w(w), phi(phi), cosTheta_o(cosTheta_o),
      cosTheta_e(cosTheta_e), twoSided(twoSided){};

float EmitterBounds::importance(const Point3f &p, const Vector3f &n) const {
  const Point3f bound_center = bounds.getCenter();
  float d2 = (p - bound_center).squaredNorm();
  // distance to the shading point has to be larger than the box size
  d2 = std::max(d2, 0.5f * bounds.getDiagonal().norm());

  Vector3f wi = (p - bound_center).normalized();
  float cos_theta = wi.dot(w);
  if (twoSided) {
    cos_theta = abs(cos_theta);
  }
  float sin_theta = sqrt(1.f - cos_theta * cos_theta);

  // this is a valid formulation for cos(max(a - b, 0)) for a, b in [0, pi]
  auto cos_a_sub_b_clamp = [](float sin_a, float cos_a, float sin_b,
                              float cos_b) -> float {
    if (cos_a > cos_b)
      return 1.f;
    return cos_a * cos_b + sin_a * sin_b;
  };
  // this is a valid formulation for sin(max(a - b, 0)) for a, b in [0, pi]
  auto sin_a_sub_b_clamp = [](float sin_a, float cos_a, float sin_b,
                              float cos_b) -> float {
    if (cos_a > cos_b)
      return 0.f;
    return sin_a * cos_b - cos_a * sin_b;
  };

  float cos_theta_u = boundingCone(bounds, p).cosTheta;
  float sin_theta_u = sqrt(1.f - cos_theta_u * cos_theta_u);

  float cos_theta_o = cosTheta_o;
  float sin_theta_o = sqrt(1.f - cos_theta_o * cos_theta_o);

  // computing cos(theta')
  float cos_theta_x =
      cos_a_sub_b_clamp(sin_theta, cos_theta, sin_theta_o, cos_theta_o);
  float sin_theta_x =
      sin_a_sub_b_clamp(sin_theta, cos_theta, sin_theta_o, cos_theta_o);
  float cos_theta_p =
      cos_a_sub_b_clamp(sin_theta_x, cos_theta_x, sin_theta_u, cos_theta_u);

  if (cos_theta_p <= cosTheta_e) {
    return 0.f;
  }

  float importance = phi * cos_theta_p / d2;
  // if (importance <= Epsilon) {
  //   throw NoriException("importance too low: %s", importance);
  // }

  // if the normal is valid compute |cos theta'_i|
  if (!n.isZero()) {
    float cos_theta_i = abs(wi.dot(n));
    float sin_theta_i = sqrt(1.f - cos_theta_i * cos_theta_i);
    float cos_thetap_i =
        cos_a_sub_b_clamp(sin_theta_i, cos_theta_i, sin_theta_u, cos_theta_u);
    importance *= cos_thetap_i;
  }
  // clamp the importance
  return std::max(importance, 0.f);
}

std::string EmitterBounds::toString() const {
  return tfm::format("EmitterBounds[\n"
                     "bounds=%s,\n"
                     "w=%s,\n"
                     "cosTheta_e=%f,\n"
                     "cosTheta_o=%f,\n"
                     "two_sided=%d ]",
                     bounds.toString(), w.toString(), phi, cosTheta_o,
                     cosTheta_e, twoSided);
}

EmitterBounds EmitterBounds::merge(const EmitterBounds &a,
                                   const EmitterBounds &b) {
  if (a.phi == 0) {
    return b;
  }
  if (b.phi == 0) {
    return a;
  }

  const Cone cone =
      Cone::merge(Cone(a.w, a.cosTheta_o), Cone(b.w, b.cosTheta_o));
  const float cos_theta_o = cone.cosTheta;
  const float cos_theta_e = std::max(a.cosTheta_e, b.cosTheta_e);
  BoundingBox3f ext_bounds = a.bounds;
  ext_bounds.expandBy(b.bounds);

  return EmitterBounds(ext_bounds, cone.w, a.phi + b.phi, cos_theta_o,
                       cos_theta_e, a.twoSided | b.twoSided);
}

NORI_NAMESPACE_END