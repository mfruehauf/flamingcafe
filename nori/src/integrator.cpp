#include <nori/common.h>
#include <nori/integrator.h>
#include <nori/object.h>

NORI_NAMESPACE_BEGIN

// add the light sampler to the object
void Integrator::addChild(NoriObject *obj) {
  switch (obj->getClassType()) {
  case ELightSampler:
    if (m_light_sampler) {
      throw NoriException(
          "Integrator tried to register multiple Light Samplers!");
    }
    m_light_sampler = static_cast<LightSampler *>(obj);
    break;
  default:
    throw NoriException("Integrator::addChild(<%s>) is not supported",
                        classTypeName(obj->getClassType()));
  }
}

void Integrator::activate(){
  if(!m_light_sampler)
  {
    m_light_sampler = static_cast<LightSampler *>(NoriObjectFactory::createInstance("uniform_light", PropertyList()));
  }
}

NORI_NAMESPACE_END