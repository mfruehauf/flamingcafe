#include <nori/bsdf.h>
#include <nori/integrator.h>
#include <nori/sampler.h>
#include <nori/scene.h>
#include <stdio.h>

NORI_NAMESPACE_BEGIN

class DirectLightIntegrator : public Integrator {
public:
  DirectLightIntegrator(const PropertyList &props) {}


  virtual Color3f Li(const Scene *scene, Sampler *sampler,
                     const Ray3f &ray) const override {
    Intersection its;
    if (!scene->rayIntersect(ray, its)) {
      return Color3f(0.f);
    }
    // by convention wo, wi point away from the intersection
    Vector3f wo = its.shFrame.toLocal(-ray.d);
    const Point2f u_scattering = sampler->next2D();

    //
    // Estimate the direct lighting integral
    //
    Color3f Ld(Color3f::Zero());
    EmitterQueryRecord lRec(its.p);
    LightSamplingContext light_ctx;
    float scattering_pdf = 0.f;

    // sample light source with multiple importance sampling
    Emitter *light = m_light_sampler->sample(light_ctx, sampler->next1D());
    Color3f Li = light->sample(lRec, u_scattering) / light_ctx.pdf;
    Vector3f wi_local = its.shFrame.toLocal(lRec.wi);
    Intersection its_wi;

    if (lRec.pdf > 0.f && !Li.isZero()) {
      // compute BSDF value or phase function;s value for the light sample
      Color3f f(Color3f::Zero());
      if (its.mesh != nullptr) {
        //  Evaluate BSDF for light sampling strategy
        BSDFQueryRecord bRec(wi_local, wo, EMeasure::ESolidAngle);
        bRec.uv = its.uv;
        bRec.p = its.p;

        f = its.mesh->getBSDF()->eval(bRec) * abs(Frame::cosTheta(wi_local));
        scattering_pdf = its.mesh->getBSDF()->pdf(bRec);
      } else {
        // TODO: Evaluate phase function for light sampling strategy
      }

      if (!f.isZero()) {
        // TODO: handleMedia
        // Compute effect of visibility of the light source sample
        if (scene->rayIntersect(lRec.shadowRay, its_wi)) {
          Li = Color3f::Zero();
        }

        // Add light's contribution to reflected radiance
        // TODO: Only handling delta lights for now
        if (!Li.isZero()) {
          // if (isDeltaLight()) {
          Ld += f * Li; // Li is already divided by the pdf
          // } else { }
        }
      }
    }

    // TODO: Sample BSDF with multiple importance sampling
    // Ignored since we only handle delta lights

    return Ld;
  }

  std::string toString() const {
    return tfm::format("DirectLightIntegrator[\n"
                       "lightsampler = %s\n"
                       "]",
                       m_light_sampler->toString());
  }

protected:
};

NORI_REGISTER_CLASS(DirectLightIntegrator, "direct");

NORI_NAMESPACE_END