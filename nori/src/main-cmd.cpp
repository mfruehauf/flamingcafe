

#include <nori/render.h>
#include <nori/block.h>
#include <nori/gui.h>
#include <filesystem/path.h>

int main(int argc, char **argv) {
  using namespace nori;
  try {
    ImageBlock block(Vector2i(720, 720));
    if (argc == 2) {
      std::string filename = argv[1];
      filesystem::path path(filename);

      if (path.extension() == "xml") {
        /* Render the XML scene file */
        RenderThread renderThread(block);
        m_renderThread.renderScene(filename);
      }
      cerr << "Error: unknown file \"" << filename
           << "\", expected an extension of type .xml or .exr" << endl;
    }
  }
}
catch (const std::exception &e) {
  cerr << "Fatal error " << e.what() << endl;
  return -1;
}
return 0;
}
