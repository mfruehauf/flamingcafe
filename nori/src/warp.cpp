/*
    This file is part of Nori, a simple educational ray tracer

    Copyright (c) 2015 by Wenzel Jakob

    Nori is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    Nori is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <array>
#include <nori/frame.h>
#include <nori/vector.h>
#include <nori/warp.h>

NORI_NAMESPACE_BEGIN

Vector3f Warp::squareToUniformCylinder(const Point2f &sample) {
  const float theta = 2.f * M_PI * sample.x();
  const float h = 2.f * sample.y() - 1.f;
  return {cos(theta), sin(theta), h};
}

float Warp::squareToUniformCylinderPdf(const Point2f &sample) {
  return 0.f; // TODO: not yet implemented
}

Vector3f Warp::sampleUniformHemisphere(Sampler *sampler, const Normal3f &pole) {
  // Naive implementation using rejection sampling
  Vector3f v;
  do {
    v.x() = 1.f - 2.f * sampler->next1D();
    v.y() = 1.f - 2.f * sampler->next1D();
    v.z() = 1.f - 2.f * sampler->next1D();
  } while (v.squaredNorm() > 1.f);

  if (v.dot(pole) < 0.f)
    v = -v;
  v /= v.norm();

  return v;
}

Point2f Warp::squareToUniformSquare(const Point2f &sample) { return sample; }

float Warp::squareToUniformSquarePdf(const Point2f &sample) {
  return ((sample.array() >= 0).all() && (sample.array() <= 1).all()) ? 1.0f
                                                                      : 0.0f;
}

Point2f Warp::squareToUniformDisk(const Point2f &sample) {
  const float r = sqrt(sample.x());
  const float angle = 2.f * M_PI * sample.y();
  return Point2f(r * cos(angle), r * sin(angle));
}

float Warp::squareToUniformDiskPdf(const Point2f &p) {
  if (p.norm() > 1.f) {
    return 0.f;
  }
  // Normalization such that the pdf integrates to 1
  return INV_PI;
}

Vector3f Warp::squareToUniformSphereCap(const Point2f &sample,
                                        float cosThetaMax) {
  const float z = cosThetaMax;
  const float theta = 2.f * M_PI * sample.x(); // [0. 2 pi]
  const float h = (1.f - z) * sample.y() + z;  // [z, 1]
  const float h_prime = sqrt(std::max(0.f, 1.f - h * h));
  // directly compute the sphere point
  return {cos(theta) * h_prime, sin(theta) * h_prime, h};
}

float Warp::squareToUniformSphereCapPdf(const Vector3f &v, float cosThetaMax) {
  if (v.norm() - 1.f >= Epsilon || v.z() < cosThetaMax) {
    return 0.f;
  }
  // area of the cap is computed by the hat-box theorem
  return INV_TWOPI * 1 / (1 - cosThetaMax);
}

Vector3f Warp::squareToUniformSphere(const Point2f &sample) {
  const Vector3f c = squareToUniformCylinder(sample);
  const float r = sqrt(std::max(0.f, 1.f - c.z() * c.z()));
  return Vector3f(c.x() * r, c.y() * r, c.z());
}

float Warp::squareToUniformSpherePdf(const Vector3f &v) {
  if (v.norm() - 1.f >= Epsilon) {
    return 0.f;
  }
  return INV_FOURPI;
}

Vector3f Warp::squareToUniformHemisphere(const Point2f &sample) {
  const Vector3f c = squareToUniformCylinder(sample);
  const float r = sqrt(std::max(0.f, 1.f - c.z() * c.z()));
  return Vector3f(c.x() * r, c.y() * r, abs(c.z()));
}

float Warp::squareToUniformHemispherePdf(const Vector3f &v) {
  if (v.norm() - 1.f >= Epsilon || v.z() < 0.f) {
    return 0.f;
  }
  return INV_TWOPI;
}

Vector3f Warp::squareToCosineHemisphere(const Point2f &sample) {
  const Point2f disk = squareToUniformDisk(sample);
  float z = sqrt(std::max(0.f, 1.f - disk.squaredNorm()));
  return {disk.x(), disk.y(), z};
}

float Warp::squareToCosineHemispherePdf(const Vector3f &v) {
  if (v.norm() - 1.f >= Epsilon || v.z() < 0.f) {
    return 0.f;
  }
  return v.z() * INV_PI;
}

Vector3f Warp::squareToBeckmann(const Point2f &sample, float alpha) {
  float log_sample = log(1.f - sample.x());
  if (std::isinf(log_sample))
    log_sample = 0.f;
  const float alpha_sq = alpha * alpha;
  const float tan2 = -alpha_sq * log_sample; // >= 0
  const float phi = 2.f * M_PI * sample.y();
  // using sin^2 + cos^2 = 1 and tan^2 = sin^2 / cos^2 we can derive the values
  // for cos and sin
  const float cos_theta = 1.f / sqrt(1.f + tan2);
  const float sin_theta = sqrt(std::max(0.f, 1.f - cos_theta * cos_theta));
  // convert spherical coordinates to cartesian ones
  // Taken form
  // https://en.wikipedia.org/wiki/Spherical_coordinate_system#Cartesian_coordinates
  return {sin_theta * cos(phi), sin_theta * sin(phi), cos_theta};
}


float Warp::squareToBeckmannPdf(const Vector3f &m, float alpha) {
  if (m.norm() - 1.f >= Epsilon || m.z() < 0.f) {
    return 0.f;
  }
  const float cos_theta = m.z();
  // Applying the Pythagorean Identity:
  // https://en.wikipedia.org/wiki/Trigonometric_functions#Pythagorean_identity
  const float tan_sq_theta = 1.f / (cos_theta * cos_theta) - 1.f;
  const float alpha_sq = alpha * alpha;
  const float n = exp(-tan_sq_theta / alpha_sq);
  const float d = M_PI * alpha_sq * cos_theta * cos_theta * cos_theta;
  // direct implementation of the beckmann pdf
  return n / d;
}

//we suppose n = (0,0,1)
Vector3f Warp::squareToGGX(const Point2f& sample, float alphax, float alphay)
{
    float phi = 2.f * M_PI * sample.x();
    float sq = sqrt(sample.y() / (1 - sample.y()));
    Vector3f h = Vector3f(sq * alphax * cos(phi), sq * alphay * sin(phi), 1);
    return h.normalized();
}


float Warp::squareToGGXPdf(const Vector3f& m, float alphax, float alphay)
{
    if (Frame::cosTheta(m) <= 0.f || std::abs(m.norm() - 1) >= Epsilon)
        return 0.f;

    float termInX = m.x() * m.x() / (alphax * alphax);
    float termInY = m.y() * m.y() / (alphay * alphay);
    
    //dont multiply (terminX+terminY) by sinTheta2 (it is already contained in it)
    return INV_PI* Frame::cosTheta(m)/((alphax*alphay)*pow(termInX+termInY + Frame::cosTheta(m)* Frame::cosTheta(m),2.f));
}

Vector3f Warp::squareToGTR1(const Point2f& sample, float alpha)
{
    float a2 = alpha * alpha;
    float termInPower = 1 - pow(a2, 1 - sample.y());
    float cosTheta2 = termInPower / (1 - a2);
    float cosTheta = sqrt(cosTheta2);
    float sinTheta = sqrt(1 - cosTheta2);

    float phi = 2 * M_PI * sample.x();
    return Vector3f(sinTheta*cos(phi), sinTheta*sin(phi), cosTheta);

}

float Warp::SquareToGTR1Pdf(const Vector3f& m, float alpha)
{
    if (Frame::cosTheta(m) < 0.f)
        return 0.f;
    float cosTheta2 = Frame::cosTheta(m) * Frame::cosTheta(m);
    float a2 = alpha * alpha;
    float alphaterm = (a2 - 1) / log(a2);
    return INV_PI* Frame::cosTheta(m) * alphaterm / (1 + (a2 - 1) * cosTheta2);
}

Point2f Warp::squareToOctagon(const Point2f &sample) {
  constexpr int numSides = 8;
  Point2f s = sample; 
  int triIdx = static_cast<int>(s.x() * numSides);    
  triIdx = std::min(triIdx, numSides - 1);
  
  float triF = static_cast<float>(triIdx) / numSides;
  // rescale the sample 
  s.x() = static_cast<float>(numSides) * (s.x() - triF);
 
  Vector3f bc = squareToUniformTriangle(s);

  // static table of octagon corners used as a cache
  static const std::array<Point2f, numSides> corners = {
      Point2f(cos(0.f * M_PI / 4.f - M_PI / 8.f), sin(0.f * M_PI / 4.f - M_PI / 8.f)),
      Point2f(cos(1.f * M_PI / 4.f - M_PI / 8.f), sin(1.f * M_PI / 4.f - M_PI / 8.f)),
      Point2f(cos(2.f * M_PI / 4.f - M_PI / 8.f), sin(2.f * M_PI / 4.f - M_PI / 8.f)),
      Point2f(cos(3.f * M_PI / 4.f - M_PI / 8.f), sin(3.f * M_PI / 4.f - M_PI / 8.f)),
      Point2f(cos(4.f * M_PI / 4.f - M_PI / 8.f), sin(4.f * M_PI / 4.f - M_PI / 8.f)),
      Point2f(cos(5.f * M_PI / 4.f - M_PI / 8.f), sin(5.f * M_PI / 4.f - M_PI / 8.f)),
      Point2f(cos(6.f * M_PI / 4.f - M_PI / 8.f), sin(6.f * M_PI / 4.f - M_PI / 8.f)),
      Point2f(cos(7.f * M_PI / 4.f - M_PI / 8.f), sin(7.f * M_PI / 4.f - M_PI / 8.f)),
  };

  Point2f p0 = Point2f::Zero();
  Point2f p1 = corners[triIdx];
  Point2f p2 = corners[(triIdx + 1) % numSides];

  // interpolate the octagon point from barycentric coordinates
  return bc.x() * p0 + bc.y() * p1 + bc.z() * p2;

}

Vector3f Warp::SquareToHenyeyGreenstein(const Point2f& sample, float g) {
    float  s = 2 * sample.x() - 1;
    float g2 = g * g;
    float t = (1 - g2) / (1 + g * s);
    float cosTheta = -(1 + g2 - t * t) / (2 * g);
    float sinTheta = sqrt(1 - cosTheta * cosTheta);

    float cosPhi = cos(2 * M_PI * sample.y());
    float sinPhi = sin(2 * M_PI * sample.y());
    return  Vector3f(sinTheta * cosPhi, sinTheta * sinPhi, cosTheta);//Henyey-Greenstein distributed vectors with
}

float Warp::SquareToHenyeyGreensteinPdf(const Vector3f& m, float g) {
    const float g2 = g * g;
    const float cosTheta = Frame::cosTheta(m);
    return INV_FOURPI * ((1 - g2) *pow(1 + g2 + 2 * g * cosTheta, -3.f / 2.f));
}

Vector3f Warp::squareToUniformTriangle(const Point2f &sample) {
  float su1 = sqrtf(sample.x());
  float u = 1.f - su1, v = sample.y() * su1;
  return Vector3f(u, v, 1.f - u - v);
}



NORI_NAMESPACE_END
