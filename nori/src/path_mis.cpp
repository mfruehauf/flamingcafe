#include <nori/bsdf.h>
#include <nori/integrator.h>
#include <nori/sampler.h>
#include <nori/scene.h>
#include <stdio.h>

NORI_NAMESPACE_BEGIN

class PathMISIntegrator : public Integrator {
 public:
  PathMISIntegrator(const PropertyList &props) {}

  bool inline russianRoulette(const float sample,
                              const float success_prob) const {
    return sample > success_prob;
  }

  virtual Color3f Li(const Scene *scene, Sampler *sampler,
                     const Ray3f &ray_) const override {
    //
    // Estimate the local illumination integral
    //
    Ray3f ray = ray_;  // copy the initial ray
    Color3f Li{Color3f::Zero()}, f{Color3f::Zero()}, t{Color3f::Ones()};
    BSDFQueryRecord bRec{Point3f::Zero()}, bRec_light{Point3f::Zero()};
    EmitterQueryRecord lRec, lRec_light;
    float success_prob = 1.f, w_mat = 1.f, w_em = 1.f, mat_scattering_pdf = 1.f,
          mat_light_pdf = 1.f, em_scattering_pdf = 1.f, em_light_pdf = 1.f;
    // ray intersection, next event estimation intersection
    Intersection its, its_nee;
    LightSamplingContext ctx;

    int bounces = 0;

    while (true) {
      if (!scene->rayIntersect(ray, its)) {
        break;
      }

      if (its.mesh != nullptr && its.mesh->isEmitter()) {
        lRec = EmitterQueryRecord(ray.o, its.p, its.shFrame.n);
        ctx = LightSamplingContext(its.p, its.shFrame.n);

        // the light pdf is the product of the emitter pdf and the pmf of
        // sampling that light
        mat_light_pdf = its.mesh->getEmitter()->pdf(lRec) *
                        m_light_sampler->pmf(ctx, its.mesh->getEmitter());
        w_mat = bRec.measure != EMeasure::ESolidAngle
                    ? 1.f  // EDiscrete and EUnknown
                    : mat_scattering_pdf / (mat_scattering_pdf + mat_light_pdf);
        w_mat = isnan(w_mat) ? 0.f : w_mat;
        Li += w_mat * t * its.mesh->getEmitter()->eval(lRec);
      }

      if(!its.mesh->getBSDF())
      {
          ray = Ray3f(its.p, ray.d);
      }
      else {
          success_prob = std::min(t.x(), 1.f - Epsilon);
          // std::cout << success_prob << std::endl;
          if (russianRoulette(sampler->next1D(), success_prob)) {
              break;
          }
          else {
              t /= success_prob;
          }

          bRec.wi = its.toLocal(-ray.d);
          bRec.uv = its.uv;
          bRec.p = its.p;

          // the surface BSDF is already importance sampled
          // f = BSDF * cos(theta) / pdf
          f = its.mesh->getBSDF()->sample(bRec, sampler->next2D());
          mat_scattering_pdf = its.mesh->getBSDF()->pdf(bRec);

          // emitter sampling

      ctx = LightSamplingContext(its.p, its.shFrame.n);
      // Emitter *light = nullptr;
      // do {
      //   light = m_light_sampler->sample(ctx, sampler->next1D());
      // } while (light == nullptr);
      Emitter *light = m_light_sampler->sample(ctx, sampler->next1D());
      if (!light) {
        std::cout << ".";
        break;
      }

          lRec_light.ref = its.p;
          Color3f Le_2 = light->sample(lRec_light, sampler->next2D()) / ctx.pdf;
          em_light_pdf = ctx.pdf * light->pdf(lRec_light);

              if (!scene->rayIntersect(lRec_light.shadowRay, its_nee) ||
                  its_nee.mesh->isEnvironmentLight()) {
                  bRec_light = BSDFQueryRecord{ bRec.wi, its.toLocal(lRec_light.wi),
                                               EMeasure::ESolidAngle };
                  bRec_light.uv = its.uv;

                  em_scattering_pdf = its.mesh->getBSDF()->pdf(bRec_light);
                  w_em = bRec_light.measure == EMeasure::EDiscrete || Le_2.isZero()
                      ? 0.f
                      : em_light_pdf / (em_light_pdf + em_scattering_pdf);
                  w_em = isnan(w_em) ? 0.f : w_em;

            float cos_theta = std::max(0.f, Frame::cosTheta(bRec_light.wo));
            Color3f f_a = its.mesh->getBSDF()->eval(bRec_light);
            Li += w_em * t * cos_theta * f_a * Le_2;
          }

          t *= f;

          // update the ray for next termination
          ray = Ray3f(its.p, its.toWorld(bRec.wo));
      }
    }
    return Li;
  }

  std::string toString() const {
    return tfm::format(
        "PathMISIntegrator[\n"
        "lightsampler = %s\n"
        "]",
        m_light_sampler->toString());
  }

 protected:
};

NORI_REGISTER_CLASS(PathMISIntegrator, "path_mis");

NORI_NAMESPACE_END