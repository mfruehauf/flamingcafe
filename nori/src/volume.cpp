#include <nori/volume.h>
#include <filesystem/resolver.h>
#include <nori/bbox.h>
#include <vector>
#include <fstream>

NORI_NAMESPACE_BEGIN

template<typename T>
class ConstVolume : public Volume<T>
{
public:
	ConstVolume(PropertyList const& props);

	T eval(Point3f const& p) const override { return m_value;}
	T maxValue() const override {return m_value;}

	std::string toString() const override;
private:
	T m_value;	
};

template<>
ConstVolume<float>::ConstVolume(PropertyList const& props){
	m_value = props.getFloat("value", 0.0f);
}

template<>
std::string ConstVolume<float>::toString() const {
	return tfm::format("constant volume [\n"
		"value = %f ,\n"
		"]",
		m_value);
}

//Grid Volumes uses the mitsuba format to store volumes values : 
//<https://github.com/mitsuba-renderer/mitsuba3/blob/master/src/volumes/grid.cpp>
template <typename T>
class GridVolume : public Volume<T>
{
public:
	GridVolume(PropertyList const& props);


	T eval(Point3f const& p) const override;

	bool isOutOfBounds(Point3f const& p) const override;

	virtual bool rayIntersect(Ray3f const& ray, float& maxt) const override;

	T maxValue() const override;

	void ChangeScale(T const& newScale) {}

	std::string toString() const override;

protected:

	void LoadVolume();

	T trilinearInterpolation(Point3f const& p) const;

	T evalArray(int const& x, int const& y, int const& z) const ;

	filesystem::path m_filename;
	unsigned m_xRes,  m_yRes, m_zRes,m_size;
	Vector3f m_maxIndexes;

	using Volume<T>::m_bbox;
	using Volume<T>::m_transform;

	std::vector<T> m_ValuesArray;
	T m_maxValue;
};


//Load the Grid values according to the format in  
//<https://github.com/mitsuba-renderer/mitsuba3/blob/master/src/volumes/grid.cpp> : 
/*
	* - Bytes 1-3
     - ASCII Bytes �V�, �O�, and �L�
   * - Byte 4
     - File format version number (currently 3)
   * - Bytes 5-8
     - Encoding identified (32-bit integer). Currently, only a value of 1 is
       supported (float32-based representation)
   * - Bytes 9-12
     - Number of cells along the X axis (32 bit integer)
   * - Bytes 13-16
     - Number of cells along the Y axis (32 bit integer)
   * - Bytes 17-20
     - Number of cells along the Z axis (32 bit integer)
   * - Bytes 21-24
     - Number of channels (32 bit integer, supported values: 1, 3 or 6)
   * - Bytes 25-48
     - Axis-aligned bounding box of the data stored in single precision (order:
       xmin, ymin, zmin, xmax, ymax, zmax)
   * - Bytes 49-*
     - Binary data of the volume stored in the specified encoding. The data
       are ordered so that the following C-style indexing operation makes sense
       after the file has been loaded into memory:
       :code:`data[((zpos*yres + ypos)*xres + xpos)*channels + chan]`
       where (xpos, ypos, zpos, chan) denotes the lookup location.
*/

/* utility to readv the each value from the file (converts from binary to the wanted type)*/
template<typename T> 
bool read(std::istream& stream, T* value, int count = 1)
{
	stream.read((char*)value, sizeof(T) * count);
	if (stream.good())
		return true;
	return false;
}


template<>
void GridVolume<float>::LoadVolume() {
	std::ifstream stream;
	stream.open(m_filename.str(),std::ios::binary);
	if (!stream.is_open())
		throw NoriException("Error! file not found : %s", m_filename);
	
	//First Head must be 'VOL'
	char Header[3];
	read<char>(stream, Header, 3);
	if(stream.eof() || Header[0] != 'V' || Header[1] != 'O' || Header[2] != 'L')
		throw NoriException("Error! Corrupted file : %s", m_filename);

	//Verify that the version is 3 (for compatibility with mitsuba)
	uint8_t version;

	read<uint8_t>(stream, &version);

	if (version != 3)
		throw NoriException("Invalid version, currently only version 3 is supported (found %i)", version);

	//Identify the encoding (must be 1)
	int32_t identifier;
	read<int32_t>(stream, &identifier);
	if(identifier!=1)
		throw NoriException("Invalid identifier, should be 1 and found %i", identifier);
	
	int32_t dims[3];
	read<int32_t>(stream, dims,3);
	m_xRes = dims[0];
	m_yRes = dims[1];
	m_zRes = dims[2];
	m_maxIndexes = Vector3f(m_xRes-1, m_yRes-1, m_zRes-1);
	m_size = m_xRes * m_yRes * m_zRes;
	

	int32_t channel;
	read<int32_t>(stream, &channel);
	if(channel != 1)
		throw NoriException("Invalid number of channels, should be 1 and found %i", channel);

	float pos[6];
	read<float>(stream, pos, 6);
	m_bbox = BoundingBox3f(Point3f(pos[0], pos[1], pos[2]), Point3f(pos[3], pos[4], pos[5]));

	m_ValuesArray.clear();
	m_ValuesArray.resize(m_size);
	bool good;

	for(size_t i = 0; i < m_size; i++)
	{
		good = read<float>(stream, &m_ValuesArray[i], 1);
		if(!good)
			throw NoriException("Error while reading the files: an error occured while reading the volumes values from the file!");
		m_maxValue = std::max(m_maxValue, m_ValuesArray[i]);
	}
	
	stream.close();
}

template<>
GridVolume<float>::GridVolume(PropertyList const& props) {
	if (props.has("filename"))
	{
		m_filename = getFileResolver()->resolve(props.getString("filename"));
		LoadVolume();
	}
	else
	{
		throw NoriException("No filename found for the texture!");
	}

	m_transform = props.getTransform("transform",Transform());

}



template<>
std::string GridVolume<float>::toString() const {
	return tfm::format(
		"GridVolume [ \n"
		" type = float,\n"
		" path = %s,\n"
		" size = %i x %i x %i , \n"
		" bounding box = %s ,\n"
		" transform = %s , \n"
		" max value = %f"
		" ]"
		,
		m_filename.str(),
		 m_xRes, m_zRes, m_yRes,
		 indent(m_bbox.toString()),
		 indent(m_transform.toString()),
		 m_maxValue
		 );
}





template<typename T>
T GridVolume<T>::maxValue() const
{
	return m_maxValue;
}

template<typename T>
bool GridVolume<T>::isOutOfBounds(Point3f const& p) const{
	return !m_bbox.contains(m_transform.inverse()*p);
}

template<typename T>
bool GridVolume<T>::rayIntersect(Ray3f const& ray, float& maxt) const
{
	float mint = 0.f;
	Point3f tO = m_transform.inverse()*ray.o;
	Point3f td = m_transform.inverse()*ray.d;
	float s = td.norm();
	bool result = m_bbox.rayIntersect(Ray3f(tO,td/s), mint, maxt);
	maxt*=s;
	return result;
}

template<typename T>
T GridVolume<T>::evalArray(int const& x, int const& y, int const& z) const
{
	return m_ValuesArray[(z*m_yRes + y)*m_xRes + x];
}



//Trilinear interpolation of a volume 
template<typename T>
T GridVolume<T>::trilinearInterpolation(Point3f const& p) const
{
		// taken from <https://en.wikipedia.org/wiki/Trilinear_interpolation>
  float x = p.x(), y = p.y(), z= p.z();
  int x1 = (int)floor(x), x2 = (int)ceil(x);
  int y1 = (int)floor(y), y2 = (int)ceil(y);
  int z1 = (int)floor(z), z2 = (int)ceil(z);

  // make sure all points are in bounds
  x1 = clamp(x1, 0, m_xRes-1), x2 = clamp(x2, 0, m_xRes-1);
  y1 = clamp(y1, 0, m_yRes-1), y2 = clamp(y2, 0, m_yRes-1);
  z1 = clamp(z1, 0, m_zRes-1), z2 = clamp(z2, 0, m_zRes-1);

	

  T q_111 = evalArray(x1, y1, z1);
  T q_112 = evalArray(x1, y1, z2);
  T q_121 = evalArray(x1, y2, z1);
  T q_122 = evalArray(x1, y2, z2);
  T q_211 = evalArray(x2, y1, z1);
  T q_212 = evalArray(x2, y1, z2);
  T q_221 = evalArray(x2, y2, z1);
  T q_222 = evalArray(x2, y2, z2);

  float x1_f = (float)x1, x2_f = (float)x2;
  float y1_f = (float)y1, y2_f = (float)y2;
  float z1_f = (float)z1, z2_f = (float)z2;

  float wx,wy,wz;
	wx = (x1==x2)?(0.f) : ((x2_f - x) / (x2_f - x1_f));
	wy = (y1==y2)?(0.f) : ((y2_f - y) / (y2_f - y1_f));
	wz = (z1==z2)?(0.f) : ((z2_f - z) / (z2_f - z1_f));

	//Interpolate z
	T r11 = wz*q_111 + (1-wz)*q_112;
	T r12 = wz*q_121 + (1-wz)*q_122;
	T r21 = wz*q_211 + (1-wz)*q_212;
	T r22 = wz*q_221 + (1-wz)*q_222;

	//Interpolate y
	T r1 = wy*r11 + (1-wy)*r12;
	T r2 = wy*r21 + (1-wy)*r22;

  // Interpolate x
  return wx*r1 + (1-wx)*r2;
}

template<typename T>
T GridVolume<T>::eval(Point3f const& p) const {
	Point3f locP = m_transform.inverse()*p;
	if(!m_bbox.contains(locP,false))
		return 0.0f;
	locP -= m_bbox.min;
	locP = locP.cwiseQuotient(m_bbox.max - m_bbox.min);
	locP = locP.cwiseProduct(m_maxIndexes);
	return trilinearInterpolation(locP);
}




NORI_REGISTER_TEMPLATED_CLASS(ConstVolume, float, "constvolume_float")
NORI_REGISTER_TEMPLATED_CLASS(GridVolume, float, "gridvolume_float")
NORI_NAMESPACE_END