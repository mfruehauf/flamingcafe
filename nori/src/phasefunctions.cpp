#include <nori/phasefunctions.h>
#include <nori/warp.h>
#include <nori/frame.h>

NORI_NAMESPACE_BEGIN
class IsotropicPhaseFunction : public PhaseFunction {
public:

	IsotropicPhaseFunction(PropertyList const& propList){}

	virtual float eval(PhaseFunctionQueryRecord const& pRec) const override {
		return INV_FOURPI;
	}

	virtual float sample(PhaseFunctionQueryRecord& pRec, Point2f const& sample) const override {
		pRec.wo = Warp::squareToUniformSphere(sample);
		return eval(pRec);
	}

	virtual std::string toString() const override
	{
		return tfm::format("Isotropic phase function[]");
	}
};

class HenyeyGreensteinPhaseFunction : public PhaseFunction {
public:
	HenyeyGreensteinPhaseFunction(PropertyList const& propList)
	{
		m_g = propList.getFloat("g", 0.0f);
	}

	virtual float eval(PhaseFunctionQueryRecord const& pRec) const override {
		const float g2 = m_g * m_g;
		const float cosTheta = pRec.wi.dot(pRec.wo);
		return INV_FOURPI/ 4.f * ((1 - g2) / pow(1 + g2 + 2 * m_g * cosTheta,3.f/2.f));
	}

	virtual float sample(PhaseFunctionQueryRecord& pRec, Point2f const& sample) const override {
		if( m_g == 0.f)
		{
			pRec.wo = Warp::squareToUniformSphere(sample);
			return INV_FOURPI;
		}

		pRec.wo = Warp::SquareToHenyeyGreenstein(sample, m_g);//Henyey-Greenstein distributed vectors with
			//wi = (0,0,1)
		Frame wiFrame = Frame(pRec.wi);
		pRec.wo = wiFrame.toWorld(pRec.wo);
		return eval(pRec);
	}
	virtual std::string toString() const override
	{
		return tfm::format("Henyey-Greenstein phase function[\n",
				"g = %f\n]", m_g );
	}

private:
	float m_g;
};

NORI_REGISTER_CLASS(IsotropicPhaseFunction, "isotropic");
NORI_REGISTER_CLASS(HenyeyGreensteinPhaseFunction, "henyey-greenstein");
NORI_NAMESPACE_END