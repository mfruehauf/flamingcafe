#include <nori/bsdf.h>
#include <nori/integrator.h>
#include <nori/sampler.h>
#include <nori/scene.h>
#include <stdio.h>

NORI_NAMESPACE_BEGIN

class DirectEMSIntegrator : public Integrator {
public:
  DirectEMSIntegrator(const PropertyList &props) {}

  virtual Color3f Li(const Scene *scene, Sampler *sampler,
                     const Ray3f &ray) const override {
    Intersection its;
    if (!scene->rayIntersect(ray, its)) {
      return Color3f(0.f);
    }
    // by convention wo, wi point away from the intersection
    Vector3f wo = its.toLocal(-ray.d);

    // Estimate constant Le(p, w_o)
    Color3f Le{Color3f::Zero()};
    if (its.mesh != nullptr && its.mesh->isEmitter()) {
      EmitterQueryRecord lRec_mesh{ray.o, its.p, its.shFrame.n};
      Le += its.mesh->getEmitter()->eval(lRec_mesh);
    }

    //
    // Estimate the local illumination integral
    //
    Color3f Lo(Color3f::Zero());
    EmitterQueryRecord lRec{its.p};

    LightSamplingContext light_ctx{its.p, its.shFrame.n};
    Emitter *light = nullptr;
    do {
      light = m_light_sampler->sample(light_ctx, sampler->next1D());
    } while (light == nullptr);

    // sample light source with multiple importance sampling
    const Point2f u_light = sampler->next2D();
    Color3f Li = light->sample(lRec, u_light) / light_ctx.pdf;

    // Emitter sampling
    // compute BSDF value or phase functions value for the light sample
    Color3f f(Color3f::Zero());
    if (its.mesh != nullptr) {
      //  Evaluate BSDF for light sampling strategy
      const Vector3f wi = its.toLocal(lRec.wi);
      BSDFQueryRecord bRec(wi, wo, EMeasure::ESolidAngle);
      bRec.uv = its.uv;
      bRec.p = its.p;
      f = its.mesh->getBSDF()->eval(bRec) * Frame::cosTheta(wi);
    }

    if (!f.isZero()) {
      // Compute effect of visibility of the light source sample
      Intersection its_wi;
      if (scene->rayIntersect(lRec.shadowRay, its_wi) &&
          !its_wi.mesh->isEnvironmentLight()) {
        Li = Color3f::Zero();
      }

      // Add light's contribution to reflected radiance
      Lo += f * Li;
    }
    return Le + Lo;
  }

  std::string toString() const {
    return tfm::format("DirectEMSIntegrator[\n"
                       "lightsampler = %s\n"
                       "]",
                       m_light_sampler->toString());
  }

protected:
};

NORI_REGISTER_CLASS(DirectEMSIntegrator, "direct_ems");

NORI_NAMESPACE_END