import numpy as np
from lark import Transformer, Lark, v_args
import os
from collections import defaultdict
from pprint import pprint
import numpy as np
import argparse
from pathlib import Path
from xml.etree import ElementTree as ET
import meshio
from xml.dom import minidom
from plyfile import PlyData

material_grammar = r"""

    start: (texture | material)*

    texture: "Texture" string  _QUOTE texture_type _QUOTE _QUOTE "imagemap" _QUOTE (_WS* _NEWLINE?)* mat_kv
    texture_type: STRING
    material: "MakeNamedMaterial" string (mat_kv (_WS* _NEWLINE?)*)+
    mat_kv: mat_arg mat_val
    mat_arg: _QUOTE MAT_ARG_TYPE STRING _QUOTE
    mat_val: string | array | NUMBER
    MAT_ARG_TYPE: "float" | "texture" | "string" | "spectrum" | "rgb"


    COMMENT: "#"/[^\n]*/ _NEWLINE
    _QUOTE: /\"|"|'/
    string: ESCAPED_STRING
    CR: /\r/
    LF: /\n/
    _NEWLINE: (CR? LF)
    STRING: /[\w+.,]+/
    array: "[" [ value+ ] "]"
    value: string | NUMBER
    _WS: WS


    %import common.ESCAPED_STRING
    %import common.SIGNED_NUMBER
    %import common.NUMBER
    %import common.WS

    %ignore WS
    %ignore COMMENT
"""


class Printable:

    def __str__(self):
        print(vars(self))
        return f"{self.type} {str(vars(self))}"


class Material(Printable):

    def __init__(self, name: str):
        self.type = "Material"
        self.name = name
        self.roughness = None
        self.reflectance = None
        self.normalmap = None
        self.albedo = None

    def visit(self, root):
        material = ET.SubElement(root, "bsdf")
        # keep all materials as gray diffuse for now
        if self.type == "coateddiffuse":
            material.set("type", "disneybsdf")

            def textured(dtype, name, value, root):
                if isinstance(value, str):
                    tex = ET.SubElement(root, "texture", {
                        "type": f"texturemap_{dtype}",
                        "name": name
                    })
                    tex.append(
                        ET.Element("string", {
                            "name": "filename",
                            "value": value
                        }))
                else:
                    tex = ET.SubElement(root, "texture", {
                        "type": f"constant_{dtype}",
                        "name": name
                    })
                    tex.append(
                        ET.Element(dtype, {
                            "name": "value",
                            "value": str(value)
                        }))

            if self.roughness is not None:
                textured("float", "roughness", self.roughness, material)
            if self.reflectance is not None:
                textured("color", "albedo", self.reflectance.filename,
                         material)

            # add pbrt coateddiffuse defaults
            textured("float", "specular", 1, material)
            textured("float", "metallic", 0, material)
        elif self.type == "dielectric":
            material.set("type", "dielectric")
            ET.SubElement(material, "float", {"name": "extIOR", "value": "1"})
            ET.SubElement(material, "float", {
                "name": "intIOR",
                "value": "1.5046"
            })
        elif self.type == "coatedconductor":
            material.set("type", "disneybsdf")

            def textured(dtype, name, value, root):
                if isinstance(value, str) and "png" in value:
                    tex = ET.SubElement(root, "texture", {
                        "type": f"texturemap_{dtype}",
                        "name": name
                    })
                    tex.append(
                        ET.Element("string", {
                            "name": "filename",
                            "value": str(value)
                        }))
                else:
                    tex = ET.SubElement(root, "texture", {
                        "type": f"constant_{dtype}",
                        "name": name
                    })
                    tex.append(
                        ET.Element(dtype, {
                            "name": "value",
                            "value": str(value)
                        }))

            set_albedo = False
            if self.reflectance is not None:
                textured("color", "albedo", self.reflectance.filename,
                         material)
                set_albedo = True

            if self.albedo is not None and not set_albedo:
                textured("color", "albedo",
                         ", ".join([str(v) for v in self.albedo]), material)

            try:
                roughness = getattr(self, "interface.roughness")
                textured("float", "clearcoat", 1., material)
                textured("float", "clearcoatGloss", 1. - roughness, material)
            except Exception:
                textured("float", "clearcoat", 0.,  material)
                textured("float", "clearcoatGloss", 0., material)

            try:
                roughness = getattr(self, "conductor.roughness")
                textured("float", "roughness", roughness, material)
            except Exception:
                textured("float", "roughness", 0, material)

            if getattr(self, "conductor.eta", "") or getattr(self,"conductor.k", ""):
                textured("float", "metallic", 1, material)
            else:
                textured("float", "metallic", 0, material)

            textured("float", "specular", .5, material) # glass IoR


class Texture(Printable):

    def __init__(self, name: str, data_type):
        self.type = "Texture"
        self.data_type = data_type
        self.name = name


class MatToTree(Transformer):

    def __init__(self):
        self.textures = dict()

    string = lambda _, x: x
    array = list

    @v_args(inline=True)
    def start(self, *args):
        return {a.name: a for a in args if isinstance(a, Material)}

    @v_args(inline=True)
    def texture(self, name, tex_type, *args):
        tex = Texture(name, tex_type)
        for arg in args:
            for k, v in arg.items():
                setattr(tex, k, v)
        self.textures[name] = tex
        return None

    texture_type = lambda _, s: str(s[0])

    @v_args(inline=True)
    def material(self, name: str, *args):
        mat = Material(name)
        for arg in args:
            for k, v in arg.items():
                setattr(mat, k, v)
        return mat

    @v_args(inline=True)
    def mat_kv(self, mat_arg, mat_val):
        apply_fn = {
            "string":
            lambda strs: str(strs[0]) if isinstance(strs, list) else str(strs),
            "float":
            lambda fs: float(fs) if isinstance(fs, str) else float(fs[0]),
            "texture":
            lambda t: self.textures[t] if t in self.textures else None,
            "spectrum":
            str,
            "rgb":
            lambda rgb: [float(c) for c in rgb]
        }
        mat_val = mat_val[0]
        return {mat_arg[1]: apply_fn[mat_arg[0]](mat_val)}

    @v_args(inline=True)
    def mat_arg(self, arg_type, arg):
        return [str(arg_type), str(arg)]

    mat_param = lambda self, x: x
    mat_val = lambda self, x: x
    mat_arg_type = lambda self, x: x

    @v_args(inline=True)
    def array(self, *args):
        return list(args)

    value = lambda self, v: v[0]

    string = lambda self, s: s[0].replace("\"", "")
    QUOTE = lambda self, x: None


def parse_materials(file):
    parser = Lark(
        material_grammar,
        parser="earley",
    )

    with open(file, "r") as f:
        f_str = f.read()
        print("parsing materials")
        t = parser.parse(f_str)
        print("transforming materials")
        return MatToTree().transform(t)


geometry_grammar = r"""
    start: (attribute)+

    attribute: "AttributeBegin"  transform_t?  inner_attribute* "AttributeEnd"
    
    transform_t: "Transform" array

    inner_attribute: "AttributeBegin" (area_light | shape | named_material)* "AttributeEnd"
    named_material: "NamedMaterial" string
    area_light: "AreaLightSource" light_type mat_kv*
    light_type: "\"" "diffuse" "\""
    
    shape: "Shape"  shape_type
    shape_type: ply_shape | triangle_shape
    ply_shape: "\"" "plymesh" "\""  mat_kv+
    triangle_shape: "\"" "trianglemesh" "\""  mat_kv+
    
    mat_kv: mat_arg mat_val
    mat_arg: _QUOTE MAT_ARG_TYPE STRING _QUOTE
    mat_val: string | array | SIGNED_NUMBER
    MAT_ARG_TYPE: "float" | "texture" | "string" |  "point2" | "point3" | "integer" | "normal" | "rgb"


    COMMENT: "#"/[^\n]*/ _NEWLINE
    _QUOTE.4: /\"|"|'/
    string: ESCAPED_STRING
    CR: /\r/
    LF: /\n/
    _NEWLINE: (CR? LF)
    STRING: /[\w+.,]+/
    array: "[" [ (value | _NEWLINE )+ ] "]"
    value: string | NUMBER | SIGNED_NUMBER
    _WS: WS


    %import common.ESCAPED_STRING
    %import common.SIGNED_NUMBER
    %import common.NUMBER
    %import common.WS

    %ignore WS
    %ignore _NEWLINE
    %ignore COMMENT
"""


class Transform(Printable):

    def __init__(self, t):
        self.type = "Transform"
        self.t = t

    def visit(self, root):
        matrix = np.array(self.t).reshape(4, 4).T
        flat = matrix.flatten()
        transform = ET.SubElement(root, "transform", {"name": "toWorld"})
        transform.append(
            ET.Element("matrix", {"value": ", ".join([str(v) for v in flat])}))


class NamedMaterial(Printable):

    def __init__(self, name):
        self.type = "NamedMaterial"
        self.name = name


class Attribute(Printable):

    def __init__(self, children):
        self.type = "Attribute"
        self.children = children


class Shape(Printable):

    def __str__(self):
        return super().__str__()

    def visit(self, root):
        shape = ET.SubElement(root, "string")
        shape.set("name", "filename")
        shape.set("value", self.filename)


class PlyShape(Shape):

    def __init__(self, args, index):
        self.type = "PlyShape"
        self.ply_path = args["filename"]
        self.index = index
        self.obj_path = Path("./meshes") / f"{self.index:05d}.ply.obj"
        self.filename = str(self.obj_path)
        # self._to_obj()

    def _to_obj(self):
        print("P", end="", flush=True)
        # taken from https://gist.github.com/assafrabin/ca110dba829dc960c48ad9ab90b8081e
        """
        Converts the given .ply file to an .obj file
        """
        ply = PlyData.read(self.ply_path)
        # meshio.read(self.ply_path).write(self.filename)

        with open(self.obj_path, 'w') as f:
            f.write("# OBJ file\n")

            vertices = ply['vertex']

            for v in vertices:  # convert vertices
                p = [v['x'], v['y'], v['z']]
                f.write("v %.6f %.6f %.6f\n" % tuple(p))

            n_idxs = defaultdict(lambda: -1)
            n_idx = 0
            for i, v in enumerate(vertices):  # vertex normals
                if 'nx' in v and 'ny' in v and 'nz' in v:
                    n = [v['nx'], v['ny'], v['nz']]
                    f.write("vn %.6f %.6f %.6f\n" % n)
                    n_idxs[i] = n_idx
                    n_idx += 1

            t_idxs = defaultdict(lambda: -1)
            t_idx = 0
            for i, v in enumerate(vertices):  # texture coordinates
                got_tex = False
                try:
                    if not got_tex:
                        t = (v["s"], v["t"])
                        f.write("vt %.6f %.6f\n" % t)
                        got_tex = True
                        t_idxs[i] = t_idx
                        t_idx += 1
                except ValueError:
                    pass
                try:
                    if not got_tex:
                        t = (v["u"], v["v"])
                        f.write("vt %.6f %.6f\n" % t)
                        got_tex = True
                        t_idxs[i] = t_idx
                        t_idx += 1
                except ValueError:
                    pass
                try:
                    if not got_tex:
                        t = (v["texture_u"], v["texture_v"])
                        f.write("vt %.6f %.6f\n" % t)
                        got_tex = True
                        t_idxs[i] = t_idx
                        t_idx += 1
                except ValueError:
                    pass
                try:
                    if not got_tex:
                        t = (v["texture_s"], v["texture_t"])
                        f.write("vt %.6f %.6f\n" % t)
                        got_tex = True
                        t_idxs[i] = t_idx
                        t_idx += 1
                except ValueError:
                    pass

            if 'face' in ply:
                for i in ply['face']['vertex_indices']:
                    f.write("f")
                    for j in range(i.size):
                        # ii = [ i[j]+1 ]
                        ii = [i[j] + 1, n_idxs[i[j]] + 1, t_idxs[i[j]] + 1]
                        ii = [str(a) if a > 0 else "" for a in ii]
                        f.write(" " + "/".join(ii))
                    f.write("\n")


class TriangleShape(Shape):

    def __init__(self, args, index):
        self.type = "TriangleShape"
        self.uv = None
        self.N = None
        self.P = None
        self.indices = None
        self.index = index
        for k, v in args.items():
            setattr(self, k, v)

        obj_dir = Path("./meshes")
        obj_dir.mkdir(exist_ok=True)
        self.filename = str(obj_dir / f"{self.index:05d}.pbrt.obj")
        # self._to_obj()

    def _to_obj(self):
        print("T", end="", flush=True)
        with open(self.filename, "w") as f:
            f.write("# OBJ file\n")

            props_written = 0
            if self.P is not None:
                props_written += 1
                pts = np.array(self.P, dtype=float).reshape(-1, 3)
                for p in pts:
                    f.write(f"v {p[0]:.6f} {p[1]:.6f} {p[2]:.6f}\n")

            if self.N is not None:
                props_written += 1
                normals = np.array(self.N, dtype=float).reshape(-1, 3)
                for n in normals:
                    f.write(f"vn {n[0]:.6f} {n[1]:.6f} {n[2]:.6f}\n")

            if self.uv is not None:
                props_written += 1
                uvs = np.array(self.uv, dtype=float).reshape(-1, 2)
                for uv in uvs:
                    f.write(f"vt {uv[0]:.6f} {uv[1]:.6f}\n")

            if self.indices is not None:
                triangles = np.array(self.indices, dtype=int).reshape(-1, 3)
                for tri in triangles:
                    f.write("f")
                    for i in range(len(tri)):
                        f.write(" ")
                        f.write("/".join(
                            [str(tri[i] + 1) for _ in range(props_written)]))
                    f.write("\n")


class Attribute(Printable):

    def __init__(self):
        self.type = "Attribute"
        self.transform: Transform = None
        self.material: NamedMaterial = None
        self.shape = None
        self.light = None

    def visit(self, root):
        element = ET.SubElement(root, "mesh")
        element.set("type", "obj")
        if self.transform is not None:
            self.transform.visit(element)
        if self.shape is not None:
            self.shape.visit(element)
        if self.light is not None:
            self.light.visit(element)
        if self.material is not None :
            self.material.visit(element)


class AreaLight(Printable):

    def __init__(self, light_type, args):
        self.type = "AreaLight"
        self.light_type = light_type
        for k, v in args.items():
            setattr(self, k, v)

    def visit(self, root):
        light = ET.SubElement(root, "emitter", {"type": "area"})
        light.append(
            ET.Element(
                "color",
                {
                    "name": "radiance",
                    "value": "10, 10, 10"
                    # "value": ", ".join([str(v) for v in self.L])
                }))


class GeoToTree(Transformer):

    def __init__(self):
        self.tri_index = 0

    @v_args(inline=True)
    def start(self, *args):
        # flatten list
        return [a for arg in args for a in arg]

    @v_args(inline=True)
    def transform_t(self, vals):
        return Transform([float(v) for v in vals])

    @v_args(inline=True)
    def attribute(self, *args):
        transform = None
        attrs = []
        for arg in args:
            if isinstance(arg, Transform):
                transform = arg
            elif isinstance(arg, Attribute):
                arg.transform = transform
                attrs.append(arg)
        return attrs

    @v_args(inline=True)
    def inner_attribute(self, *args):
        attr = Attribute()
        for c in args:
            if isinstance(c, PlyShape):
                attr.shape = c
            elif isinstance(c, AreaLight):
                attr.light = c
            elif isinstance(c, Shape):
                attr.shape = c
            elif isinstance(c, NamedMaterial):
                attr.material = c
        return attr

    @v_args(inline=True)
    def shape(self, s):
        return s.children[0]

    @v_args(inline=True)
    def triangle_shape(self, *args):
        args = {k: v for arg in args for k, v in arg.items()}
        shape = TriangleShape(args, self.tri_index)
        self.tri_index += 1
        return shape

    @v_args(inline=True)
    def ply_shape(self, *args):
        args = {k: v for arg in args for k, v in arg.items()}
        shape = PlyShape(args, self.tri_index)
        self.tri_index += 1
        return shape

    @v_args(inline=True)
    def area_light(self, tpe, args):
        return AreaLight(tpe, args)

    light_type = lambda _, t: "diffuse"

    @v_args(inline=True)
    def named_material(self, name):
        return NamedMaterial(name)

    @v_args(inline=True)
    def mat_kv(self, mat_arg, mat_val):
        apply_fn = {
            "float": lambda fs: float(fs),
            "texture": lambda t: t[0],
            "string": str,
            "point2": lambda ps: [float(p) for p in ps],
            "point3": lambda ps: [float(p) for p in ps],
            "normal": lambda ps: [float(p) for p in ps],
            "rgb": lambda ps: [float(p) for p in ps],
            "integer": lambda ps: [int(p) for p in ps],
            "texture": str,
            "spectrum": str,
        }
        tpe, name = mat_arg
        mat_val = mat_val[0]
        return {name: apply_fn[tpe](mat_val)}

    @v_args(inline=True)
    def mat_arg(self, arg_type, arg):
        return [str(arg_type), str(arg)]

    mat_param = lambda self, x: x
    mat_val = lambda self, x: x
    mat_arg_type = lambda self, x: x

    string = lambda self, x: x
    array = list
    value = lambda self, v: v[0]
    string = lambda self, s: s[0].replace("\"", "")
    QUOTE = lambda self, x: None


def parse_geometry(file):
    with open(file, "r") as f:
        f_str = f.read()
        parser = Lark(geometry_grammar, parser="lalr", transformer=GeoToTree())
        print("parsing & transforming geometry")
        return parser.parse(f_str)


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("geo", type=str)
    parser.add_argument("mats", type=str)
    return parser.parse_args()


if __name__ == "__main__":
    args = parse_args()
    MAT_FILE = args.geo
    GEO_FILE = args.mats

    materials = parse_materials(MAT_FILE)
    attributes = parse_geometry(GEO_FILE)

    for attr in attributes:
        # link materials
        if attr.material and attr.material.name:
            name = attr.material.name
            if name in materials:
                attr.material = materials[attr.material.name]
            else:
                attr.material = None

    # convert the parsed scene to nori's format
    print("Creating nori xml")
    root = ET.Element("scene")
    for attr in attributes:
        attr.visit(root)

    print("Writing xml")
    nori_xml = ET.tostring(root, "utf-8")
    parsed = minidom.parseString(nori_xml)
    xml_str = parsed.toprettyxml()
    with open("./converted.xml", "w") as f:
        f.write(xml_str)