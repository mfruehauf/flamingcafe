## CG 2022 Final Project

## Running on euler

1. Start a node with enough cpu power
```bash
srun --ntasks=1 --cpus-per-task=16 --time 8:00:00 --mem-per-cpu=2G --pty bash
```
2. build nori
```bash
mkdir build
cd build
cmake ../nori -DCMAKE_BUILD_TYPE=Release
make -j
```
3. run the render
```bash
./build/nori_cmd scenes/nori/*.xml
```